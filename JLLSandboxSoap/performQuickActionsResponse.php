<?php

class performQuickActionsResponse
{

    /**
     * @var PerformQuickActionResult $result
     */
    protected $result = null;

    /**
     * @param PerformQuickActionResult $result
     */
    public function __construct($result)
    {
      $this->result = $result;
    }

    /**
     * @return PerformQuickActionResult
     */
    public function getResult()
    {
      return $this->result;
    }

    /**
     * @param PerformQuickActionResult $result
     * @return performQuickActionsResponse
     */
    public function setResult($result)
    {
      $this->result = $result;
      return $this;
    }

}
