<?php

class getDeletedResponse
{

    /**
     * @var GetDeletedResult $result
     */
    protected $result = null;

    /**
     * @param GetDeletedResult $result
     */
    public function __construct($result)
    {
      $this->result = $result;
    }

    /**
     * @return GetDeletedResult
     */
    public function getResult()
    {
      return $this->result;
    }

    /**
     * @param GetDeletedResult $result
     * @return getDeletedResponse
     */
    public function setResult($result)
    {
      $this->result = $result;
      return $this;
    }

}
