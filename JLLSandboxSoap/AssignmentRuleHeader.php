<?php

class AssignmentRuleHeader
{

    /**
     * @var ID $assignmentRuleId
     */
    protected $assignmentRuleId = null;

    /**
     * @var boolean $useDefaultRule
     */
    protected $useDefaultRule = null;

    /**
     * @param ID $assignmentRuleId
     * @param boolean $useDefaultRule
     */
    public function __construct($assignmentRuleId, $useDefaultRule)
    {
      $this->assignmentRuleId = $assignmentRuleId;
      $this->useDefaultRule = $useDefaultRule;
    }

    /**
     * @return ID
     */
    public function getAssignmentRuleId()
    {
      return $this->assignmentRuleId;
    }

    /**
     * @param ID $assignmentRuleId
     * @return AssignmentRuleHeader
     */
    public function setAssignmentRuleId($assignmentRuleId)
    {
      $this->assignmentRuleId = $assignmentRuleId;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getUseDefaultRule()
    {
      return $this->useDefaultRule;
    }

    /**
     * @param boolean $useDefaultRule
     * @return AssignmentRuleHeader
     */
    public function setUseDefaultRule($useDefaultRule)
    {
      $this->useDefaultRule = $useDefaultRule;
      return $this;
    }

}
