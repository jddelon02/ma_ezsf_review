<?php

class sendEmailMessageResponse
{

    /**
     * @var SendEmailResult $result
     */
    protected $result = null;

    /**
     * @param SendEmailResult $result
     */
    public function __construct($result)
    {
      $this->result = $result;
    }

    /**
     * @return SendEmailResult
     */
    public function getResult()
    {
      return $this->result;
    }

    /**
     * @param SendEmailResult $result
     * @return sendEmailMessageResponse
     */
    public function setResult($result)
    {
      $this->result = $result;
      return $this;
    }

}
