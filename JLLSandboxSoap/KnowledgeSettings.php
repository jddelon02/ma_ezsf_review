<?php

class KnowledgeSettings
{

    /**
     * @var string $defaultLanguage
     */
    protected $defaultLanguage = null;

    /**
     * @var boolean $knowledgeEnabled
     */
    protected $knowledgeEnabled = null;

    /**
     * @var KnowledgeLanguageItem[] $languages
     */
    protected $languages = null;

    /**
     * @param boolean $knowledgeEnabled
     */
    public function __construct($knowledgeEnabled)
    {
      $this->knowledgeEnabled = $knowledgeEnabled;
    }

    /**
     * @return string
     */
    public function getDefaultLanguage()
    {
      return $this->defaultLanguage;
    }

    /**
     * @param string $defaultLanguage
     * @return KnowledgeSettings
     */
    public function setDefaultLanguage($defaultLanguage)
    {
      $this->defaultLanguage = $defaultLanguage;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getKnowledgeEnabled()
    {
      return $this->knowledgeEnabled;
    }

    /**
     * @param boolean $knowledgeEnabled
     * @return KnowledgeSettings
     */
    public function setKnowledgeEnabled($knowledgeEnabled)
    {
      $this->knowledgeEnabled = $knowledgeEnabled;
      return $this;
    }

    /**
     * @return KnowledgeLanguageItem[]
     */
    public function getLanguages()
    {
      return $this->languages;
    }

    /**
     * @param KnowledgeLanguageItem[] $languages
     * @return KnowledgeSettings
     */
    public function setLanguages(array $languages = null)
    {
      $this->languages = $languages;
      return $this;
    }

}
