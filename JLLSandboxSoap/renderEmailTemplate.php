<?php

class renderEmailTemplate
{

    /**
     * @var RenderEmailTemplateRequest $renderRequests
     */
    protected $renderRequests = null;

    /**
     * @param RenderEmailTemplateRequest $renderRequests
     */
    public function __construct($renderRequests)
    {
      $this->renderRequests = $renderRequests;
    }

    /**
     * @return RenderEmailTemplateRequest
     */
    public function getRenderRequests()
    {
      return $this->renderRequests;
    }

    /**
     * @param RenderEmailTemplateRequest $renderRequests
     * @return renderEmailTemplate
     */
    public function setRenderRequests($renderRequests)
    {
      $this->renderRequests = $renderRequests;
      return $this;
    }

}
