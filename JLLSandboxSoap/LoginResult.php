<?php

class LoginResult
{

    /**
     * @var string $metadataServerUrl
     */
    protected $metadataServerUrl = null;

    /**
     * @var boolean $passwordExpired
     */
    protected $passwordExpired = null;

    /**
     * @var boolean $sandbox
     */
    protected $sandbox = null;

    /**
     * @var string $serverUrl
     */
    protected $serverUrl = null;

    /**
     * @var string $sessionId
     */
    protected $sessionId = null;

    /**
     * @var ID $userId
     */
    protected $userId = null;

    /**
     * @var GetUserInfoResult $userInfo
     */
    protected $userInfo = null;

    /**
     * @param boolean $passwordExpired
     * @param boolean $sandbox
     */
    public function __construct($passwordExpired, $sandbox)
    {
      $this->passwordExpired = $passwordExpired;
      $this->sandbox = $sandbox;
    }

    /**
     * @return string
     */
    public function getMetadataServerUrl()
    {
      return $this->metadataServerUrl;
    }

    /**
     * @param string $metadataServerUrl
     * @return LoginResult
     */
    public function setMetadataServerUrl($metadataServerUrl)
    {
      $this->metadataServerUrl = $metadataServerUrl;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getPasswordExpired()
    {
      return $this->passwordExpired;
    }

    /**
     * @param boolean $passwordExpired
     * @return LoginResult
     */
    public function setPasswordExpired($passwordExpired)
    {
      $this->passwordExpired = $passwordExpired;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getSandbox()
    {
      return $this->sandbox;
    }

    /**
     * @param boolean $sandbox
     * @return LoginResult
     */
    public function setSandbox($sandbox)
    {
      $this->sandbox = $sandbox;
      return $this;
    }

    /**
     * @return string
     */
    public function getServerUrl()
    {
      return $this->serverUrl;
    }

    /**
     * @param string $serverUrl
     * @return LoginResult
     */
    public function setServerUrl($serverUrl)
    {
      $this->serverUrl = $serverUrl;
      return $this;
    }

    /**
     * @return string
     */
    public function getSessionId()
    {
      return $this->sessionId;
    }

    /**
     * @param string $sessionId
     * @return LoginResult
     */
    public function setSessionId($sessionId)
    {
      $this->sessionId = $sessionId;
      return $this;
    }

    /**
     * @return ID
     */
    public function getUserId()
    {
      return $this->userId;
    }

    /**
     * @param ID $userId
     * @return LoginResult
     */
    public function setUserId($userId)
    {
      $this->userId = $userId;
      return $this;
    }

    /**
     * @return GetUserInfoResult
     */
    public function getUserInfo()
    {
      return $this->userInfo;
    }

    /**
     * @param GetUserInfoResult $userInfo
     * @return LoginResult
     */
    public function setUserInfo($userInfo)
    {
      $this->userInfo = $userInfo;
      return $this;
    }

}
