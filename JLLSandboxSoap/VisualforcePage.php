<?php

class VisualforcePage extends DescribeLayoutComponent
{

    /**
     * @var boolean $showLabel
     */
    protected $showLabel = null;

    /**
     * @var boolean $showScrollbars
     */
    protected $showScrollbars = null;

    /**
     * @var string $suggestedHeight
     */
    protected $suggestedHeight = null;

    /**
     * @var string $suggestedWidth
     */
    protected $suggestedWidth = null;

    /**
     * @var string $url
     */
    protected $url = null;

    /**
     * @param int $displayLines
     * @param int $tabOrder
     * @param layoutComponentType $type
     * @param boolean $showLabel
     * @param boolean $showScrollbars
     * @param string $suggestedHeight
     * @param string $suggestedWidth
     * @param string $url
     */
    public function __construct($displayLines, $tabOrder, $type, $showLabel, $showScrollbars, $suggestedHeight, $suggestedWidth, $url)
    {
      parent::__construct($displayLines, $tabOrder, $type);
      $this->showLabel = $showLabel;
      $this->showScrollbars = $showScrollbars;
      $this->suggestedHeight = $suggestedHeight;
      $this->suggestedWidth = $suggestedWidth;
      $this->url = $url;
    }

    /**
     * @return boolean
     */
    public function getShowLabel()
    {
      return $this->showLabel;
    }

    /**
     * @param boolean $showLabel
     * @return VisualforcePage
     */
    public function setShowLabel($showLabel)
    {
      $this->showLabel = $showLabel;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getShowScrollbars()
    {
      return $this->showScrollbars;
    }

    /**
     * @param boolean $showScrollbars
     * @return VisualforcePage
     */
    public function setShowScrollbars($showScrollbars)
    {
      $this->showScrollbars = $showScrollbars;
      return $this;
    }

    /**
     * @return string
     */
    public function getSuggestedHeight()
    {
      return $this->suggestedHeight;
    }

    /**
     * @param string $suggestedHeight
     * @return VisualforcePage
     */
    public function setSuggestedHeight($suggestedHeight)
    {
      $this->suggestedHeight = $suggestedHeight;
      return $this;
    }

    /**
     * @return string
     */
    public function getSuggestedWidth()
    {
      return $this->suggestedWidth;
    }

    /**
     * @param string $suggestedWidth
     * @return VisualforcePage
     */
    public function setSuggestedWidth($suggestedWidth)
    {
      $this->suggestedWidth = $suggestedWidth;
      return $this;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
      return $this->url;
    }

    /**
     * @param string $url
     * @return VisualforcePage
     */
    public function setUrl($url)
    {
      $this->url = $url;
      return $this;
    }

}
