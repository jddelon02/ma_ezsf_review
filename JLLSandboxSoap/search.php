<?php

class search
{

    /**
     * @var string $searchString
     */
    protected $searchString = null;

    /**
     * @param string $searchString
     */
    public function __construct($searchString)
    {
      $this->searchString = $searchString;
    }

    /**
     * @return string
     */
    public function getSearchString()
    {
      return $this->searchString;
    }

    /**
     * @param string $searchString
     * @return search
     */
    public function setSearchString($searchString)
    {
      $this->searchString = $searchString;
      return $this;
    }

}
