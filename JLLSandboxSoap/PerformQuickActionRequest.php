<?php

class PerformQuickActionRequest
{

    /**
     * @var ID $contextId
     */
    protected $contextId = null;

    /**
     * @var string $quickActionName
     */
    protected $quickActionName = null;

    /**
     * @var sObject[] $records
     */
    protected $records = null;

    /**
     * @param string $quickActionName
     */
    public function __construct($quickActionName)
    {
      $this->quickActionName = $quickActionName;
    }

    /**
     * @return ID
     */
    public function getContextId()
    {
      return $this->contextId;
    }

    /**
     * @param ID $contextId
     * @return PerformQuickActionRequest
     */
    public function setContextId($contextId)
    {
      $this->contextId = $contextId;
      return $this;
    }

    /**
     * @return string
     */
    public function getQuickActionName()
    {
      return $this->quickActionName;
    }

    /**
     * @param string $quickActionName
     * @return PerformQuickActionRequest
     */
    public function setQuickActionName($quickActionName)
    {
      $this->quickActionName = $quickActionName;
      return $this;
    }

    /**
     * @return sObject[]
     */
    public function getRecords()
    {
      return $this->records;
    }

    /**
     * @param sObject[] $records
     * @return PerformQuickActionRequest
     */
    public function setRecords(array $records = null)
    {
      $this->records = $records;
      return $this;
    }

}
