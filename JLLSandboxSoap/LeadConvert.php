<?php

class LeadConvert
{

    /**
     * @var ID $accountId
     */
    protected $accountId = null;

    /**
     * @var ID $contactId
     */
    protected $contactId = null;

    /**
     * @var string $convertedStatus
     */
    protected $convertedStatus = null;

    /**
     * @var boolean $doNotCreateOpportunity
     */
    protected $doNotCreateOpportunity = null;

    /**
     * @var ID $leadId
     */
    protected $leadId = null;

    /**
     * @var string $opportunityName
     */
    protected $opportunityName = null;

    /**
     * @var boolean $overwriteLeadSource
     */
    protected $overwriteLeadSource = null;

    /**
     * @var ID $ownerId
     */
    protected $ownerId = null;

    /**
     * @var boolean $sendNotificationEmail
     */
    protected $sendNotificationEmail = null;

    /**
     * @param string $convertedStatus
     * @param boolean $doNotCreateOpportunity
     * @param ID $leadId
     * @param boolean $overwriteLeadSource
     * @param boolean $sendNotificationEmail
     */
    public function __construct($convertedStatus, $doNotCreateOpportunity, $leadId, $overwriteLeadSource, $sendNotificationEmail)
    {
      $this->convertedStatus = $convertedStatus;
      $this->doNotCreateOpportunity = $doNotCreateOpportunity;
      $this->leadId = $leadId;
      $this->overwriteLeadSource = $overwriteLeadSource;
      $this->sendNotificationEmail = $sendNotificationEmail;
    }

    /**
     * @return ID
     */
    public function getAccountId()
    {
      return $this->accountId;
    }

    /**
     * @param ID $accountId
     * @return LeadConvert
     */
    public function setAccountId($accountId)
    {
      $this->accountId = $accountId;
      return $this;
    }

    /**
     * @return ID
     */
    public function getContactId()
    {
      return $this->contactId;
    }

    /**
     * @param ID $contactId
     * @return LeadConvert
     */
    public function setContactId($contactId)
    {
      $this->contactId = $contactId;
      return $this;
    }

    /**
     * @return string
     */
    public function getConvertedStatus()
    {
      return $this->convertedStatus;
    }

    /**
     * @param string $convertedStatus
     * @return LeadConvert
     */
    public function setConvertedStatus($convertedStatus)
    {
      $this->convertedStatus = $convertedStatus;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getDoNotCreateOpportunity()
    {
      return $this->doNotCreateOpportunity;
    }

    /**
     * @param boolean $doNotCreateOpportunity
     * @return LeadConvert
     */
    public function setDoNotCreateOpportunity($doNotCreateOpportunity)
    {
      $this->doNotCreateOpportunity = $doNotCreateOpportunity;
      return $this;
    }

    /**
     * @return ID
     */
    public function getLeadId()
    {
      return $this->leadId;
    }

    /**
     * @param ID $leadId
     * @return LeadConvert
     */
    public function setLeadId($leadId)
    {
      $this->leadId = $leadId;
      return $this;
    }

    /**
     * @return string
     */
    public function getOpportunityName()
    {
      return $this->opportunityName;
    }

    /**
     * @param string $opportunityName
     * @return LeadConvert
     */
    public function setOpportunityName($opportunityName)
    {
      $this->opportunityName = $opportunityName;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getOverwriteLeadSource()
    {
      return $this->overwriteLeadSource;
    }

    /**
     * @param boolean $overwriteLeadSource
     * @return LeadConvert
     */
    public function setOverwriteLeadSource($overwriteLeadSource)
    {
      $this->overwriteLeadSource = $overwriteLeadSource;
      return $this;
    }

    /**
     * @return ID
     */
    public function getOwnerId()
    {
      return $this->ownerId;
    }

    /**
     * @param ID $ownerId
     * @return LeadConvert
     */
    public function setOwnerId($ownerId)
    {
      $this->ownerId = $ownerId;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getSendNotificationEmail()
    {
      return $this->sendNotificationEmail;
    }

    /**
     * @param boolean $sendNotificationEmail
     * @return LeadConvert
     */
    public function setSendNotificationEmail($sendNotificationEmail)
    {
      $this->sendNotificationEmail = $sendNotificationEmail;
      return $this;
    }

}
