<?php

class retrieveQuickActionTemplates
{

    /**
     * @var string $quickActionNames
     */
    protected $quickActionNames = null;

    /**
     * @var ID $contextId
     */
    protected $contextId = null;

    /**
     * @param string $quickActionNames
     * @param ID $contextId
     */
    public function __construct($quickActionNames, $contextId)
    {
      $this->quickActionNames = $quickActionNames;
      $this->contextId = $contextId;
    }

    /**
     * @return string
     */
    public function getQuickActionNames()
    {
      return $this->quickActionNames;
    }

    /**
     * @param string $quickActionNames
     * @return retrieveQuickActionTemplates
     */
    public function setQuickActionNames($quickActionNames)
    {
      $this->quickActionNames = $quickActionNames;
      return $this;
    }

    /**
     * @return ID
     */
    public function getContextId()
    {
      return $this->contextId;
    }

    /**
     * @param ID $contextId
     * @return retrieveQuickActionTemplates
     */
    public function setContextId($contextId)
    {
      $this->contextId = $contextId;
      return $this;
    }

}
