<?php

class DescribeSoqlListView
{

    /**
     * @var ListViewColumn[] $columns
     */
    protected $columns = null;

    /**
     * @var ID $id
     */
    protected $id = null;

    /**
     * @var ListViewOrderBy[] $orderBy
     */
    protected $orderBy = null;

    /**
     * @var string $query
     */
    protected $query = null;

    /**
     * @var string $scope
     */
    protected $scope = null;

    /**
     * @var string $sobjectType
     */
    protected $sobjectType = null;

    /**
     * @var SoqlWhereCondition $whereCondition
     */
    protected $whereCondition = null;

    /**
     * @param ListViewColumn[] $columns
     * @param ID $id
     * @param ListViewOrderBy[] $orderBy
     * @param string $query
     * @param string $sobjectType
     */
    public function __construct(array $columns, $id, array $orderBy, $query, $sobjectType)
    {
      $this->columns = $columns;
      $this->id = $id;
      $this->orderBy = $orderBy;
      $this->query = $query;
      $this->sobjectType = $sobjectType;
    }

    /**
     * @return ListViewColumn[]
     */
    public function getColumns()
    {
      return $this->columns;
    }

    /**
     * @param ListViewColumn[] $columns
     * @return DescribeSoqlListView
     */
    public function setColumns(array $columns)
    {
      $this->columns = $columns;
      return $this;
    }

    /**
     * @return ID
     */
    public function getId()
    {
      return $this->id;
    }

    /**
     * @param ID $id
     * @return DescribeSoqlListView
     */
    public function setId($id)
    {
      $this->id = $id;
      return $this;
    }

    /**
     * @return ListViewOrderBy[]
     */
    public function getOrderBy()
    {
      return $this->orderBy;
    }

    /**
     * @param ListViewOrderBy[] $orderBy
     * @return DescribeSoqlListView
     */
    public function setOrderBy(array $orderBy)
    {
      $this->orderBy = $orderBy;
      return $this;
    }

    /**
     * @return string
     */
    public function getQuery()
    {
      return $this->query;
    }

    /**
     * @param string $query
     * @return DescribeSoqlListView
     */
    public function setQuery($query)
    {
      $this->query = $query;
      return $this;
    }

    /**
     * @return string
     */
    public function getScope()
    {
      return $this->scope;
    }

    /**
     * @param string $scope
     * @return DescribeSoqlListView
     */
    public function setScope($scope)
    {
      $this->scope = $scope;
      return $this;
    }

    /**
     * @return string
     */
    public function getSobjectType()
    {
      return $this->sobjectType;
    }

    /**
     * @param string $sobjectType
     * @return DescribeSoqlListView
     */
    public function setSobjectType($sobjectType)
    {
      $this->sobjectType = $sobjectType;
      return $this;
    }

    /**
     * @return SoqlWhereCondition
     */
    public function getWhereCondition()
    {
      return $this->whereCondition;
    }

    /**
     * @param SoqlWhereCondition $whereCondition
     * @return DescribeSoqlListView
     */
    public function setWhereCondition($whereCondition)
    {
      $this->whereCondition = $whereCondition;
      return $this;
    }

}
