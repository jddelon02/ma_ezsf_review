<?php

class merge
{

    /**
     * @var MergeRequest $request
     */
    protected $request = null;

    /**
     * @param MergeRequest $request
     */
    public function __construct($request)
    {
      $this->request = $request;
    }

    /**
     * @return MergeRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param MergeRequest $request
     * @return merge
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
