<?php

class describeAvailableQuickActions
{

    /**
     * @var string $contextType
     */
    protected $contextType = null;

    /**
     * @param string $contextType
     */
    public function __construct($contextType)
    {
      $this->contextType = $contextType;
    }

    /**
     * @return string
     */
    public function getContextType()
    {
      return $this->contextType;
    }

    /**
     * @param string $contextType
     * @return describeAvailableQuickActions
     */
    public function setContextType($contextType)
    {
      $this->contextType = $contextType;
      return $this;
    }

}
