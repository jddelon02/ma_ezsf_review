<?php

class describeCompactLayoutsResponse
{

    /**
     * @var DescribeCompactLayoutsResult $result
     */
    protected $result = null;

    /**
     * @param DescribeCompactLayoutsResult $result
     */
    public function __construct($result)
    {
      $this->result = $result;
    }

    /**
     * @return DescribeCompactLayoutsResult
     */
    public function getResult()
    {
      return $this->result;
    }

    /**
     * @param DescribeCompactLayoutsResult $result
     * @return describeCompactLayoutsResponse
     */
    public function setResult($result)
    {
      $this->result = $result;
      return $this;
    }

}
