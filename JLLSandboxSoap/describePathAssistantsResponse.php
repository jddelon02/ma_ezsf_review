<?php

class describePathAssistantsResponse
{

    /**
     * @var DescribePathAssistantsResult $result
     */
    protected $result = null;

    /**
     * @param DescribePathAssistantsResult $result
     */
    public function __construct($result)
    {
      $this->result = $result;
    }

    /**
     * @return DescribePathAssistantsResult
     */
    public function getResult()
    {
      return $this->result;
    }

    /**
     * @param DescribePathAssistantsResult $result
     * @return describePathAssistantsResponse
     */
    public function setResult($result)
    {
      $this->result = $result;
      return $this;
    }

}
