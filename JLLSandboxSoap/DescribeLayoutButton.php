<?php

class DescribeLayoutButton
{

    /**
     * @var WebLinkWindowType $behavior
     */
    protected $behavior = null;

    /**
     * @var DescribeColor[] $colors
     */
    protected $colors = null;

    /**
     * @var string $content
     */
    protected $content = null;

    /**
     * @var WebLinkType $contentSource
     */
    protected $contentSource = null;

    /**
     * @var boolean $custom
     */
    protected $custom = null;

    /**
     * @var string $encoding
     */
    protected $encoding = null;

    /**
     * @var int $height
     */
    protected $height = null;

    /**
     * @var DescribeIcon[] $icons
     */
    protected $icons = null;

    /**
     * @var string $label
     */
    protected $label = null;

    /**
     * @var boolean $menubar
     */
    protected $menubar = null;

    /**
     * @var string $name
     */
    protected $name = null;

    /**
     * @var boolean $overridden
     */
    protected $overridden = null;

    /**
     * @var boolean $resizeable
     */
    protected $resizeable = null;

    /**
     * @var boolean $scrollbars
     */
    protected $scrollbars = null;

    /**
     * @var boolean $showsLocation
     */
    protected $showsLocation = null;

    /**
     * @var boolean $showsStatus
     */
    protected $showsStatus = null;

    /**
     * @var boolean $toolbar
     */
    protected $toolbar = null;

    /**
     * @var string $url
     */
    protected $url = null;

    /**
     * @var int $width
     */
    protected $width = null;

    /**
     * @var WebLinkPosition $windowPosition
     */
    protected $windowPosition = null;

    /**
     * @param boolean $custom
     * @param boolean $overridden
     */
    public function __construct($custom, $overridden)
    {
      $this->custom = $custom;
      $this->overridden = $overridden;
    }

    /**
     * @return WebLinkWindowType
     */
    public function getBehavior()
    {
      return $this->behavior;
    }

    /**
     * @param WebLinkWindowType $behavior
     * @return DescribeLayoutButton
     */
    public function setBehavior($behavior)
    {
      $this->behavior = $behavior;
      return $this;
    }

    /**
     * @return DescribeColor[]
     */
    public function getColors()
    {
      return $this->colors;
    }

    /**
     * @param DescribeColor[] $colors
     * @return DescribeLayoutButton
     */
    public function setColors(array $colors = null)
    {
      $this->colors = $colors;
      return $this;
    }

    /**
     * @return string
     */
    public function getContent()
    {
      return $this->content;
    }

    /**
     * @param string $content
     * @return DescribeLayoutButton
     */
    public function setContent($content)
    {
      $this->content = $content;
      return $this;
    }

    /**
     * @return WebLinkType
     */
    public function getContentSource()
    {
      return $this->contentSource;
    }

    /**
     * @param WebLinkType $contentSource
     * @return DescribeLayoutButton
     */
    public function setContentSource($contentSource)
    {
      $this->contentSource = $contentSource;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getCustom()
    {
      return $this->custom;
    }

    /**
     * @param boolean $custom
     * @return DescribeLayoutButton
     */
    public function setCustom($custom)
    {
      $this->custom = $custom;
      return $this;
    }

    /**
     * @return string
     */
    public function getEncoding()
    {
      return $this->encoding;
    }

    /**
     * @param string $encoding
     * @return DescribeLayoutButton
     */
    public function setEncoding($encoding)
    {
      $this->encoding = $encoding;
      return $this;
    }

    /**
     * @return int
     */
    public function getHeight()
    {
      return $this->height;
    }

    /**
     * @param int $height
     * @return DescribeLayoutButton
     */
    public function setHeight($height)
    {
      $this->height = $height;
      return $this;
    }

    /**
     * @return DescribeIcon[]
     */
    public function getIcons()
    {
      return $this->icons;
    }

    /**
     * @param DescribeIcon[] $icons
     * @return DescribeLayoutButton
     */
    public function setIcons(array $icons = null)
    {
      $this->icons = $icons;
      return $this;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
      return $this->label;
    }

    /**
     * @param string $label
     * @return DescribeLayoutButton
     */
    public function setLabel($label)
    {
      $this->label = $label;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getMenubar()
    {
      return $this->menubar;
    }

    /**
     * @param boolean $menubar
     * @return DescribeLayoutButton
     */
    public function setMenubar($menubar)
    {
      $this->menubar = $menubar;
      return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
      return $this->name;
    }

    /**
     * @param string $name
     * @return DescribeLayoutButton
     */
    public function setName($name)
    {
      $this->name = $name;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getOverridden()
    {
      return $this->overridden;
    }

    /**
     * @param boolean $overridden
     * @return DescribeLayoutButton
     */
    public function setOverridden($overridden)
    {
      $this->overridden = $overridden;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getResizeable()
    {
      return $this->resizeable;
    }

    /**
     * @param boolean $resizeable
     * @return DescribeLayoutButton
     */
    public function setResizeable($resizeable)
    {
      $this->resizeable = $resizeable;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getScrollbars()
    {
      return $this->scrollbars;
    }

    /**
     * @param boolean $scrollbars
     * @return DescribeLayoutButton
     */
    public function setScrollbars($scrollbars)
    {
      $this->scrollbars = $scrollbars;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getShowsLocation()
    {
      return $this->showsLocation;
    }

    /**
     * @param boolean $showsLocation
     * @return DescribeLayoutButton
     */
    public function setShowsLocation($showsLocation)
    {
      $this->showsLocation = $showsLocation;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getShowsStatus()
    {
      return $this->showsStatus;
    }

    /**
     * @param boolean $showsStatus
     * @return DescribeLayoutButton
     */
    public function setShowsStatus($showsStatus)
    {
      $this->showsStatus = $showsStatus;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getToolbar()
    {
      return $this->toolbar;
    }

    /**
     * @param boolean $toolbar
     * @return DescribeLayoutButton
     */
    public function setToolbar($toolbar)
    {
      $this->toolbar = $toolbar;
      return $this;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
      return $this->url;
    }

    /**
     * @param string $url
     * @return DescribeLayoutButton
     */
    public function setUrl($url)
    {
      $this->url = $url;
      return $this;
    }

    /**
     * @return int
     */
    public function getWidth()
    {
      return $this->width;
    }

    /**
     * @param int $width
     * @return DescribeLayoutButton
     */
    public function setWidth($width)
    {
      $this->width = $width;
      return $this;
    }

    /**
     * @return WebLinkPosition
     */
    public function getWindowPosition()
    {
      return $this->windowPosition;
    }

    /**
     * @param WebLinkPosition $windowPosition
     * @return DescribeLayoutButton
     */
    public function setWindowPosition($windowPosition)
    {
      $this->windowPosition = $windowPosition;
      return $this;
    }

}
