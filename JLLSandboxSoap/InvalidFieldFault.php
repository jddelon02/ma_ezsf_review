<?php

class InvalidFieldFault extends ApiQueryFault
{

    /**
     * @param ExceptionCode $exceptionCode
     * @param string $exceptionMessage
     * @param int $row
     * @param int $column
     */
    public function __construct($exceptionCode, $exceptionMessage, $row, $column)
    {
      parent::__construct($exceptionCode, $exceptionMessage, $row, $column);
    }

}
