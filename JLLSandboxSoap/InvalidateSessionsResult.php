<?php

class InvalidateSessionsResult
{

    /**
     * @var Error[] $errors
     */
    protected $errors = null;

    /**
     * @var boolean $success
     */
    protected $success = null;

    /**
     * @param boolean $success
     */
    public function __construct($success)
    {
      $this->success = $success;
    }

    /**
     * @return Error[]
     */
    public function getErrors()
    {
      return $this->errors;
    }

    /**
     * @param Error[] $errors
     * @return InvalidateSessionsResult
     */
    public function setErrors(array $errors = null)
    {
      $this->errors = $errors;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getSuccess()
    {
      return $this->success;
    }

    /**
     * @param boolean $success
     * @return InvalidateSessionsResult
     */
    public function setSuccess($success)
    {
      $this->success = $success;
      return $this;
    }

}
