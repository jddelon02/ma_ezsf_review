<?php

class KnowledgeLanguageItem
{

    /**
     * @var boolean $active
     */
    protected $active = null;

    /**
     * @var string $name
     */
    protected $name = null;

    /**
     * @param boolean $active
     * @param string $name
     */
    public function __construct($active, $name)
    {
      $this->active = $active;
      $this->name = $name;
    }

    /**
     * @return boolean
     */
    public function getActive()
    {
      return $this->active;
    }

    /**
     * @param boolean $active
     * @return KnowledgeLanguageItem
     */
    public function setActive($active)
    {
      $this->active = $active;
      return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
      return $this->name;
    }

    /**
     * @param string $name
     * @return KnowledgeLanguageItem
     */
    public function setName($name)
    {
      $this->name = $name;
      return $this;
    }

}
