<?php

class ProcessWorkitemRequest extends ProcessRequest
{

    /**
     * @var string $action
     */
    protected $action = null;

    /**
     * @var ID $workitemId
     */
    protected $workitemId = null;

    /**
     * @param string $action
     * @param ID $workitemId
     */
    public function __construct($action, $workitemId)
    {
      parent::__construct();
      $this->action = $action;
      $this->workitemId = $workitemId;
    }

    /**
     * @return string
     */
    public function getAction()
    {
      return $this->action;
    }

    /**
     * @param string $action
     * @return ProcessWorkitemRequest
     */
    public function setAction($action)
    {
      $this->action = $action;
      return $this;
    }

    /**
     * @return ID
     */
    public function getWorkitemId()
    {
      return $this->workitemId;
    }

    /**
     * @param ID $workitemId
     * @return ProcessWorkitemRequest
     */
    public function setWorkitemId($workitemId)
    {
      $this->workitemId = $workitemId;
      return $this;
    }

}
