<?php

class UserTerritoryDeleteHeader
{

    /**
     * @var ID $transferToUserId
     */
    protected $transferToUserId = null;

    /**
     * @param ID $transferToUserId
     */
    public function __construct($transferToUserId)
    {
      $this->transferToUserId = $transferToUserId;
    }

    /**
     * @return ID
     */
    public function getTransferToUserId()
    {
      return $this->transferToUserId;
    }

    /**
     * @param ID $transferToUserId
     * @return UserTerritoryDeleteHeader
     */
    public function setTransferToUserId($transferToUserId)
    {
      $this->transferToUserId = $transferToUserId;
      return $this;
    }

}
