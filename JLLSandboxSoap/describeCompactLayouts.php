<?php

class describeCompactLayouts
{

    /**
     * @var string $sObjectType
     */
    protected $sObjectType = null;

    /**
     * @var ID $recordTypeIds
     */
    protected $recordTypeIds = null;

    /**
     * @param string $sObjectType
     * @param ID $recordTypeIds
     */
    public function __construct($sObjectType, $recordTypeIds)
    {
      $this->sObjectType = $sObjectType;
      $this->recordTypeIds = $recordTypeIds;
    }

    /**
     * @return string
     */
    public function getSObjectType()
    {
      return $this->sObjectType;
    }

    /**
     * @param string $sObjectType
     * @return describeCompactLayouts
     */
    public function setSObjectType($sObjectType)
    {
      $this->sObjectType = $sObjectType;
      return $this;
    }

    /**
     * @return ID
     */
    public function getRecordTypeIds()
    {
      return $this->recordTypeIds;
    }

    /**
     * @param ID $recordTypeIds
     * @return describeCompactLayouts
     */
    public function setRecordTypeIds($recordTypeIds)
    {
      $this->recordTypeIds = $recordTypeIds;
      return $this;
    }

}
