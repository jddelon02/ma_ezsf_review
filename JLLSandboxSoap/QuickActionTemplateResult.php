<?php

class QuickActionTemplateResult
{

    /**
     * @var sObject $defaultValueFormulas
     */
    protected $defaultValueFormulas = null;

    /**
     * @var sObject $defaultValues
     */
    protected $defaultValues = null;

    /**
     * @var Error[] $errors
     */
    protected $errors = null;

    /**
     * @var boolean $success
     */
    protected $success = null;

    /**
     * @param boolean $success
     */
    public function __construct($success)
    {
      $this->success = $success;
    }

    /**
     * @return sObject
     */
    public function getDefaultValueFormulas()
    {
      return $this->defaultValueFormulas;
    }

    /**
     * @param sObject $defaultValueFormulas
     * @return QuickActionTemplateResult
     */
    public function setDefaultValueFormulas($defaultValueFormulas)
    {
      $this->defaultValueFormulas = $defaultValueFormulas;
      return $this;
    }

    /**
     * @return sObject
     */
    public function getDefaultValues()
    {
      return $this->defaultValues;
    }

    /**
     * @param sObject $defaultValues
     * @return QuickActionTemplateResult
     */
    public function setDefaultValues($defaultValues)
    {
      $this->defaultValues = $defaultValues;
      return $this;
    }

    /**
     * @return Error[]
     */
    public function getErrors()
    {
      return $this->errors;
    }

    /**
     * @param Error[] $errors
     * @return QuickActionTemplateResult
     */
    public function setErrors(array $errors = null)
    {
      $this->errors = $errors;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getSuccess()
    {
      return $this->success;
    }

    /**
     * @param boolean $success
     * @return QuickActionTemplateResult
     */
    public function setSuccess($success)
    {
      $this->success = $success;
      return $this;
    }

}
