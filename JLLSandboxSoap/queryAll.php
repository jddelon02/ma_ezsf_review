<?php

class queryAll
{

    /**
     * @var string $queryString
     */
    protected $queryString = null;

    /**
     * @param string $queryString
     */
    public function __construct($queryString)
    {
      $this->queryString = $queryString;
    }

    /**
     * @return string
     */
    public function getQueryString()
    {
      return $this->queryString;
    }

    /**
     * @param string $queryString
     * @return queryAll
     */
    public function setQueryString($queryString)
    {
      $this->queryString = $queryString;
      return $this;
    }

}
