<?php

class SearchResult
{

    /**
     * @var string $queryId
     */
    protected $queryId = null;

    /**
     * @var SearchRecord[] $searchRecords
     */
    protected $searchRecords = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getQueryId()
    {
      return $this->queryId;
    }

    /**
     * @param string $queryId
     * @return SearchResult
     */
    public function setQueryId($queryId)
    {
      $this->queryId = $queryId;
      return $this;
    }

    /**
     * @return SearchRecord[]
     */
    public function getSearchRecords()
    {
      return $this->searchRecords;
    }

    /**
     * @param SearchRecord[] $searchRecords
     * @return SearchResult
     */
    public function setSearchRecords(array $searchRecords = null)
    {
      $this->searchRecords = $searchRecords;
      return $this;
    }

}
