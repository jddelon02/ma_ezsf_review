<?php

class describeKnowledgeSettingsResponse
{

    /**
     * @var KnowledgeSettings $result
     */
    protected $result = null;

    /**
     * @param KnowledgeSettings $result
     */
    public function __construct($result)
    {
      $this->result = $result;
    }

    /**
     * @return KnowledgeSettings
     */
    public function getResult()
    {
      return $this->result;
    }

    /**
     * @param KnowledgeSettings $result
     * @return describeKnowledgeSettingsResponse
     */
    public function setResult($result)
    {
      $this->result = $result;
      return $this;
    }

}
