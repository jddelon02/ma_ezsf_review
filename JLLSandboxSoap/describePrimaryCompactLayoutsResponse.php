<?php

class describePrimaryCompactLayoutsResponse
{

    /**
     * @var DescribeCompactLayout $result
     */
    protected $result = null;

    /**
     * @param DescribeCompactLayout $result
     */
    public function __construct($result)
    {
      $this->result = $result;
    }

    /**
     * @return DescribeCompactLayout
     */
    public function getResult()
    {
      return $this->result;
    }

    /**
     * @param DescribeCompactLayout $result
     * @return describePrimaryCompactLayoutsResponse
     */
    public function setResult($result)
    {
      $this->result = $result;
      return $this;
    }

}
