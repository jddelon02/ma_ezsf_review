<?php

class DescribeLayoutSection
{

    /**
     * @var int $columns
     */
    protected $columns = null;

    /**
     * @var string $heading
     */
    protected $heading = null;

    /**
     * @var DescribeLayoutRow[] $layoutRows
     */
    protected $layoutRows = null;

    /**
     * @var ID $parentLayoutId
     */
    protected $parentLayoutId = null;

    /**
     * @var int $rows
     */
    protected $rows = null;

    /**
     * @var TabOrderType $tabOrder
     */
    protected $tabOrder = null;

    /**
     * @var boolean $useCollapsibleSection
     */
    protected $useCollapsibleSection = null;

    /**
     * @var boolean $useHeading
     */
    protected $useHeading = null;

    /**
     * @param int $columns
     * @param DescribeLayoutRow[] $layoutRows
     * @param ID $parentLayoutId
     * @param int $rows
     * @param TabOrderType $tabOrder
     * @param boolean $useCollapsibleSection
     * @param boolean $useHeading
     */
    public function __construct($columns, array $layoutRows, $parentLayoutId, $rows, $tabOrder, $useCollapsibleSection, $useHeading)
    {
      $this->columns = $columns;
      $this->layoutRows = $layoutRows;
      $this->parentLayoutId = $parentLayoutId;
      $this->rows = $rows;
      $this->tabOrder = $tabOrder;
      $this->useCollapsibleSection = $useCollapsibleSection;
      $this->useHeading = $useHeading;
    }

    /**
     * @return int
     */
    public function getColumns()
    {
      return $this->columns;
    }

    /**
     * @param int $columns
     * @return DescribeLayoutSection
     */
    public function setColumns($columns)
    {
      $this->columns = $columns;
      return $this;
    }

    /**
     * @return string
     */
    public function getHeading()
    {
      return $this->heading;
    }

    /**
     * @param string $heading
     * @return DescribeLayoutSection
     */
    public function setHeading($heading)
    {
      $this->heading = $heading;
      return $this;
    }

    /**
     * @return DescribeLayoutRow[]
     */
    public function getLayoutRows()
    {
      return $this->layoutRows;
    }

    /**
     * @param DescribeLayoutRow[] $layoutRows
     * @return DescribeLayoutSection
     */
    public function setLayoutRows(array $layoutRows)
    {
      $this->layoutRows = $layoutRows;
      return $this;
    }

    /**
     * @return ID
     */
    public function getParentLayoutId()
    {
      return $this->parentLayoutId;
    }

    /**
     * @param ID $parentLayoutId
     * @return DescribeLayoutSection
     */
    public function setParentLayoutId($parentLayoutId)
    {
      $this->parentLayoutId = $parentLayoutId;
      return $this;
    }

    /**
     * @return int
     */
    public function getRows()
    {
      return $this->rows;
    }

    /**
     * @param int $rows
     * @return DescribeLayoutSection
     */
    public function setRows($rows)
    {
      $this->rows = $rows;
      return $this;
    }

    /**
     * @return TabOrderType
     */
    public function getTabOrder()
    {
      return $this->tabOrder;
    }

    /**
     * @param TabOrderType $tabOrder
     * @return DescribeLayoutSection
     */
    public function setTabOrder($tabOrder)
    {
      $this->tabOrder = $tabOrder;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getUseCollapsibleSection()
    {
      return $this->useCollapsibleSection;
    }

    /**
     * @param boolean $useCollapsibleSection
     * @return DescribeLayoutSection
     */
    public function setUseCollapsibleSection($useCollapsibleSection)
    {
      $this->useCollapsibleSection = $useCollapsibleSection;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getUseHeading()
    {
      return $this->useHeading;
    }

    /**
     * @param boolean $useHeading
     * @return DescribeLayoutSection
     */
    public function setUseHeading($useHeading)
    {
      $this->useHeading = $useHeading;
      return $this;
    }

}
