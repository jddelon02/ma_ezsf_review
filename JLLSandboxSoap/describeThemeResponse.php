<?php

class describeThemeResponse
{

    /**
     * @var DescribeThemeResult $result
     */
    protected $result = null;

    /**
     * @param DescribeThemeResult $result
     */
    public function __construct($result)
    {
      $this->result = $result;
    }

    /**
     * @return DescribeThemeResult
     */
    public function getResult()
    {
      return $this->result;
    }

    /**
     * @param DescribeThemeResult $result
     * @return describeThemeResponse
     */
    public function setResult($result)
    {
      $this->result = $result;
      return $this;
    }

}
