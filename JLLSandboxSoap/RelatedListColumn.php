<?php

class RelatedListColumn
{

    /**
     * @var string $field
     */
    protected $field = null;

    /**
     * @var string $format
     */
    protected $format = null;

    /**
     * @var string $label
     */
    protected $label = null;

    /**
     * @var string $lookupId
     */
    protected $lookupId = null;

    /**
     * @var string $name
     */
    protected $name = null;

    /**
     * @param string $label
     * @param string $name
     */
    public function __construct($label, $name)
    {
      $this->label = $label;
      $this->name = $name;
    }

    /**
     * @return string
     */
    public function getField()
    {
      return $this->field;
    }

    /**
     * @param string $field
     * @return RelatedListColumn
     */
    public function setField($field)
    {
      $this->field = $field;
      return $this;
    }

    /**
     * @return string
     */
    public function getFormat()
    {
      return $this->format;
    }

    /**
     * @param string $format
     * @return RelatedListColumn
     */
    public function setFormat($format)
    {
      $this->format = $format;
      return $this;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
      return $this->label;
    }

    /**
     * @param string $label
     * @return RelatedListColumn
     */
    public function setLabel($label)
    {
      $this->label = $label;
      return $this;
    }

    /**
     * @return string
     */
    public function getLookupId()
    {
      return $this->lookupId;
    }

    /**
     * @param string $lookupId
     * @return RelatedListColumn
     */
    public function setLookupId($lookupId)
    {
      $this->lookupId = $lookupId;
      return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
      return $this->name;
    }

    /**
     * @param string $name
     * @return RelatedListColumn
     */
    public function setName($name)
    {
      $this->name = $name;
      return $this;
    }

}
