<?php

class StreamingEnabledHeader
{

    /**
     * @var boolean $streamingEnabled
     */
    protected $streamingEnabled = null;

    /**
     * @param boolean $streamingEnabled
     */
    public function __construct($streamingEnabled)
    {
      $this->streamingEnabled = $streamingEnabled;
    }

    /**
     * @return boolean
     */
    public function getStreamingEnabled()
    {
      return $this->streamingEnabled;
    }

    /**
     * @param boolean $streamingEnabled
     * @return StreamingEnabledHeader
     */
    public function setStreamingEnabled($streamingEnabled)
    {
      $this->streamingEnabled = $streamingEnabled;
      return $this;
    }

}
