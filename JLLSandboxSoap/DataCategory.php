<?php

class DataCategory
{

    /**
     * @var DataCategory[] $childCategories
     */
    protected $childCategories = null;

    /**
     * @var string $label
     */
    protected $label = null;

    /**
     * @var string $name
     */
    protected $name = null;

    /**
     * @param string $label
     * @param string $name
     */
    public function __construct($label, $name)
    {
      $this->label = $label;
      $this->name = $name;
    }

    /**
     * @return DataCategory[]
     */
    public function getChildCategories()
    {
      return $this->childCategories;
    }

    /**
     * @param DataCategory[] $childCategories
     * @return DataCategory
     */
    public function setChildCategories(array $childCategories = null)
    {
      $this->childCategories = $childCategories;
      return $this;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
      return $this->label;
    }

    /**
     * @param string $label
     * @return DataCategory
     */
    public function setLabel($label)
    {
      $this->label = $label;
      return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
      return $this->name;
    }

    /**
     * @param string $name
     * @return DataCategory
     */
    public function setName($name)
    {
      $this->name = $name;
      return $this;
    }

}
