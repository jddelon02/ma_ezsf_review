<?php

class DuplicateError extends Error
{

    /**
     * @var DuplicateResult $duplicateResult
     */
    protected $duplicateResult = null;

    /**
     * @param string $message
     * @param StatusCode $statusCode
     * @param DuplicateResult $duplicateResult
     */
    public function __construct($message, $statusCode, $duplicateResult)
    {
      parent::__construct($message, $statusCode);
      $this->duplicateResult = $duplicateResult;
    }

    /**
     * @return DuplicateResult
     */
    public function getDuplicateResult()
    {
      return $this->duplicateResult;
    }

    /**
     * @param DuplicateResult $duplicateResult
     * @return DuplicateError
     */
    public function setDuplicateResult($duplicateResult)
    {
      $this->duplicateResult = $duplicateResult;
      return $this;
    }

}
