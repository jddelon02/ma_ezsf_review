<?php

class DuplicateRuleHeader
{

    /**
     * @var boolean $allowSave
     */
    protected $allowSave = null;

    /**
     * @var boolean $includeRecordDetails
     */
    protected $includeRecordDetails = null;

    /**
     * @var boolean $runAsCurrentUser
     */
    protected $runAsCurrentUser = null;

    /**
     * @param boolean $allowSave
     * @param boolean $includeRecordDetails
     * @param boolean $runAsCurrentUser
     */
    public function __construct($allowSave, $includeRecordDetails, $runAsCurrentUser)
    {
      $this->allowSave = $allowSave;
      $this->includeRecordDetails = $includeRecordDetails;
      $this->runAsCurrentUser = $runAsCurrentUser;
    }

    /**
     * @return boolean
     */
    public function getAllowSave()
    {
      return $this->allowSave;
    }

    /**
     * @param boolean $allowSave
     * @return DuplicateRuleHeader
     */
    public function setAllowSave($allowSave)
    {
      $this->allowSave = $allowSave;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIncludeRecordDetails()
    {
      return $this->includeRecordDetails;
    }

    /**
     * @param boolean $includeRecordDetails
     * @return DuplicateRuleHeader
     */
    public function setIncludeRecordDetails($includeRecordDetails)
    {
      $this->includeRecordDetails = $includeRecordDetails;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getRunAsCurrentUser()
    {
      return $this->runAsCurrentUser;
    }

    /**
     * @param boolean $runAsCurrentUser
     * @return DuplicateRuleHeader
     */
    public function setRunAsCurrentUser($runAsCurrentUser)
    {
      $this->runAsCurrentUser = $runAsCurrentUser;
      return $this;
    }

}
