<?php

class sendEmail
{

    /**
     * @var Email $messages
     */
    protected $messages = null;

    /**
     * @param Email $messages
     */
    public function __construct($messages)
    {
      $this->messages = $messages;
    }

    /**
     * @return Email
     */
    public function getMessages()
    {
      return $this->messages;
    }

    /**
     * @param Email $messages
     * @return sendEmail
     */
    public function setMessages($messages)
    {
      $this->messages = $messages;
      return $this;
    }

}
