<?php

class describeQuickActionsResponse
{

    /**
     * @var DescribeQuickActionResult $result
     */
    protected $result = null;

    /**
     * @param DescribeQuickActionResult $result
     */
    public function __construct($result)
    {
      $this->result = $result;
    }

    /**
     * @return DescribeQuickActionResult
     */
    public function getResult()
    {
      return $this->result;
    }

    /**
     * @param DescribeQuickActionResult $result
     * @return describeQuickActionsResponse
     */
    public function setResult($result)
    {
      $this->result = $result;
      return $this;
    }

}
