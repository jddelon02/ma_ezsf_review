<?php

class SearchRecord
{

    /**
     * @var sObject $record
     */
    protected $record = null;

    /**
     * @var SearchSnippet $snippet
     */
    protected $snippet = null;

    /**
     * @param sObject $record
     */
    public function __construct($record)
    {
      $this->record = $record;
    }

    /**
     * @return sObject
     */
    public function getRecord()
    {
      return $this->record;
    }

    /**
     * @param sObject $record
     * @return SearchRecord
     */
    public function setRecord($record)
    {
      $this->record = $record;
      return $this;
    }

    /**
     * @return SearchSnippet
     */
    public function getSnippet()
    {
      return $this->snippet;
    }

    /**
     * @param SearchSnippet $snippet
     * @return SearchRecord
     */
    public function setSnippet($snippet)
    {
      $this->snippet = $snippet;
      return $this;
    }

}
