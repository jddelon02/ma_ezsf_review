<?php

class performQuickActions
{

    /**
     * @var PerformQuickActionRequest $quickActions
     */
    protected $quickActions = null;

    /**
     * @param PerformQuickActionRequest $quickActions
     */
    public function __construct($quickActions)
    {
      $this->quickActions = $quickActions;
    }

    /**
     * @return PerformQuickActionRequest
     */
    public function getQuickActions()
    {
      return $this->quickActions;
    }

    /**
     * @param PerformQuickActionRequest $quickActions
     * @return performQuickActions
     */
    public function setQuickActions($quickActions)
    {
      $this->quickActions = $quickActions;
      return $this;
    }

}
