<?php

class SoqlSubQueryCondition extends SoqlWhereCondition
{

    /**
     * @var string $field
     */
    protected $field = null;

    /**
     * @var soqlOperator $operator
     */
    protected $operator = null;

    /**
     * @var string $subQuery
     */
    protected $subQuery = null;

    /**
     * @param string $field
     * @param soqlOperator $operator
     * @param string $subQuery
     */
    public function __construct($field, $operator, $subQuery)
    {
      $this->field = $field;
      $this->operator = $operator;
      $this->subQuery = $subQuery;
    }

    /**
     * @return string
     */
    public function getField()
    {
      return $this->field;
    }

    /**
     * @param string $field
     * @return SoqlSubQueryCondition
     */
    public function setField($field)
    {
      $this->field = $field;
      return $this;
    }

    /**
     * @return soqlOperator
     */
    public function getOperator()
    {
      return $this->operator;
    }

    /**
     * @param soqlOperator $operator
     * @return SoqlSubQueryCondition
     */
    public function setOperator($operator)
    {
      $this->operator = $operator;
      return $this;
    }

    /**
     * @return string
     */
    public function getSubQuery()
    {
      return $this->subQuery;
    }

    /**
     * @param string $subQuery
     * @return SoqlSubQueryCondition
     */
    public function setSubQuery($subQuery)
    {
      $this->subQuery = $subQuery;
      return $this;
    }

}
