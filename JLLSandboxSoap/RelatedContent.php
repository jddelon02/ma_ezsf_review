<?php

class RelatedContent
{

    /**
     * @var DescribeRelatedContentItem[] $relatedContentItems
     */
    protected $relatedContentItems = null;

    /**
     * @param DescribeRelatedContentItem[] $relatedContentItems
     */
    public function __construct(array $relatedContentItems)
    {
      $this->relatedContentItems = $relatedContentItems;
    }

    /**
     * @return DescribeRelatedContentItem[]
     */
    public function getRelatedContentItems()
    {
      return $this->relatedContentItems;
    }

    /**
     * @param DescribeRelatedContentItem[] $relatedContentItems
     * @return RelatedContent
     */
    public function setRelatedContentItems(array $relatedContentItems)
    {
      $this->relatedContentItems = $relatedContentItems;
      return $this;
    }

}
