<?php

class describeTabsResponse
{

    /**
     * @var DescribeTabSetResult $result
     */
    protected $result = null;

    /**
     * @param DescribeTabSetResult $result
     */
    public function __construct($result)
    {
      $this->result = $result;
    }

    /**
     * @return DescribeTabSetResult
     */
    public function getResult()
    {
      return $this->result;
    }

    /**
     * @param DescribeTabSetResult $result
     * @return describeTabsResponse
     */
    public function setResult($result)
    {
      $this->result = $result;
      return $this;
    }

}
