<?php

class MassEmailMessage extends Email
{

    /**
     * @var string $description
     */
    protected $description = null;

    /**
     * @var ID $targetObjectIds
     */
    protected $targetObjectIds = null;

    /**
     * @var ID $templateId
     */
    protected $templateId = null;

    /**
     * @var ID $whatIds
     */
    protected $whatIds = null;

    /**
     * @param ID $templateId
     */
    public function __construct($templateId)
    {
      parent::__construct();
      $this->templateId = $templateId;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
      return $this->description;
    }

    /**
     * @param string $description
     * @return MassEmailMessage
     */
    public function setDescription($description)
    {
      $this->description = $description;
      return $this;
    }

    /**
     * @return ID
     */
    public function getTargetObjectIds()
    {
      return $this->targetObjectIds;
    }

    /**
     * @param ID $targetObjectIds
     * @return MassEmailMessage
     */
    public function setTargetObjectIds($targetObjectIds)
    {
      $this->targetObjectIds = $targetObjectIds;
      return $this;
    }

    /**
     * @return ID
     */
    public function getTemplateId()
    {
      return $this->templateId;
    }

    /**
     * @param ID $templateId
     * @return MassEmailMessage
     */
    public function setTemplateId($templateId)
    {
      $this->templateId = $templateId;
      return $this;
    }

    /**
     * @return ID
     */
    public function getWhatIds()
    {
      return $this->whatIds;
    }

    /**
     * @param ID $whatIds
     * @return MassEmailMessage
     */
    public function setWhatIds($whatIds)
    {
      $this->whatIds = $whatIds;
      return $this;
    }

}
