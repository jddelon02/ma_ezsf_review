<?php

class describeSObject
{

    /**
     * @var string $sObjectType
     */
    protected $sObjectType = null;

    /**
     * @param string $sObjectType
     */
    public function __construct($sObjectType)
    {
      $this->sObjectType = $sObjectType;
    }

    /**
     * @return string
     */
    public function getSObjectType()
    {
      return $this->sObjectType;
    }

    /**
     * @param string $sObjectType
     * @return describeSObject
     */
    public function setSObjectType($sObjectType)
    {
      $this->sObjectType = $sObjectType;
      return $this;
    }

}
