<?php

class setPasswordResponse
{

    /**
     * @var SetPasswordResult $result
     */
    protected $result = null;

    /**
     * @param SetPasswordResult $result
     */
    public function __construct($result)
    {
      $this->result = $result;
    }

    /**
     * @return SetPasswordResult
     */
    public function getResult()
    {
      return $this->result;
    }

    /**
     * @param SetPasswordResult $result
     * @return setPasswordResponse
     */
    public function setResult($result)
    {
      $this->result = $result;
      return $this;
    }

}
