<?php

class InvalidIdFault extends ApiFault
{

    /**
     * @param ExceptionCode $exceptionCode
     * @param string $exceptionMessage
     */
    public function __construct($exceptionCode, $exceptionMessage)
    {
      parent::__construct($exceptionCode, $exceptionMessage);
    }

}
