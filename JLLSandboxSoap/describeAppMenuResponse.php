<?php

class describeAppMenuResponse
{

    /**
     * @var DescribeAppMenuResult $result
     */
    protected $result = null;

    /**
     * @param DescribeAppMenuResult $result
     */
    public function __construct($result)
    {
      $this->result = $result;
    }

    /**
     * @return DescribeAppMenuResult
     */
    public function getResult()
    {
      return $this->result;
    }

    /**
     * @param DescribeAppMenuResult $result
     * @return describeAppMenuResponse
     */
    public function setResult($result)
    {
      $this->result = $result;
      return $this;
    }

}
