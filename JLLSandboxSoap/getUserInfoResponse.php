<?php

class getUserInfoResponse
{

    /**
     * @var GetUserInfoResult $result
     */
    protected $result = null;

    /**
     * @param GetUserInfoResult $result
     */
    public function __construct($result)
    {
      $this->result = $result;
    }

    /**
     * @return GetUserInfoResult
     */
    public function getResult()
    {
      return $this->result;
    }

    /**
     * @param GetUserInfoResult $result
     * @return getUserInfoResponse
     */
    public function setResult($result)
    {
      $this->result = $result;
      return $this;
    }

}
