<?php

class retrieveQuickActionTemplatesResponse
{

    /**
     * @var QuickActionTemplateResult $result
     */
    protected $result = null;

    /**
     * @param QuickActionTemplateResult $result
     */
    public function __construct($result)
    {
      $this->result = $result;
    }

    /**
     * @return QuickActionTemplateResult
     */
    public function getResult()
    {
      return $this->result;
    }

    /**
     * @param QuickActionTemplateResult $result
     * @return retrieveQuickActionTemplatesResponse
     */
    public function setResult($result)
    {
      $this->result = $result;
      return $this;
    }

}
