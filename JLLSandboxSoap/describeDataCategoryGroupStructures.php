<?php

class describeDataCategoryGroupStructures
{

    /**
     * @var DataCategoryGroupSobjectTypePair $pairs
     */
    protected $pairs = null;

    /**
     * @var boolean $topCategoriesOnly
     */
    protected $topCategoriesOnly = null;

    /**
     * @param DataCategoryGroupSobjectTypePair $pairs
     * @param boolean $topCategoriesOnly
     */
    public function __construct($pairs, $topCategoriesOnly)
    {
      $this->pairs = $pairs;
      $this->topCategoriesOnly = $topCategoriesOnly;
    }

    /**
     * @return DataCategoryGroupSobjectTypePair
     */
    public function getPairs()
    {
      return $this->pairs;
    }

    /**
     * @param DataCategoryGroupSobjectTypePair $pairs
     * @return describeDataCategoryGroupStructures
     */
    public function setPairs($pairs)
    {
      $this->pairs = $pairs;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getTopCategoriesOnly()
    {
      return $this->topCategoriesOnly;
    }

    /**
     * @param boolean $topCategoriesOnly
     * @return describeDataCategoryGroupStructures
     */
    public function setTopCategoriesOnly($topCategoriesOnly)
    {
      $this->topCategoriesOnly = $topCategoriesOnly;
      return $this;
    }

}
