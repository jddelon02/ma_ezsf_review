<?php

class DisableFeedTrackingHeader
{

    /**
     * @var boolean $disableFeedTracking
     */
    protected $disableFeedTracking = null;

    /**
     * @param boolean $disableFeedTracking
     */
    public function __construct($disableFeedTracking)
    {
      $this->disableFeedTracking = $disableFeedTracking;
    }

    /**
     * @return boolean
     */
    public function getDisableFeedTracking()
    {
      return $this->disableFeedTracking;
    }

    /**
     * @param boolean $disableFeedTracking
     * @return DisableFeedTrackingHeader
     */
    public function setDisableFeedTracking($disableFeedTracking)
    {
      $this->disableFeedTracking = $disableFeedTracking;
      return $this;
    }

}
