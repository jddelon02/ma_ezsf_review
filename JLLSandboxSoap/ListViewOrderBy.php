<?php

class ListViewOrderBy
{

    /**
     * @var string $fieldNameOrPath
     */
    protected $fieldNameOrPath = null;

    /**
     * @var orderByNullsPosition $nullsPosition
     */
    protected $nullsPosition = null;

    /**
     * @var orderByDirection $sortDirection
     */
    protected $sortDirection = null;

    /**
     * @param string $fieldNameOrPath
     */
    public function __construct($fieldNameOrPath)
    {
      $this->fieldNameOrPath = $fieldNameOrPath;
    }

    /**
     * @return string
     */
    public function getFieldNameOrPath()
    {
      return $this->fieldNameOrPath;
    }

    /**
     * @param string $fieldNameOrPath
     * @return ListViewOrderBy
     */
    public function setFieldNameOrPath($fieldNameOrPath)
    {
      $this->fieldNameOrPath = $fieldNameOrPath;
      return $this;
    }

    /**
     * @return orderByNullsPosition
     */
    public function getNullsPosition()
    {
      return $this->nullsPosition;
    }

    /**
     * @param orderByNullsPosition $nullsPosition
     * @return ListViewOrderBy
     */
    public function setNullsPosition($nullsPosition)
    {
      $this->nullsPosition = $nullsPosition;
      return $this;
    }

    /**
     * @return orderByDirection
     */
    public function getSortDirection()
    {
      return $this->sortDirection;
    }

    /**
     * @param orderByDirection $sortDirection
     * @return ListViewOrderBy
     */
    public function setSortDirection($sortDirection)
    {
      $this->sortDirection = $sortDirection;
      return $this;
    }

}
