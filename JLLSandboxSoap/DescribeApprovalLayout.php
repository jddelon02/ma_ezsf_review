<?php

class describeApprovalLayout
{

    /**
     * @var string $sObjectType
     */
    protected $sObjectType = null;

    /**
     * @var string $approvalProcessNames
     */
    protected $approvalProcessNames = null;

    /**
     * @param string $sObjectType
     * @param string $approvalProcessNames
     */
    public function __construct($sObjectType, $approvalProcessNames)
    {
      $this->sObjectType = $sObjectType;
      $this->approvalProcessNames = $approvalProcessNames;
    }

    /**
     * @return string
     */
    public function getSObjectType()
    {
      return $this->sObjectType;
    }

    /**
     * @param string $sObjectType
     * @return describeApprovalLayout
     */
    public function setSObjectType($sObjectType)
    {
      $this->sObjectType = $sObjectType;
      return $this;
    }

    /**
     * @return string
     */
    public function getApprovalProcessNames()
    {
      return $this->approvalProcessNames;
    }

    /**
     * @param string $approvalProcessNames
     * @return describeApprovalLayout
     */
    public function setApprovalProcessNames($approvalProcessNames)
    {
      $this->approvalProcessNames = $approvalProcessNames;
      return $this;
    }

}
