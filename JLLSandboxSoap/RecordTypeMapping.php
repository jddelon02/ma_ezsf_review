<?php

class RecordTypeMapping
{

    /**
     * @var boolean $available
     */
    protected $available = null;

    /**
     * @var boolean $defaultRecordTypeMapping
     */
    protected $defaultRecordTypeMapping = null;

    /**
     * @var ID $layoutId
     */
    protected $layoutId = null;

    /**
     * @var boolean $master
     */
    protected $master = null;

    /**
     * @var string $name
     */
    protected $name = null;

    /**
     * @var PicklistForRecordType[] $picklistsForRecordType
     */
    protected $picklistsForRecordType = null;

    /**
     * @var ID $recordTypeId
     */
    protected $recordTypeId = null;

    /**
     * @param boolean $available
     * @param boolean $defaultRecordTypeMapping
     * @param ID $layoutId
     * @param boolean $master
     * @param string $name
     */
    public function __construct($available, $defaultRecordTypeMapping, $layoutId, $master, $name)
    {
      $this->available = $available;
      $this->defaultRecordTypeMapping = $defaultRecordTypeMapping;
      $this->layoutId = $layoutId;
      $this->master = $master;
      $this->name = $name;
    }

    /**
     * @return boolean
     */
    public function getAvailable()
    {
      return $this->available;
    }

    /**
     * @param boolean $available
     * @return RecordTypeMapping
     */
    public function setAvailable($available)
    {
      $this->available = $available;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getDefaultRecordTypeMapping()
    {
      return $this->defaultRecordTypeMapping;
    }

    /**
     * @param boolean $defaultRecordTypeMapping
     * @return RecordTypeMapping
     */
    public function setDefaultRecordTypeMapping($defaultRecordTypeMapping)
    {
      $this->defaultRecordTypeMapping = $defaultRecordTypeMapping;
      return $this;
    }

    /**
     * @return ID
     */
    public function getLayoutId()
    {
      return $this->layoutId;
    }

    /**
     * @param ID $layoutId
     * @return RecordTypeMapping
     */
    public function setLayoutId($layoutId)
    {
      $this->layoutId = $layoutId;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getMaster()
    {
      return $this->master;
    }

    /**
     * @param boolean $master
     * @return RecordTypeMapping
     */
    public function setMaster($master)
    {
      $this->master = $master;
      return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
      return $this->name;
    }

    /**
     * @param string $name
     * @return RecordTypeMapping
     */
    public function setName($name)
    {
      $this->name = $name;
      return $this;
    }

    /**
     * @return PicklistForRecordType[]
     */
    public function getPicklistsForRecordType()
    {
      return $this->picklistsForRecordType;
    }

    /**
     * @param PicklistForRecordType[] $picklistsForRecordType
     * @return RecordTypeMapping
     */
    public function setPicklistsForRecordType(array $picklistsForRecordType = null)
    {
      $this->picklistsForRecordType = $picklistsForRecordType;
      return $this;
    }

    /**
     * @return ID
     */
    public function getRecordTypeId()
    {
      return $this->recordTypeId;
    }

    /**
     * @param ID $recordTypeId
     * @return RecordTypeMapping
     */
    public function setRecordTypeId($recordTypeId)
    {
      $this->recordTypeId = $recordTypeId;
      return $this;
    }

}
