<?php

class ExecuteListViewResult
{

    /**
     * @var ListViewColumn[] $columns
     */
    protected $columns = null;

    /**
     * @var string $developerName
     */
    protected $developerName = null;

    /**
     * @var boolean $done
     */
    protected $done = null;

    /**
     * @var ID $id
     */
    protected $id = null;

    /**
     * @var string $label
     */
    protected $label = null;

    /**
     * @var ListViewRecord[] $records
     */
    protected $records = null;

    /**
     * @var int $size
     */
    protected $size = null;

    /**
     * @param ListViewColumn[] $columns
     * @param string $developerName
     * @param boolean $done
     * @param ID $id
     * @param string $label
     * @param ListViewRecord[] $records
     * @param int $size
     */
    public function __construct(array $columns, $developerName, $done, $id, $label, array $records, $size)
    {
      $this->columns = $columns;
      $this->developerName = $developerName;
      $this->done = $done;
      $this->id = $id;
      $this->label = $label;
      $this->records = $records;
      $this->size = $size;
    }

    /**
     * @return ListViewColumn[]
     */
    public function getColumns()
    {
      return $this->columns;
    }

    /**
     * @param ListViewColumn[] $columns
     * @return ExecuteListViewResult
     */
    public function setColumns(array $columns)
    {
      $this->columns = $columns;
      return $this;
    }

    /**
     * @return string
     */
    public function getDeveloperName()
    {
      return $this->developerName;
    }

    /**
     * @param string $developerName
     * @return ExecuteListViewResult
     */
    public function setDeveloperName($developerName)
    {
      $this->developerName = $developerName;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getDone()
    {
      return $this->done;
    }

    /**
     * @param boolean $done
     * @return ExecuteListViewResult
     */
    public function setDone($done)
    {
      $this->done = $done;
      return $this;
    }

    /**
     * @return ID
     */
    public function getId()
    {
      return $this->id;
    }

    /**
     * @param ID $id
     * @return ExecuteListViewResult
     */
    public function setId($id)
    {
      $this->id = $id;
      return $this;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
      return $this->label;
    }

    /**
     * @param string $label
     * @return ExecuteListViewResult
     */
    public function setLabel($label)
    {
      $this->label = $label;
      return $this;
    }

    /**
     * @return ListViewRecord[]
     */
    public function getRecords()
    {
      return $this->records;
    }

    /**
     * @param ListViewRecord[] $records
     * @return ExecuteListViewResult
     */
    public function setRecords(array $records)
    {
      $this->records = $records;
      return $this;
    }

    /**
     * @return int
     */
    public function getSize()
    {
      return $this->size;
    }

    /**
     * @param int $size
     * @return ExecuteListViewResult
     */
    public function setSize($size)
    {
      $this->size = $size;
      return $this;
    }

}
