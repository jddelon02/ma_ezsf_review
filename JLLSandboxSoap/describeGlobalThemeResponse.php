<?php

class describeGlobalThemeResponse
{

    /**
     * @var DescribeGlobalTheme $result
     */
    protected $result = null;

    /**
     * @param DescribeGlobalTheme $result
     */
    public function __construct($result)
    {
      $this->result = $result;
    }

    /**
     * @return DescribeGlobalTheme
     */
    public function getResult()
    {
      return $this->result;
    }

    /**
     * @param DescribeGlobalTheme $result
     * @return describeGlobalThemeResponse
     */
    public function setResult($result)
    {
      $this->result = $result;
      return $this;
    }

}
