<?php

class LocaleOptions
{

    /**
     * @var string $language
     */
    protected $language = null;

    /**
     * @var boolean $localizeErrors
     */
    protected $localizeErrors = null;

    /**
     * @param string $language
     * @param boolean $localizeErrors
     */
    public function __construct($language, $localizeErrors)
    {
      $this->language = $language;
      $this->localizeErrors = $localizeErrors;
    }

    /**
     * @return string
     */
    public function getLanguage()
    {
      return $this->language;
    }

    /**
     * @param string $language
     * @return LocaleOptions
     */
    public function setLanguage($language)
    {
      $this->language = $language;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getLocalizeErrors()
    {
      return $this->localizeErrors;
    }

    /**
     * @param boolean $localizeErrors
     * @return LocaleOptions
     */
    public function setLocalizeErrors($localizeErrors)
    {
      $this->localizeErrors = $localizeErrors;
      return $this;
    }

}
