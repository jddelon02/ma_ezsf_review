<?php

class ResetPasswordResult
{

    /**
     * @var string $password
     */
    protected $password = null;

    /**
     * @param string $password
     */
    public function __construct($password)
    {
      $this->password = $password;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
      return $this->password;
    }

    /**
     * @param string $password
     * @return ResetPasswordResult
     */
    public function setPassword($password)
    {
      $this->password = $password;
      return $this;
    }

}
