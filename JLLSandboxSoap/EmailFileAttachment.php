<?php

class EmailFileAttachment
{

    /**
     * @var base64Binary $body
     */
    protected $body = null;

    /**
     * @var string $contentType
     */
    protected $contentType = null;

    /**
     * @var string $fileName
     */
    protected $fileName = null;

    /**
     * @var boolean $inline
     */
    protected $inline = null;

    /**
     * @param string $fileName
     */
    public function __construct($fileName)
    {
      $this->fileName = $fileName;
    }

    /**
     * @return base64Binary
     */
    public function getBody()
    {
      return $this->body;
    }

    /**
     * @param base64Binary $body
     * @return EmailFileAttachment
     */
    public function setBody($body)
    {
      $this->body = $body;
      return $this;
    }

    /**
     * @return string
     */
    public function getContentType()
    {
      return $this->contentType;
    }

    /**
     * @param string $contentType
     * @return EmailFileAttachment
     */
    public function setContentType($contentType)
    {
      $this->contentType = $contentType;
      return $this;
    }

    /**
     * @return string
     */
    public function getFileName()
    {
      return $this->fileName;
    }

    /**
     * @param string $fileName
     * @return EmailFileAttachment
     */
    public function setFileName($fileName)
    {
      $this->fileName = $fileName;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getInline()
    {
      return $this->inline;
    }

    /**
     * @param boolean $inline
     * @return EmailFileAttachment
     */
    public function setInline($inline)
    {
      $this->inline = $inline;
      return $this;
    }

}
