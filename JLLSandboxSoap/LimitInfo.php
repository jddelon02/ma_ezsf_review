<?php

class LimitInfo
{

    /**
     * @var int $current
     */
    protected $current = null;

    /**
     * @var int $limit
     */
    protected $limit = null;

    /**
     * @var string $type
     */
    protected $type = null;

    /**
     * @param int $current
     * @param int $limit
     * @param string $type
     */
    public function __construct($current, $limit, $type)
    {
      $this->current = $current;
      $this->limit = $limit;
      $this->type = $type;
    }

    /**
     * @return int
     */
    public function getCurrent()
    {
      return $this->current;
    }

    /**
     * @param int $current
     * @return LimitInfo
     */
    public function setCurrent($current)
    {
      $this->current = $current;
      return $this;
    }

    /**
     * @return int
     */
    public function getLimit()
    {
      return $this->limit;
    }

    /**
     * @param int $limit
     * @return LimitInfo
     */
    public function setLimit($limit)
    {
      $this->limit = $limit;
      return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
      return $this->type;
    }

    /**
     * @param string $type
     * @return LimitInfo
     */
    public function setType($type)
    {
      $this->type = $type;
      return $this;
    }

}
