<?php

class CallOptions
{

    /**
     * @var string $client
     */
    protected $client = null;

    /**
     * @var string $defaultNamespace
     */
    protected $defaultNamespace = null;

    /**
     * @var boolean $returnFieldDataTypes
     */
    protected $returnFieldDataTypes = null;

    /**
     * @param string $client
     * @param string $defaultNamespace
     * @param boolean $returnFieldDataTypes
     */
    public function __construct($client, $defaultNamespace, $returnFieldDataTypes)
    {
      $this->client = $client;
      $this->defaultNamespace = $defaultNamespace;
      $this->returnFieldDataTypes = $returnFieldDataTypes;
    }

    /**
     * @return string
     */
    public function getClient()
    {
      return $this->client;
    }

    /**
     * @param string $client
     * @return CallOptions
     */
    public function setClient($client)
    {
      $this->client = $client;
      return $this;
    }

    /**
     * @return string
     */
    public function getDefaultNamespace()
    {
      return $this->defaultNamespace;
    }

    /**
     * @param string $defaultNamespace
     * @return CallOptions
     */
    public function setDefaultNamespace($defaultNamespace)
    {
      $this->defaultNamespace = $defaultNamespace;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getReturnFieldDataTypes()
    {
      return $this->returnFieldDataTypes;
    }

    /**
     * @param boolean $returnFieldDataTypes
     * @return CallOptions
     */
    public function setReturnFieldDataTypes($returnFieldDataTypes)
    {
      $this->returnFieldDataTypes = $returnFieldDataTypes;
      return $this;
    }

}
