<?php

class DescribeFlexiPageResult
{

    /**
     * @var ID $id
     */
    protected $id = null;

    /**
     * @var string $label
     */
    protected $label = null;

    /**
     * @var string $name
     */
    protected $name = null;

    /**
     * @var DescribeQuickActionListResult $quickActionList
     */
    protected $quickActionList = null;

    /**
     * @var DescribeFlexiPageRegion[] $regions
     */
    protected $regions = null;

    /**
     * @var string $sobjectType
     */
    protected $sobjectType = null;

    /**
     * @var string $template
     */
    protected $template = null;

    /**
     * @var string $type
     */
    protected $type = null;

    /**
     * @param ID $id
     * @param string $label
     * @param string $name
     * @param string $type
     */
    public function __construct($id, $label, $name, $type)
    {
      $this->id = $id;
      $this->label = $label;
      $this->name = $name;
      $this->type = $type;
    }

    /**
     * @return ID
     */
    public function getId()
    {
      return $this->id;
    }

    /**
     * @param ID $id
     * @return DescribeFlexiPageResult
     */
    public function setId($id)
    {
      $this->id = $id;
      return $this;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
      return $this->label;
    }

    /**
     * @param string $label
     * @return DescribeFlexiPageResult
     */
    public function setLabel($label)
    {
      $this->label = $label;
      return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
      return $this->name;
    }

    /**
     * @param string $name
     * @return DescribeFlexiPageResult
     */
    public function setName($name)
    {
      $this->name = $name;
      return $this;
    }

    /**
     * @return DescribeQuickActionListResult
     */
    public function getQuickActionList()
    {
      return $this->quickActionList;
    }

    /**
     * @param DescribeQuickActionListResult $quickActionList
     * @return DescribeFlexiPageResult
     */
    public function setQuickActionList($quickActionList)
    {
      $this->quickActionList = $quickActionList;
      return $this;
    }

    /**
     * @return DescribeFlexiPageRegion[]
     */
    public function getRegions()
    {
      return $this->regions;
    }

    /**
     * @param DescribeFlexiPageRegion[] $regions
     * @return DescribeFlexiPageResult
     */
    public function setRegions(array $regions = null)
    {
      $this->regions = $regions;
      return $this;
    }

    /**
     * @return string
     */
    public function getSobjectType()
    {
      return $this->sobjectType;
    }

    /**
     * @param string $sobjectType
     * @return DescribeFlexiPageResult
     */
    public function setSobjectType($sobjectType)
    {
      $this->sobjectType = $sobjectType;
      return $this;
    }

    /**
     * @return string
     */
    public function getTemplate()
    {
      return $this->template;
    }

    /**
     * @param string $template
     * @return DescribeFlexiPageResult
     */
    public function setTemplate($template)
    {
      $this->template = $template;
      return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
      return $this->type;
    }

    /**
     * @param string $type
     * @return DescribeFlexiPageResult
     */
    public function setType($type)
    {
      $this->type = $type;
      return $this;
    }

}
