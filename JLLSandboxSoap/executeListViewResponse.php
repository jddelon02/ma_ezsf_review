<?php

class executeListViewResponse
{

    /**
     * @var ExecuteListViewResult $result
     */
    protected $result = null;

    /**
     * @param ExecuteListViewResult $result
     */
    public function __construct($result)
    {
      $this->result = $result;
    }

    /**
     * @return ExecuteListViewResult
     */
    public function getResult()
    {
      return $this->result;
    }

    /**
     * @param ExecuteListViewResult $result
     * @return executeListViewResponse
     */
    public function setResult($result)
    {
      $this->result = $result;
      return $this;
    }

}
