<?php

class DescribeLayoutFeedView
{

    /**
     * @var DescribeLayoutFeedFilter[] $feedFilters
     */
    protected $feedFilters = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return DescribeLayoutFeedFilter[]
     */
    public function getFeedFilters()
    {
      return $this->feedFilters;
    }

    /**
     * @param DescribeLayoutFeedFilter[] $feedFilters
     * @return DescribeLayoutFeedView
     */
    public function setFeedFilters(array $feedFilters = null)
    {
      $this->feedFilters = $feedFilters;
      return $this;
    }

}
