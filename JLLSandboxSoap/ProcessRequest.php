<?php

class ProcessRequest
{

    /**
     * @var string $comments
     */
    protected $comments = null;

    /**
     * @var ID[] $nextApproverIds
     */
    protected $nextApproverIds = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getComments()
    {
      return $this->comments;
    }

    /**
     * @param string $comments
     * @return ProcessRequest
     */
    public function setComments($comments)
    {
      $this->comments = $comments;
      return $this;
    }

    /**
     * @return ID[]
     */
    public function getNextApproverIds()
    {
      return $this->nextApproverIds;
    }

    /**
     * @param ID[] $nextApproverIds
     * @return ProcessRequest
     */
    public function setNextApproverIds(array $nextApproverIds = null)
    {
      $this->nextApproverIds = $nextApproverIds;
      return $this;
    }

}
