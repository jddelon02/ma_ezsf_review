<?php

class RenderEmailTemplateResult
{

    /**
     * @var RenderEmailTemplateBodyResult $bodyResults
     */
    protected $bodyResults = null;

    /**
     * @var Error[] $errors
     */
    protected $errors = null;

    /**
     * @var boolean $success
     */
    protected $success = null;

    /**
     * @param boolean $success
     */
    public function __construct($success)
    {
      $this->success = $success;
    }

    /**
     * @return RenderEmailTemplateBodyResult
     */
    public function getBodyResults()
    {
      return $this->bodyResults;
    }

    /**
     * @param RenderEmailTemplateBodyResult $bodyResults
     * @return RenderEmailTemplateResult
     */
    public function setBodyResults($bodyResults)
    {
      $this->bodyResults = $bodyResults;
      return $this;
    }

    /**
     * @return Error[]
     */
    public function getErrors()
    {
      return $this->errors;
    }

    /**
     * @param Error[] $errors
     * @return RenderEmailTemplateResult
     */
    public function setErrors(array $errors = null)
    {
      $this->errors = $errors;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getSuccess()
    {
      return $this->success;
    }

    /**
     * @param boolean $success
     * @return RenderEmailTemplateResult
     */
    public function setSuccess($success)
    {
      $this->success = $success;
      return $this;
    }

}
