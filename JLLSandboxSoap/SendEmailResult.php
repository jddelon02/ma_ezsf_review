<?php

class SendEmailResult
{

    /**
     * @var SendEmailError[] $errors
     */
    protected $errors = null;

    /**
     * @var boolean $success
     */
    protected $success = null;

    /**
     * @param boolean $success
     */
    public function __construct($success)
    {
      $this->success = $success;
    }

    /**
     * @return SendEmailError[]
     */
    public function getErrors()
    {
      return $this->errors;
    }

    /**
     * @param SendEmailError[] $errors
     * @return SendEmailResult
     */
    public function setErrors(array $errors = null)
    {
      $this->errors = $errors;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getSuccess()
    {
      return $this->success;
    }

    /**
     * @param boolean $success
     * @return SendEmailResult
     */
    public function setSuccess($success)
    {
      $this->success = $success;
      return $this;
    }

}
