<?php


/**
 * Sforce SOAP API
 */
class SforceService extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     */
    private static $classmap = array (
      'sObject' => '\\sObject',
      'address' => '\\address',
      'location' => '\\location',
      'QueryResult' => '\\QueryResult',
      'SearchResult' => '\\SearchResult',
      'SearchRecord' => '\\SearchRecord',
      'SearchSnippet' => '\\SearchSnippet',
      'RelationshipReferenceTo' => '\\RelationshipReferenceTo',
      'RecordTypesSupported' => '\\RecordTypesSupported',
      'SearchLayoutButtonsDisplayed' => '\\SearchLayoutButtonsDisplayed',
      'SearchLayoutButton' => '\\SearchLayoutButton',
      'SearchLayoutFieldsDisplayed' => '\\SearchLayoutFieldsDisplayed',
      'SearchLayoutField' => '\\SearchLayoutField',
      'NameValuePair' => '\\NameValuePair',
      'NameObjectValuePair' => '\\NameObjectValuePair',
      'GetUpdatedResult' => '\\GetUpdatedResult',
      'GetDeletedResult' => '\\GetDeletedResult',
      'DeletedRecord' => '\\DeletedRecord',
      'GetServerTimestampResult' => '\\GetServerTimestampResult',
      'InvalidateSessionsResult' => '\\InvalidateSessionsResult',
      'SetPasswordResult' => '\\SetPasswordResult',
      'ResetPasswordResult' => '\\ResetPasswordResult',
      'GetUserInfoResult' => '\\GetUserInfoResult',
      'LoginResult' => '\\LoginResult',
      'Error' => '\\Error',
      'SendEmailError' => '\\SendEmailError',
      'SaveResult' => '\\SaveResult',
      'RenderEmailTemplateError' => '\\RenderEmailTemplateError',
      'UpsertResult' => '\\UpsertResult',
      'PerformQuickActionResult' => '\\PerformQuickActionResult',
      'QuickActionTemplateResult' => '\\QuickActionTemplateResult',
      'MergeRequest' => '\\MergeRequest',
      'MergeResult' => '\\MergeResult',
      'ProcessRequest' => '\\ProcessRequest',
      'ProcessSubmitRequest' => '\\ProcessSubmitRequest',
      'ProcessWorkitemRequest' => '\\ProcessWorkitemRequest',
      'PerformQuickActionRequest' => '\\PerformQuickActionRequest',
      'DescribeAvailableQuickActionResult' => '\\DescribeAvailableQuickActionResult',
      'DescribeQuickActionResult' => '\\DescribeQuickActionResult',
      'DescribeQuickActionDefaultValue' => '\\DescribeQuickActionDefaultValue',
      'ProcessResult' => '\\ProcessResult',
      'DeleteResult' => '\\DeleteResult',
      'UndeleteResult' => '\\UndeleteResult',
      'EmptyRecycleBinResult' => '\\EmptyRecycleBinResult',
      'LeadConvert' => '\\LeadConvert',
      'LeadConvertResult' => '\\LeadConvertResult',
      'DescribeSObjectResult' => '\\DescribeSObjectResult',
      'DescribeGlobalSObjectResult' => '\\DescribeGlobalSObjectResult',
      'ChildRelationship' => '\\ChildRelationship',
      'DescribeGlobalResult' => '\\DescribeGlobalResult',
      'DescribeGlobalTheme' => '\\DescribeGlobalTheme',
      'ScopeInfo' => '\\ScopeInfo',
      'FilteredLookupInfo' => '\\FilteredLookupInfo',
      'Field' => '\\Field',
      'PicklistEntry' => '\\PicklistEntry',
      'DescribeDataCategoryGroupResult' => '\\DescribeDataCategoryGroupResult',
      'DescribeDataCategoryGroupStructureResult' => '\\DescribeDataCategoryGroupStructureResult',
      'DataCategoryGroupSobjectTypePair' => '\\DataCategoryGroupSobjectTypePair',
      'DataCategory' => '\\DataCategory',
      'KnowledgeSettings' => '\\KnowledgeSettings',
      'KnowledgeLanguageItem' => '\\KnowledgeLanguageItem',
      'FieldDiff' => '\\FieldDiff',
      'AdditionalInformationMap' => '\\AdditionalInformationMap',
      'MatchRecord' => '\\MatchRecord',
      'MatchResult' => '\\MatchResult',
      'DuplicateResult' => '\\DuplicateResult',
      'DuplicateError' => '\\DuplicateError',
      'DescribeNounResult' => '\\DescribeNounResult',
      'NameCaseValue' => '\\NameCaseValue',
      'DescribeFlexiPageResult' => '\\DescribeFlexiPageResult',
      'DescribeFlexiPageRegion' => '\\DescribeFlexiPageRegion',
      'DescribeComponentInstance' => '\\DescribeComponentInstance',
      'DescribeComponentInstanceProperty' => '\\DescribeComponentInstanceProperty',
      'FlexipageContext' => '\\FlexipageContext',
      'DescribeAppMenuResult' => '\\DescribeAppMenuResult',
      'DescribeAppMenuItem' => '\\DescribeAppMenuItem',
      'DescribeThemeResult' => '\\DescribeThemeResult',
      'DescribeThemeItem' => '\\DescribeThemeItem',
      'DescribeSoftphoneLayoutResult' => '\\DescribeSoftphoneLayoutResult',
      'DescribeSoftphoneLayoutCallType' => '\\DescribeSoftphoneLayoutCallType',
      'DescribeSoftphoneScreenPopOption' => '\\DescribeSoftphoneScreenPopOption',
      'DescribeSoftphoneLayoutInfoField' => '\\DescribeSoftphoneLayoutInfoField',
      'DescribeSoftphoneLayoutSection' => '\\DescribeSoftphoneLayoutSection',
      'DescribeSoftphoneLayoutItem' => '\\DescribeSoftphoneLayoutItem',
      'DescribeCompactLayoutsResult' => '\\DescribeCompactLayoutsResult',
      'DescribeCompactLayout' => '\\DescribeCompactLayout',
      'RecordTypeCompactLayoutMapping' => '\\RecordTypeCompactLayoutMapping',
      'DescribePathAssistantsResult' => '\\DescribePathAssistantsResult',
      'DescribePathAssistant' => '\\DescribePathAssistant',
      'DescribePathAssistantStep' => '\\DescribePathAssistantStep',
      'DescribePathAssistantField' => '\\DescribePathAssistantField',
      'DescribeApprovalLayoutResult' => '\\DescribeApprovalLayoutResult',
      'DescribeApprovalLayout' => '\\DescribeApprovalLayout',
      'DescribeLayoutResult' => '\\DescribeLayoutResult',
      'DescribeLayout' => '\\DescribeLayout',
      'DescribeQuickActionListResult' => '\\DescribeQuickActionListResult',
      'DescribeQuickActionListItemResult' => '\\DescribeQuickActionListItemResult',
      'DescribeLayoutFeedView' => '\\DescribeLayoutFeedView',
      'DescribeLayoutFeedFilter' => '\\DescribeLayoutFeedFilter',
      'DescribeLayoutSection' => '\\DescribeLayoutSection',
      'DescribeLayoutButtonSection' => '\\DescribeLayoutButtonSection',
      'DescribeLayoutRow' => '\\DescribeLayoutRow',
      'DescribeLayoutItem' => '\\DescribeLayoutItem',
      'DescribeLayoutButton' => '\\DescribeLayoutButton',
      'DescribeLayoutComponent' => '\\DescribeLayoutComponent',
      'FieldComponent' => '\\FieldComponent',
      'FieldLayoutComponent' => '\\FieldLayoutComponent',
      'VisualforcePage' => '\\VisualforcePage',
      'Canvas' => '\\Canvas',
      'ReportChartComponent' => '\\ReportChartComponent',
      'AnalyticsCloudComponent' => '\\AnalyticsCloudComponent',
      'CustomLinkComponent' => '\\CustomLinkComponent',
      'NamedLayoutInfo' => '\\NamedLayoutInfo',
      'RecordTypeInfo' => '\\RecordTypeInfo',
      'RecordTypeMapping' => '\\RecordTypeMapping',
      'PicklistForRecordType' => '\\PicklistForRecordType',
      'RelatedContent' => '\\RelatedContent',
      'DescribeRelatedContentItem' => '\\DescribeRelatedContentItem',
      'RelatedList' => '\\RelatedList',
      'RelatedListColumn' => '\\RelatedListColumn',
      'RelatedListSort' => '\\RelatedListSort',
      'EmailFileAttachment' => '\\EmailFileAttachment',
      'Email' => '\\Email',
      'MassEmailMessage' => '\\MassEmailMessage',
      'SingleEmailMessage' => '\\SingleEmailMessage',
      'SendEmailResult' => '\\SendEmailResult',
      'ListViewColumn' => '\\ListViewColumn',
      'ListViewOrderBy' => '\\ListViewOrderBy',
      'DescribeSoqlListView' => '\\DescribeSoqlListView',
      'DescribeSoqlListViewsRequest' => '\\DescribeSoqlListViewsRequest',
      'DescribeSoqlListViewParams' => '\\DescribeSoqlListViewParams',
      'DescribeSoqlListViewResult' => '\\DescribeSoqlListViewResult',
      'ExecuteListViewRequest' => '\\ExecuteListViewRequest',
      'ExecuteListViewResult' => '\\ExecuteListViewResult',
      'ListViewRecord' => '\\ListViewRecord',
      'ListViewRecordColumn' => '\\ListViewRecordColumn',
      'SoqlWhereCondition' => '\\SoqlWhereCondition',
      'SoqlCondition' => '\\SoqlCondition',
      'SoqlNotCondition' => '\\SoqlNotCondition',
      'SoqlConditionGroup' => '\\SoqlConditionGroup',
      'SoqlSubQueryCondition' => '\\SoqlSubQueryCondition',
      'DescribeSearchLayoutResult' => '\\DescribeSearchLayoutResult',
      'DescribeColumn' => '\\DescribeColumn',
      'DescribeSearchScopeOrderResult' => '\\DescribeSearchScopeOrderResult',
      'DescribeTabSetResult' => '\\DescribeTabSetResult',
      'DescribeTab' => '\\DescribeTab',
      'DescribeColor' => '\\DescribeColor',
      'DescribeIcon' => '\\DescribeIcon',
      'ActionOverride' => '\\ActionOverride',
      'login' => '\\login',
      'loginResponse' => '\\loginResponse',
      'describeSObject' => '\\describeSObject',
      'describeSObjectResponse' => '\\describeSObjectResponse',
      'describeSObjects' => '\\describeSObjects',
      'describeSObjectsResponse' => '\\describeSObjectsResponse',
      'describeGlobal' => '\\describeGlobal',
      'describeGlobalResponse' => '\\describeGlobalResponse',
      'describeGlobalTheme' => '\\describeGlobalTheme',
      'describeGlobalThemeResponse' => '\\describeGlobalThemeResponse',
      'describeTheme' => '\\describeTheme',
      'describeThemeResponse' => '\\describeThemeResponse',
      'describeDataCategoryGroups' => '\\describeDataCategoryGroups',
      'describeDataCategoryGroupsResponse' => '\\describeDataCategoryGroupsResponse',
      'describeDataCategoryGroupStructures' => '\\describeDataCategoryGroupStructures',
      'describeDataCategoryGroupStructuresResponse' => '\\describeDataCategoryGroupStructuresResponse',
      'describeKnowledgeSettings' => '\\describeKnowledgeSettings',
      'describeKnowledgeSettingsResponse' => '\\describeKnowledgeSettingsResponse',
      'describeFlexiPages' => '\\describeFlexiPages',
      'describeFlexiPagesResponse' => '\\describeFlexiPagesResponse',
      'describeAppMenu' => '\\describeAppMenu',
      'describeAppMenuResponse' => '\\describeAppMenuResponse',
      'describeLayout' => '\\describeLayout',
      'describeLayoutResponse' => '\\describeLayoutResponse',
      'describeCompactLayouts' => '\\describeCompactLayouts',
      'describeCompactLayoutsResponse' => '\\describeCompactLayoutsResponse',
      'describePrimaryCompactLayouts' => '\\describePrimaryCompactLayouts',
      'describePrimaryCompactLayoutsResponse' => '\\describePrimaryCompactLayoutsResponse',
      'describePathAssistants' => '\\describePathAssistants',
      'describePathAssistantsResponse' => '\\describePathAssistantsResponse',
      'describeApprovalLayout' => '\\describeApprovalLayout',
      'describeApprovalLayoutResponse' => '\\describeApprovalLayoutResponse',
      'describeSoftphoneLayout' => '\\describeSoftphoneLayout',
      'describeSoftphoneLayoutResponse' => '\\describeSoftphoneLayoutResponse',
      'describeSoqlListViews' => '\\describeSoqlListViews',
      'describeSoqlListViewsResponse' => '\\describeSoqlListViewsResponse',
      'executeListView' => '\\executeListView',
      'executeListViewResponse' => '\\executeListViewResponse',
      'describeSObjectListViews' => '\\describeSObjectListViews',
      'describeSObjectListViewsResponse' => '\\describeSObjectListViewsResponse',
      'describeSearchLayouts' => '\\describeSearchLayouts',
      'describeSearchLayoutsResponse' => '\\describeSearchLayoutsResponse',
      'describeSearchScopeOrder' => '\\describeSearchScopeOrder',
      'describeSearchScopeOrderResponse' => '\\describeSearchScopeOrderResponse',
      'describeTabs' => '\\describeTabs',
      'describeTabsResponse' => '\\describeTabsResponse',
      'describeAllTabs' => '\\describeAllTabs',
      'describeAllTabsResponse' => '\\describeAllTabsResponse',
      'describeNouns' => '\\describeNouns',
      'describeNounsResponse' => '\\describeNounsResponse',
      'create' => '\\create',
      'createResponse' => '\\createResponse',
      'sendEmail' => '\\sendEmail',
      'sendEmailResponse' => '\\sendEmailResponse',
      'RenderEmailTemplateRequest' => '\\RenderEmailTemplateRequest',
      'RenderEmailTemplateBodyResult' => '\\RenderEmailTemplateBodyResult',
      'RenderEmailTemplateResult' => '\\RenderEmailTemplateResult',
      'renderEmailTemplate' => '\\renderEmailTemplate',
      'renderEmailTemplateResponse' => '\\renderEmailTemplateResponse',
      'sendEmailMessage' => '\\sendEmailMessage',
      'sendEmailMessageResponse' => '\\sendEmailMessageResponse',
      'update' => '\\update',
      'updateResponse' => '\\updateResponse',
      'upsert' => '\\upsert',
      'upsertResponse' => '\\upsertResponse',
      'merge' => '\\merge',
      'mergeResponse' => '\\mergeResponse',
      'delete' => '\\delete',
      'deleteResponse' => '\\deleteResponse',
      'undelete' => '\\undelete',
      'undeleteResponse' => '\\undeleteResponse',
      'emptyRecycleBin' => '\\emptyRecycleBin',
      'emptyRecycleBinResponse' => '\\emptyRecycleBinResponse',
      'process' => '\\process',
      'processResponse' => '\\processResponse',
      'performQuickActions' => '\\performQuickActions',
      'performQuickActionsResponse' => '\\performQuickActionsResponse',
      'retrieveQuickActionTemplates' => '\\retrieveQuickActionTemplates',
      'retrieveQuickActionTemplatesResponse' => '\\retrieveQuickActionTemplatesResponse',
      'describeQuickActions' => '\\describeQuickActions',
      'describeQuickActionsResponse' => '\\describeQuickActionsResponse',
      'describeAvailableQuickActions' => '\\describeAvailableQuickActions',
      'describeAvailableQuickActionsResponse' => '\\describeAvailableQuickActionsResponse',
      'retrieve' => '\\retrieve',
      'retrieveResponse' => '\\retrieveResponse',
      'convertLead' => '\\convertLead',
      'convertLeadResponse' => '\\convertLeadResponse',
      'getUpdated' => '\\getUpdated',
      'getUpdatedResponse' => '\\getUpdatedResponse',
      'getDeleted' => '\\getDeleted',
      'getDeletedResponse' => '\\getDeletedResponse',
      'logout' => '\\logout',
      'logoutResponse' => '\\logoutResponse',
      'invalidateSessions' => '\\invalidateSessions',
      'invalidateSessionsResponse' => '\\invalidateSessionsResponse',
      'query' => '\\query',
      'queryResponse' => '\\queryResponse',
      'queryAll' => '\\queryAll',
      'queryAllResponse' => '\\queryAllResponse',
      'queryMore' => '\\queryMore',
      'queryMoreResponse' => '\\queryMoreResponse',
      'search' => '\\search',
      'searchResponse' => '\\searchResponse',
      'getServerTimestamp' => '\\getServerTimestamp',
      'getServerTimestampResponse' => '\\getServerTimestampResponse',
      'setPassword' => '\\setPassword',
      'setPasswordResponse' => '\\setPasswordResponse',
      'resetPassword' => '\\resetPassword',
      'resetPasswordResponse' => '\\resetPasswordResponse',
      'getUserInfo' => '\\getUserInfo',
      'getUserInfoResponse' => '\\getUserInfoResponse',
      'SessionHeader' => '\\SessionHeader',
      'LoginScopeHeader' => '\\LoginScopeHeader',
      'CallOptions' => '\\CallOptions',
      'QueryOptions' => '\\QueryOptions',
      'DebuggingHeader' => '\\DebuggingHeader',
      'DebuggingInfo' => '\\DebuggingInfo',
      'PackageVersion' => '\\PackageVersion',
      'PackageVersionHeader' => '\\PackageVersionHeader',
      'AllowFieldTruncationHeader' => '\\AllowFieldTruncationHeader',
      'DisableFeedTrackingHeader' => '\\DisableFeedTrackingHeader',
      'StreamingEnabledHeader' => '\\StreamingEnabledHeader',
      'AllOrNoneHeader' => '\\AllOrNoneHeader',
      'DuplicateRuleHeader' => '\\DuplicateRuleHeader',
      'LimitInfo' => '\\LimitInfo',
      'LimitInfoHeader' => '\\LimitInfoHeader',
      'MruHeader' => '\\MruHeader',
      'EmailHeader' => '\\EmailHeader',
      'AssignmentRuleHeader' => '\\AssignmentRuleHeader',
      'UserTerritoryDeleteHeader' => '\\UserTerritoryDeleteHeader',
      'LocaleOptions' => '\\LocaleOptions',
      'OwnerChangeOption' => '\\OwnerChangeOption',
      'OwnerChangeOptions' => '\\OwnerChangeOptions',
      'ApiFault' => '\\ApiFault',
      'ApiQueryFault' => '\\ApiQueryFault',
      'LoginFault' => '\\LoginFault',
      'InvalidQueryLocatorFault' => '\\InvalidQueryLocatorFault',
      'InvalidNewPasswordFault' => '\\InvalidNewPasswordFault',
      'InvalidIdFault' => '\\InvalidIdFault',
      'UnexpectedErrorFault' => '\\UnexpectedErrorFault',
      'InvalidFieldFault' => '\\InvalidFieldFault',
      'InvalidSObjectFault' => '\\InvalidSObjectFault',
      'MalformedQueryFault' => '\\MalformedQueryFault',
      'MalformedSearchFault' => '\\MalformedSearchFault',
    );

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     */
    public function __construct(array $options = array(), $wsdl = null)
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      $options = array_merge(array (
      'features' => 1,
    ), $options);
      if (!$wsdl) {
        $wsdl = DRUPAL_ROOT . '/' . variable_get('salesforce_api_filename_wsdl');
      }
      parent::__construct($wsdl, $options);
    }

    /**
     * Login to the Salesforce.com SOAP Api
     *
     * @param login $parameters
     * @return loginResponse
     */
    public function login(login $parameters)
    {
      return $this->__soapCall('login', array($parameters));
    }

    /**
     * Describe an sObject
     *
     * @param describeSObject $parameters
     * @return describeSObjectResponse
     */
    public function describeSObject(describeSObject $parameters)
    {
      return $this->__soapCall('describeSObject', array($parameters));
    }

    /**
     * Describe multiple sObjects (upto 100)
     *
     * @param describeSObjects $parameters
     * @return describeSObjectsResponse
     */
    public function describeSObjects(describeSObjects $parameters)
    {
      return $this->__soapCall('describeSObjects', array($parameters));
    }

    /**
     * Describe the Global state
     *
     * @param describeGlobal $parameters
     * @return describeGlobalResponse
     */
    public function describeGlobal(describeGlobal $parameters)
    {
      return $this->__soapCall('describeGlobal', array($parameters));
    }

    /**
     * Describe all the data category groups available for a given set of types
     *
     * @param describeDataCategoryGroups $parameters
     * @return describeDataCategoryGroupsResponse
     */
    public function describeDataCategoryGroups(describeDataCategoryGroups $parameters)
    {
      return $this->__soapCall('describeDataCategoryGroups', array($parameters));
    }

    /**
     * Describe the data category group structures for a given set of pair of types and data category group name
     *
     * @param describeDataCategoryGroupStructures $parameters
     * @return describeDataCategoryGroupStructuresResponse
     */
    public function describeDataCategoryGroupStructures(describeDataCategoryGroupStructures $parameters)
    {
      return $this->__soapCall('describeDataCategoryGroupStructures', array($parameters));
    }

    /**
     * Describes your Knowledge settings, such as if knowledgeEnabled is on or off, its default language and supported languages
     *
     * @param describeKnowledgeSettings $parameters
     * @return describeKnowledgeSettingsResponse
     */
    public function describeKnowledgeSettings(describeKnowledgeSettings $parameters)
    {
      return $this->__soapCall('describeKnowledgeSettings', array($parameters));
    }

    /**
     * Describe a list of FlexiPage and their contents
     *
     * @param describeFlexiPages $parameters
     * @return describeFlexiPagesResponse
     */
    public function describeFlexiPages(describeFlexiPages $parameters)
    {
      return $this->__soapCall('describeFlexiPages', array($parameters));
    }

    /**
     * Describe the items in an AppMenu
     *
     * @param describeAppMenu $parameters
     * @return describeAppMenuResponse
     */
    public function describeAppMenu(describeAppMenu $parameters)
    {
      return $this->__soapCall('describeAppMenu', array($parameters));
    }

    /**
     * Describe Gloal and Themes
     *
     * @param describeGlobalTheme $parameters
     * @return describeGlobalThemeResponse
     */
    public function describeGlobalTheme(describeGlobalTheme $parameters)
    {
      return $this->__soapCall('describeGlobalTheme', array($parameters));
    }

    /**
     * Describe Themes
     *
     * @param describeTheme $parameters
     * @return describeThemeResponse
     */
    public function describeTheme(describeTheme $parameters)
    {
      return $this->__soapCall('describeTheme', array($parameters));
    }

    /**
     * Describe the layout of the given sObject or the given actionable global page.
     *
     * @param describeLayout $parameters
     * @return describeLayoutResponse
     */
    public function describeLayout(describeLayout $parameters)
    {
      return $this->__soapCall('describeLayout', array($parameters));
    }

    /**
     * Describe the layout of the SoftPhone
     *
     * @param describeSoftphoneLayout $parameters
     * @return describeSoftphoneLayoutResponse
     */
    public function describeSoftphoneLayout(describeSoftphoneLayout $parameters)
    {
      return $this->__soapCall('describeSoftphoneLayout', array($parameters));
    }

    /**
     * Describe the search view of an sObject
     *
     * @param describeSearchLayouts $parameters
     * @return describeSearchLayoutsResponse
     */
    public function describeSearchLayouts(describeSearchLayouts $parameters)
    {
      return $this->__soapCall('describeSearchLayouts', array($parameters));
    }

    /**
     * Describe a list of objects representing the order and scope of objects on a users search result page
     *
     * @param describeSearchScopeOrder $parameters
     * @return describeSearchScopeOrderResponse
     */
    public function describeSearchScopeOrder(describeSearchScopeOrder $parameters)
    {
      return $this->__soapCall('describeSearchScopeOrder', array($parameters));
    }

    /**
     * Describe the compact layouts of the given sObject
     *
     * @param describeCompactLayouts $parameters
     * @return describeCompactLayoutsResponse
     */
    public function describeCompactLayouts(describeCompactLayouts $parameters)
    {
      return $this->__soapCall('describeCompactLayouts', array($parameters));
    }

    /**
     * Describe the Path Assistants for the given sObject and optionally RecordTypes
     *
     * @param describePathAssistants $parameters
     * @return describePathAssistantsResponse
     */
    public function describePathAssistants(describePathAssistants $parameters)
    {
      return $this->__soapCall('describePathAssistants', array($parameters));
    }

    /**
     * Describe the approval layouts of the given sObject
     *
     * @param describeApprovalLayout $parameters
     * @return describeApprovalLayoutResponse
     */
    public function describeApprovalLayout(describeApprovalLayout $parameters)
    {
      return $this->__soapCall('describeApprovalLayout', array($parameters));
    }

    /**
     * Describe the ListViews as SOQL metadata for the generation of SOQL.
     *
     * @param describeSoqlListViews $parameters
     * @return describeSoqlListViewsResponse
     */
    public function describeSoqlListViews(describeSoqlListViews $parameters)
    {
      return $this->__soapCall('describeSoqlListViews', array($parameters));
    }

    /**
     * Execute the specified list view and return the presentation-ready results.
     *
     * @param executeListView $parameters
     * @return executeListViewResponse
     */
    public function executeListView(executeListView $parameters)
    {
      return $this->__soapCall('executeListView', array($parameters));
    }

    /**
     * Describe the ListViews of a SObject as SOQL metadata for the generation of SOQL.
     *
     * @param describeSObjectListViews $parameters
     * @return describeSObjectListViewsResponse
     */
    public function describeSObjectListViews(describeSObjectListViews $parameters)
    {
      return $this->__soapCall('describeSObjectListViews', array($parameters));
    }

    /**
     * Describe the tabs that appear on a users page
     *
     * @param describeTabs $parameters
     * @return describeTabsResponse
     */
    public function describeTabs(describeTabs $parameters)
    {
      return $this->__soapCall('describeTabs', array($parameters));
    }

    /**
     * Describe all tabs available to a user
     *
     * @param describeAllTabs $parameters
     * @return describeAllTabsResponse
     */
    public function describeAllTabs(describeAllTabs $parameters)
    {
      return $this->__soapCall('describeAllTabs', array($parameters));
    }

    /**
     * Describe the primary compact layouts for the sObjects requested
     *
     * @param describePrimaryCompactLayouts $parameters
     * @return describePrimaryCompactLayoutsResponse
     */
    public function describePrimaryCompactLayouts(describePrimaryCompactLayouts $parameters)
    {
      return $this->__soapCall('describePrimaryCompactLayouts', array($parameters));
    }

    /**
     * Create a set of new sObjects
     *
     * @param create $parameters
     * @return createResponse
     */
    public function create(create $parameters)
    {
      return $this->__soapCall('create', array($parameters));
    }

    /**
     * Update a set of sObjects
     *
     * @param update $parameters
     * @return updateResponse
     */
    public function update(update $parameters)
    {
      return $this->__soapCall('update', array($parameters));
    }

    /**
     * Update or insert a set of sObjects based on object id
     *
     * @param upsert $parameters
     * @return upsertResponse
     */
    public function upsert(upsert $parameters)
    {
      return $this->__soapCall('upsert', array($parameters));
    }

    /**
     * Merge and update a set of sObjects based on object id
     *
     * @param merge $parameters
     * @return mergeResponse
     */
    public function merge(merge $parameters)
    {
      return $this->__soapCall('merge', array($parameters));
    }

    /**
     * Delete a set of sObjects
     *
     * @param delete $parameters
     * @return deleteResponse
     */
    public function delete(delete $parameters)
    {
      return $this->__soapCall('delete', array($parameters));
    }

    /**
     * Undelete a set of sObjects
     *
     * @param undelete $parameters
     * @return undeleteResponse
     */
    public function undelete(undelete $parameters)
    {
      return $this->__soapCall('undelete', array($parameters));
    }

    /**
     * Empty a set of sObjects from the recycle bin
     *
     * @param emptyRecycleBin $parameters
     * @return emptyRecycleBinResponse
     */
    public function emptyRecycleBin(emptyRecycleBin $parameters)
    {
      return $this->__soapCall('emptyRecycleBin', array($parameters));
    }

    /**
     * Get a set of sObjects
     *
     * @param retrieve $parameters
     * @return retrieveResponse
     */
    public function retrieve(retrieve $parameters)
    {
      return $this->__soapCall('retrieve', array($parameters));
    }

    /**
     * Submit an entity to a workflow process or process a workitem
     *
     * @param process $parameters
     * @return processResponse
     */
    public function process(process $parameters)
    {
      return $this->__soapCall('process', array($parameters));
    }

    /**
     * convert a set of leads
     *
     * @param convertLead $parameters
     * @return convertLeadResponse
     */
    public function convertLead(convertLead $parameters)
    {
      return $this->__soapCall('convertLead', array($parameters));
    }

    /**
     * Logout the current user, invalidating the current session.
     *
     * @param logout $parameters
     * @return logoutResponse
     */
    public function logout(logout $parameters)
    {
      return $this->__soapCall('logout', array($parameters));
    }

    /**
     * Logs out and invalidates session ids
     *
     * @param invalidateSessions $parameters
     * @return invalidateSessionsResponse
     */
    public function invalidateSessions(invalidateSessions $parameters)
    {
      return $this->__soapCall('invalidateSessions', array($parameters));
    }

    /**
     * Get the IDs for deleted sObjects
     *
     * @param getDeleted $parameters
     * @return getDeletedResponse
     */
    public function getDeleted(getDeleted $parameters)
    {
      return $this->__soapCall('getDeleted', array($parameters));
    }

    /**
     * Get the IDs for updated sObjects
     *
     * @param getUpdated $parameters
     * @return getUpdatedResponse
     */
    public function getUpdated(getUpdated $parameters)
    {
      return $this->__soapCall('getUpdated', array($parameters));
    }

    /**
     * Create a Query Cursor
     *
     * @param query $parameters
     * @return queryResponse
     */
    public function query(query $parameters)
    {
      return $this->__soapCall('query', array($parameters));
    }

    /**
     * Create a Query Cursor, including deleted sObjects
     *
     * @param queryAll $parameters
     * @return queryAllResponse
     */
    public function queryAll(queryAll $parameters)
    {
      return $this->__soapCall('queryAll', array($parameters));
    }

    /**
     * Gets the next batch of sObjects from a query
     *
     * @param queryMore $parameters
     * @return queryMoreResponse
     */
    public function queryMore(queryMore $parameters)
    {
      return $this->__soapCall('queryMore', array($parameters));
    }

    /**
     * Search for sObjects
     *
     * @param search $parameters
     * @return searchResponse
     */
    public function search(search $parameters)
    {
      return $this->__soapCall('search', array($parameters));
    }

    /**
     * Gets server timestamp
     *
     * @param getServerTimestamp $parameters
     * @return getServerTimestampResponse
     */
    public function getServerTimestamp(getServerTimestamp $parameters)
    {
      return $this->__soapCall('getServerTimestamp', array($parameters));
    }

    /**
     * Set a user's password
     *
     * @param setPassword $parameters
     * @return setPasswordResponse
     */
    public function setPassword(setPassword $parameters)
    {
      return $this->__soapCall('setPassword', array($parameters));
    }

    /**
     * Reset a user's password
     *
     * @param resetPassword $parameters
     * @return resetPasswordResponse
     */
    public function resetPassword(resetPassword $parameters)
    {
      return $this->__soapCall('resetPassword', array($parameters));
    }

    /**
     * Returns standard information relevant to the current user
     *
     * @param getUserInfo $parameters
     * @return getUserInfoResponse
     */
    public function getUserInfo(getUserInfo $parameters)
    {
      return $this->__soapCall('getUserInfo', array($parameters));
    }

    /**
     * Send existing draft EmailMessage
     *
     * @param sendEmailMessage $parameters
     * @return sendEmailMessageResponse
     */
    public function sendEmailMessage(sendEmailMessage $parameters)
    {
      return $this->__soapCall('sendEmailMessage', array($parameters));
    }

    /**
     * Send outbound email
     *
     * @param sendEmail $parameters
     * @return sendEmailResponse
     */
    public function sendEmail(sendEmail $parameters)
    {
      return $this->__soapCall('sendEmail', array($parameters));
    }

    /**
     * Perform a template merge on one or more blocks of text. Optionally, just validate the template text.
     *
     * @param renderEmailTemplate $parameters
     * @return renderEmailTemplateResponse
     */
    public function renderEmailTemplate(renderEmailTemplate $parameters)
    {
      return $this->__soapCall('renderEmailTemplate', array($parameters));
    }

    /**
     * Perform a series of predefined actions such as quick create or log a task
     *
     * @param performQuickActions $parameters
     * @return performQuickActionsResponse
     */
    public function performQuickActions(performQuickActions $parameters)
    {
      return $this->__soapCall('performQuickActions', array($parameters));
    }

    /**
     * Describe the details of a series of quick actions
     *
     * @param describeQuickActions $parameters
     * @return describeQuickActionsResponse
     */
    public function describeQuickActions(describeQuickActions $parameters)
    {
      return $this->__soapCall('describeQuickActions', array($parameters));
    }

    /**
     * Describe the details of a series of quick actions available for the given contextType
     *
     * @param describeAvailableQuickActions $parameters
     * @return describeAvailableQuickActionsResponse
     */
    public function describeAvailableQuickActions(describeAvailableQuickActions $parameters)
    {
      return $this->__soapCall('describeAvailableQuickActions', array($parameters));
    }

    /**
     * Retreive the template sobjects, if appropriate, for the given quick action names in a given context
     *
     * @param retrieveQuickActionTemplates $parameters
     * @return retrieveQuickActionTemplatesResponse
     */
    public function retrieveQuickActionTemplates(retrieveQuickActionTemplates $parameters)
    {
      return $this->__soapCall('retrieveQuickActionTemplates', array($parameters));
    }

    /**
     * Return the renameable nouns from the server for use in presentation using the salesforce grammar engine
     *
     * @param describeNouns $parameters
     * @return describeNounsResponse
     */
    public function describeNouns(describeNouns $parameters)
    {
      return $this->__soapCall('describeNouns', array($parameters));
    }

}
