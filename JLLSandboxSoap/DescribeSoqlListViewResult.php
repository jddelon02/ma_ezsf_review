<?php

class DescribeSoqlListViewResult
{

    /**
     * @var DescribeSoqlListView[] $describeSoqlListViews
     */
    protected $describeSoqlListViews = null;

    /**
     * @param DescribeSoqlListView[] $describeSoqlListViews
     */
    public function __construct(array $describeSoqlListViews)
    {
      $this->describeSoqlListViews = $describeSoqlListViews;
    }

    /**
     * @return DescribeSoqlListView[]
     */
    public function getDescribeSoqlListViews()
    {
      return $this->describeSoqlListViews;
    }

    /**
     * @param DescribeSoqlListView[] $describeSoqlListViews
     * @return DescribeSoqlListViewResult
     */
    public function setDescribeSoqlListViews(array $describeSoqlListViews)
    {
      $this->describeSoqlListViews = $describeSoqlListViews;
      return $this;
    }

}
