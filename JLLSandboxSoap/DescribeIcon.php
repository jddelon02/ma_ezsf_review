<?php

class DescribeIcon
{

    /**
     * @var string $contentType
     */
    protected $contentType = null;

    /**
     * @var int $height
     */
    protected $height = null;

    /**
     * @var string $theme
     */
    protected $theme = null;

    /**
     * @var string $url
     */
    protected $url = null;

    /**
     * @var int $width
     */
    protected $width = null;

    /**
     * @param string $contentType
     * @param string $theme
     * @param string $url
     */
    public function __construct($contentType, $theme, $url)
    {
      $this->contentType = $contentType;
      $this->theme = $theme;
      $this->url = $url;
    }

    /**
     * @return string
     */
    public function getContentType()
    {
      return $this->contentType;
    }

    /**
     * @param string $contentType
     * @return DescribeIcon
     */
    public function setContentType($contentType)
    {
      $this->contentType = $contentType;
      return $this;
    }

    /**
     * @return int
     */
    public function getHeight()
    {
      return $this->height;
    }

    /**
     * @param int $height
     * @return DescribeIcon
     */
    public function setHeight($height)
    {
      $this->height = $height;
      return $this;
    }

    /**
     * @return string
     */
    public function getTheme()
    {
      return $this->theme;
    }

    /**
     * @param string $theme
     * @return DescribeIcon
     */
    public function setTheme($theme)
    {
      $this->theme = $theme;
      return $this;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
      return $this->url;
    }

    /**
     * @param string $url
     * @return DescribeIcon
     */
    public function setUrl($url)
    {
      $this->url = $url;
      return $this;
    }

    /**
     * @return int
     */
    public function getWidth()
    {
      return $this->width;
    }

    /**
     * @param int $width
     * @return DescribeIcon
     */
    public function setWidth($width)
    {
      $this->width = $width;
      return $this;
    }

}
