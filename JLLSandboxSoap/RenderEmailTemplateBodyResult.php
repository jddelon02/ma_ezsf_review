<?php

class RenderEmailTemplateBodyResult
{

    /**
     * @var RenderEmailTemplateError[] $errors
     */
    protected $errors = null;

    /**
     * @var string $mergedBody
     */
    protected $mergedBody = null;

    /**
     * @var boolean $success
     */
    protected $success = null;

    /**
     * @param boolean $success
     */
    public function __construct($success)
    {
      $this->success = $success;
    }

    /**
     * @return RenderEmailTemplateError[]
     */
    public function getErrors()
    {
      return $this->errors;
    }

    /**
     * @param RenderEmailTemplateError[] $errors
     * @return RenderEmailTemplateBodyResult
     */
    public function setErrors(array $errors = null)
    {
      $this->errors = $errors;
      return $this;
    }

    /**
     * @return string
     */
    public function getMergedBody()
    {
      return $this->mergedBody;
    }

    /**
     * @param string $mergedBody
     * @return RenderEmailTemplateBodyResult
     */
    public function setMergedBody($mergedBody)
    {
      $this->mergedBody = $mergedBody;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getSuccess()
    {
      return $this->success;
    }

    /**
     * @param boolean $success
     * @return RenderEmailTemplateBodyResult
     */
    public function setSuccess($success)
    {
      $this->success = $success;
      return $this;
    }

}
