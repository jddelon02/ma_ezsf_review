<?php

class DescribeFlexiPageRegion
{

    /**
     * @var DescribeComponentInstance[] $components
     */
    protected $components = null;

    /**
     * @var string $name
     */
    protected $name = null;

    /**
     * @param string $name
     */
    public function __construct($name)
    {
      $this->name = $name;
    }

    /**
     * @return DescribeComponentInstance[]
     */
    public function getComponents()
    {
      return $this->components;
    }

    /**
     * @param DescribeComponentInstance[] $components
     * @return DescribeFlexiPageRegion
     */
    public function setComponents(array $components = null)
    {
      $this->components = $components;
      return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
      return $this->name;
    }

    /**
     * @param string $name
     * @return DescribeFlexiPageRegion
     */
    public function setName($name)
    {
      $this->name = $name;
      return $this;
    }

}
