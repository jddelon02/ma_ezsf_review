<?php

class getServerTimestampResponse
{

    /**
     * @var GetServerTimestampResult $result
     */
    protected $result = null;

    /**
     * @param GetServerTimestampResult $result
     */
    public function __construct($result)
    {
      $this->result = $result;
    }

    /**
     * @return GetServerTimestampResult
     */
    public function getResult()
    {
      return $this->result;
    }

    /**
     * @param GetServerTimestampResult $result
     * @return getServerTimestampResponse
     */
    public function setResult($result)
    {
      $this->result = $result;
      return $this;
    }

}
