<?php

class describeLayout
{

    /**
     * @var string $sObjectType
     */
    protected $sObjectType = null;

    /**
     * @var string $layoutName
     */
    protected $layoutName = null;

    /**
     * @var ID $recordTypeIds
     */
    protected $recordTypeIds = null;

    /**
     * @param string $sObjectType
     * @param string $layoutName
     * @param ID $recordTypeIds
     */
    public function __construct($sObjectType, $layoutName, $recordTypeIds)
    {
      $this->sObjectType = $sObjectType;
      $this->layoutName = $layoutName;
      $this->recordTypeIds = $recordTypeIds;
    }

    /**
     * @return string
     */
    public function getSObjectType()
    {
      return $this->sObjectType;
    }

    /**
     * @param string $sObjectType
     * @return describeLayout
     */
    public function setSObjectType($sObjectType)
    {
      $this->sObjectType = $sObjectType;
      return $this;
    }

    /**
     * @return string
     */
    public function getLayoutName()
    {
      return $this->layoutName;
    }

    /**
     * @param string $layoutName
     * @return describeLayout
     */
    public function setLayoutName($layoutName)
    {
      $this->layoutName = $layoutName;
      return $this;
    }

    /**
     * @return ID
     */
    public function getRecordTypeIds()
    {
      return $this->recordTypeIds;
    }

    /**
     * @param ID $recordTypeIds
     * @return describeLayout
     */
    public function setRecordTypeIds($recordTypeIds)
    {
      $this->recordTypeIds = $recordTypeIds;
      return $this;
    }

}
