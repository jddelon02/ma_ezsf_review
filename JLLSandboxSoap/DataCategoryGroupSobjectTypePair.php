<?php

class DataCategoryGroupSobjectTypePair
{

    /**
     * @var string $dataCategoryGroupName
     */
    protected $dataCategoryGroupName = null;

    /**
     * @var string $sobject
     */
    protected $sobject = null;

    /**
     * @param string $dataCategoryGroupName
     * @param string $sobject
     */
    public function __construct($dataCategoryGroupName, $sobject)
    {
      $this->dataCategoryGroupName = $dataCategoryGroupName;
      $this->sobject = $sobject;
    }

    /**
     * @return string
     */
    public function getDataCategoryGroupName()
    {
      return $this->dataCategoryGroupName;
    }

    /**
     * @param string $dataCategoryGroupName
     * @return DataCategoryGroupSobjectTypePair
     */
    public function setDataCategoryGroupName($dataCategoryGroupName)
    {
      $this->dataCategoryGroupName = $dataCategoryGroupName;
      return $this;
    }

    /**
     * @return string
     */
    public function getSobject()
    {
      return $this->sobject;
    }

    /**
     * @param string $sobject
     * @return DataCategoryGroupSobjectTypePair
     */
    public function setSobject($sobject)
    {
      $this->sobject = $sobject;
      return $this;
    }

}
