<?php

class invalidateSessions
{

    /**
     * @var string $sessionIds
     */
    protected $sessionIds = null;

    /**
     * @param string $sessionIds
     */
    public function __construct($sessionIds)
    {
      $this->sessionIds = $sessionIds;
    }

    /**
     * @return string
     */
    public function getSessionIds()
    {
      return $this->sessionIds;
    }

    /**
     * @param string $sessionIds
     * @return invalidateSessions
     */
    public function setSessionIds($sessionIds)
    {
      $this->sessionIds = $sessionIds;
      return $this;
    }

}
