<?php

class describeNouns
{

    /**
     * @var string $nouns
     */
    protected $nouns = null;

    /**
     * @var boolean $onlyRenamed
     */
    protected $onlyRenamed = null;

    /**
     * @var boolean $includeFields
     */
    protected $includeFields = null;

    /**
     * @param string $nouns
     * @param boolean $onlyRenamed
     * @param boolean $includeFields
     */
    public function __construct($nouns, $onlyRenamed, $includeFields)
    {
      $this->nouns = $nouns;
      $this->onlyRenamed = $onlyRenamed;
      $this->includeFields = $includeFields;
    }

    /**
     * @return string
     */
    public function getNouns()
    {
      return $this->nouns;
    }

    /**
     * @param string $nouns
     * @return describeNouns
     */
    public function setNouns($nouns)
    {
      $this->nouns = $nouns;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getOnlyRenamed()
    {
      return $this->onlyRenamed;
    }

    /**
     * @param boolean $onlyRenamed
     * @return describeNouns
     */
    public function setOnlyRenamed($onlyRenamed)
    {
      $this->onlyRenamed = $onlyRenamed;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIncludeFields()
    {
      return $this->includeFields;
    }

    /**
     * @param boolean $includeFields
     * @return describeNouns
     */
    public function setIncludeFields($includeFields)
    {
      $this->includeFields = $includeFields;
      return $this;
    }

}
