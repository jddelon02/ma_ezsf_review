<?php

class describeQuickActions
{

    /**
     * @var string $quickActions
     */
    protected $quickActions = null;

    /**
     * @param string $quickActions
     */
    public function __construct($quickActions)
    {
      $this->quickActions = $quickActions;
    }

    /**
     * @return string
     */
    public function getQuickActions()
    {
      return $this->quickActions;
    }

    /**
     * @param string $quickActions
     * @return describeQuickActions
     */
    public function setQuickActions($quickActions)
    {
      $this->quickActions = $quickActions;
      return $this;
    }

}
