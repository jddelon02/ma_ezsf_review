<?php

class QueryOptions
{

    /**
     * @var int $batchSize
     */
    protected $batchSize = null;

    /**
     * @param int $batchSize
     */
    public function __construct($batchSize)
    {
      $this->batchSize = $batchSize;
    }

    /**
     * @return int
     */
    public function getBatchSize()
    {
      return $this->batchSize;
    }

    /**
     * @param int $batchSize
     * @return QueryOptions
     */
    public function setBatchSize($batchSize)
    {
      $this->batchSize = $batchSize;
      return $this;
    }

}
