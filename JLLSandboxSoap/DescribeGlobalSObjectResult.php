<?php

class DescribeGlobalSObjectResult
{

    /**
     * @var boolean $activateable
     */
    protected $activateable = null;

    /**
     * @var boolean $createable
     */
    protected $createable = null;

    /**
     * @var boolean $custom
     */
    protected $custom = null;

    /**
     * @var boolean $customSetting
     */
    protected $customSetting = null;

    /**
     * @var boolean $deletable
     */
    protected $deletable = null;

    /**
     * @var boolean $deprecatedAndHidden
     */
    protected $deprecatedAndHidden = null;

    /**
     * @var boolean $feedEnabled
     */
    protected $feedEnabled = null;

    /**
     * @var boolean $idEnabled
     */
    protected $idEnabled = null;

    /**
     * @var string $keyPrefix
     */
    protected $keyPrefix = null;

    /**
     * @var string $label
     */
    protected $label = null;

    /**
     * @var string $labelPlural
     */
    protected $labelPlural = null;

    /**
     * @var boolean $layoutable
     */
    protected $layoutable = null;

    /**
     * @var boolean $mergeable
     */
    protected $mergeable = null;

    /**
     * @var string $name
     */
    protected $name = null;

    /**
     * @var boolean $queryable
     */
    protected $queryable = null;

    /**
     * @var boolean $replicateable
     */
    protected $replicateable = null;

    /**
     * @var boolean $retrieveable
     */
    protected $retrieveable = null;

    /**
     * @var boolean $searchable
     */
    protected $searchable = null;

    /**
     * @var boolean $triggerable
     */
    protected $triggerable = null;

    /**
     * @var boolean $undeletable
     */
    protected $undeletable = null;

    /**
     * @var boolean $updateable
     */
    protected $updateable = null;

    /**
     * @param boolean $activateable
     * @param boolean $createable
     * @param boolean $custom
     * @param boolean $customSetting
     * @param boolean $deletable
     * @param boolean $deprecatedAndHidden
     * @param boolean $feedEnabled
     * @param boolean $idEnabled
     * @param string $label
     * @param string $labelPlural
     * @param boolean $layoutable
     * @param boolean $mergeable
     * @param string $name
     * @param boolean $queryable
     * @param boolean $replicateable
     * @param boolean $retrieveable
     * @param boolean $searchable
     * @param boolean $triggerable
     * @param boolean $undeletable
     * @param boolean $updateable
     */
    public function __construct($activateable, $createable, $custom, $customSetting, $deletable, $deprecatedAndHidden, $feedEnabled, $idEnabled, $label, $labelPlural, $layoutable, $mergeable, $name, $queryable, $replicateable, $retrieveable, $searchable, $triggerable, $undeletable, $updateable)
    {
      $this->activateable = $activateable;
      $this->createable = $createable;
      $this->custom = $custom;
      $this->customSetting = $customSetting;
      $this->deletable = $deletable;
      $this->deprecatedAndHidden = $deprecatedAndHidden;
      $this->feedEnabled = $feedEnabled;
      $this->idEnabled = $idEnabled;
      $this->label = $label;
      $this->labelPlural = $labelPlural;
      $this->layoutable = $layoutable;
      $this->mergeable = $mergeable;
      $this->name = $name;
      $this->queryable = $queryable;
      $this->replicateable = $replicateable;
      $this->retrieveable = $retrieveable;
      $this->searchable = $searchable;
      $this->triggerable = $triggerable;
      $this->undeletable = $undeletable;
      $this->updateable = $updateable;
    }

    /**
     * @return boolean
     */
    public function getActivateable()
    {
      return $this->activateable;
    }

    /**
     * @param boolean $activateable
     * @return DescribeGlobalSObjectResult
     */
    public function setActivateable($activateable)
    {
      $this->activateable = $activateable;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getCreateable()
    {
      return $this->createable;
    }

    /**
     * @param boolean $createable
     * @return DescribeGlobalSObjectResult
     */
    public function setCreateable($createable)
    {
      $this->createable = $createable;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getCustom()
    {
      return $this->custom;
    }

    /**
     * @param boolean $custom
     * @return DescribeGlobalSObjectResult
     */
    public function setCustom($custom)
    {
      $this->custom = $custom;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getCustomSetting()
    {
      return $this->customSetting;
    }

    /**
     * @param boolean $customSetting
     * @return DescribeGlobalSObjectResult
     */
    public function setCustomSetting($customSetting)
    {
      $this->customSetting = $customSetting;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getDeletable()
    {
      return $this->deletable;
    }

    /**
     * @param boolean $deletable
     * @return DescribeGlobalSObjectResult
     */
    public function setDeletable($deletable)
    {
      $this->deletable = $deletable;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getDeprecatedAndHidden()
    {
      return $this->deprecatedAndHidden;
    }

    /**
     * @param boolean $deprecatedAndHidden
     * @return DescribeGlobalSObjectResult
     */
    public function setDeprecatedAndHidden($deprecatedAndHidden)
    {
      $this->deprecatedAndHidden = $deprecatedAndHidden;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getFeedEnabled()
    {
      return $this->feedEnabled;
    }

    /**
     * @param boolean $feedEnabled
     * @return DescribeGlobalSObjectResult
     */
    public function setFeedEnabled($feedEnabled)
    {
      $this->feedEnabled = $feedEnabled;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIdEnabled()
    {
      return $this->idEnabled;
    }

    /**
     * @param boolean $idEnabled
     * @return DescribeGlobalSObjectResult
     */
    public function setIdEnabled($idEnabled)
    {
      $this->idEnabled = $idEnabled;
      return $this;
    }

    /**
     * @return string
     */
    public function getKeyPrefix()
    {
      return $this->keyPrefix;
    }

    /**
     * @param string $keyPrefix
     * @return DescribeGlobalSObjectResult
     */
    public function setKeyPrefix($keyPrefix)
    {
      $this->keyPrefix = $keyPrefix;
      return $this;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
      return $this->label;
    }

    /**
     * @param string $label
     * @return DescribeGlobalSObjectResult
     */
    public function setLabel($label)
    {
      $this->label = $label;
      return $this;
    }

    /**
     * @return string
     */
    public function getLabelPlural()
    {
      return $this->labelPlural;
    }

    /**
     * @param string $labelPlural
     * @return DescribeGlobalSObjectResult
     */
    public function setLabelPlural($labelPlural)
    {
      $this->labelPlural = $labelPlural;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getLayoutable()
    {
      return $this->layoutable;
    }

    /**
     * @param boolean $layoutable
     * @return DescribeGlobalSObjectResult
     */
    public function setLayoutable($layoutable)
    {
      $this->layoutable = $layoutable;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getMergeable()
    {
      return $this->mergeable;
    }

    /**
     * @param boolean $mergeable
     * @return DescribeGlobalSObjectResult
     */
    public function setMergeable($mergeable)
    {
      $this->mergeable = $mergeable;
      return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
      return $this->name;
    }

    /**
     * @param string $name
     * @return DescribeGlobalSObjectResult
     */
    public function setName($name)
    {
      $this->name = $name;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getQueryable()
    {
      return $this->queryable;
    }

    /**
     * @param boolean $queryable
     * @return DescribeGlobalSObjectResult
     */
    public function setQueryable($queryable)
    {
      $this->queryable = $queryable;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getReplicateable()
    {
      return $this->replicateable;
    }

    /**
     * @param boolean $replicateable
     * @return DescribeGlobalSObjectResult
     */
    public function setReplicateable($replicateable)
    {
      $this->replicateable = $replicateable;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getRetrieveable()
    {
      return $this->retrieveable;
    }

    /**
     * @param boolean $retrieveable
     * @return DescribeGlobalSObjectResult
     */
    public function setRetrieveable($retrieveable)
    {
      $this->retrieveable = $retrieveable;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getSearchable()
    {
      return $this->searchable;
    }

    /**
     * @param boolean $searchable
     * @return DescribeGlobalSObjectResult
     */
    public function setSearchable($searchable)
    {
      $this->searchable = $searchable;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getTriggerable()
    {
      return $this->triggerable;
    }

    /**
     * @param boolean $triggerable
     * @return DescribeGlobalSObjectResult
     */
    public function setTriggerable($triggerable)
    {
      $this->triggerable = $triggerable;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getUndeletable()
    {
      return $this->undeletable;
    }

    /**
     * @param boolean $undeletable
     * @return DescribeGlobalSObjectResult
     */
    public function setUndeletable($undeletable)
    {
      $this->undeletable = $undeletable;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getUpdateable()
    {
      return $this->updateable;
    }

    /**
     * @param boolean $updateable
     * @return DescribeGlobalSObjectResult
     */
    public function setUpdateable($updateable)
    {
      $this->updateable = $updateable;
      return $this;
    }

}
