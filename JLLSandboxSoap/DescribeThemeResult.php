<?php

class DescribeThemeResult
{

    /**
     * @var DescribeThemeItem[] $themeItems
     */
    protected $themeItems = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return DescribeThemeItem[]
     */
    public function getThemeItems()
    {
      return $this->themeItems;
    }

    /**
     * @param DescribeThemeItem[] $themeItems
     * @return DescribeThemeResult
     */
    public function setThemeItems(array $themeItems = null)
    {
      $this->themeItems = $themeItems;
      return $this;
    }

}
