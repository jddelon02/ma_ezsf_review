<?php

class ExecuteListViewRequest
{

    /**
     * @var string $developerNameOrId
     */
    protected $developerNameOrId = null;

    /**
     * @var int $limit
     */
    protected $limit = null;

    /**
     * @var int $offset
     */
    protected $offset = null;

    /**
     * @var ListViewOrderBy[] $orderBy
     */
    protected $orderBy = null;

    /**
     * @var string $sobjectType
     */
    protected $sobjectType = null;

    /**
     * @param string $developerNameOrId
     * @param ListViewOrderBy[] $orderBy
     * @param string $sobjectType
     */
    public function __construct($developerNameOrId, array $orderBy, $sobjectType)
    {
      $this->developerNameOrId = $developerNameOrId;
      $this->orderBy = $orderBy;
      $this->sobjectType = $sobjectType;
    }

    /**
     * @return string
     */
    public function getDeveloperNameOrId()
    {
      return $this->developerNameOrId;
    }

    /**
     * @param string $developerNameOrId
     * @return ExecuteListViewRequest
     */
    public function setDeveloperNameOrId($developerNameOrId)
    {
      $this->developerNameOrId = $developerNameOrId;
      return $this;
    }

    /**
     * @return int
     */
    public function getLimit()
    {
      return $this->limit;
    }

    /**
     * @param int $limit
     * @return ExecuteListViewRequest
     */
    public function setLimit($limit)
    {
      $this->limit = $limit;
      return $this;
    }

    /**
     * @return int
     */
    public function getOffset()
    {
      return $this->offset;
    }

    /**
     * @param int $offset
     * @return ExecuteListViewRequest
     */
    public function setOffset($offset)
    {
      $this->offset = $offset;
      return $this;
    }

    /**
     * @return ListViewOrderBy[]
     */
    public function getOrderBy()
    {
      return $this->orderBy;
    }

    /**
     * @param ListViewOrderBy[] $orderBy
     * @return ExecuteListViewRequest
     */
    public function setOrderBy(array $orderBy)
    {
      $this->orderBy = $orderBy;
      return $this;
    }

    /**
     * @return string
     */
    public function getSobjectType()
    {
      return $this->sobjectType;
    }

    /**
     * @param string $sobjectType
     * @return ExecuteListViewRequest
     */
    public function setSobjectType($sobjectType)
    {
      $this->sobjectType = $sobjectType;
      return $this;
    }

}
