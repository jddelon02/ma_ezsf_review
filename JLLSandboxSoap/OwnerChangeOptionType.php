<?php

class OwnerChangeOptionType
{
    const __default = 'TransferOpenActivities';
    const TransferOpenActivities = 'TransferOpenActivities';
    const TransferNotesAndAttachments = 'TransferNotesAndAttachments';
    const TransferOthersOpenOpportunities = 'TransferOthersOpenOpportunities';
    const TransferOwnedOpenOpportunities = 'TransferOwnedOpenOpportunities';
    const TransferContracts = 'TransferContracts';
    const TransferOrders = 'TransferOrders';
    const TransferContacts = 'TransferContacts';


}
