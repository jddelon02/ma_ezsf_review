<?php

class describeFlexiPagesResponse
{

    /**
     * @var DescribeFlexiPageResult $result
     */
    protected $result = null;

    /**
     * @param DescribeFlexiPageResult $result
     */
    public function __construct($result)
    {
      $this->result = $result;
    }

    /**
     * @return DescribeFlexiPageResult
     */
    public function getResult()
    {
      return $this->result;
    }

    /**
     * @param DescribeFlexiPageResult $result
     * @return describeFlexiPagesResponse
     */
    public function setResult($result)
    {
      $this->result = $result;
      return $this;
    }

}
