<?php

class LeadConvertResult
{

    /**
     * @var ID $accountId
     */
    protected $accountId = null;

    /**
     * @var ID $contactId
     */
    protected $contactId = null;

    /**
     * @var Error[] $errors
     */
    protected $errors = null;

    /**
     * @var ID $leadId
     */
    protected $leadId = null;

    /**
     * @var ID $opportunityId
     */
    protected $opportunityId = null;

    /**
     * @var boolean $success
     */
    protected $success = null;

    /**
     * @param boolean $success
     */
    public function __construct($success)
    {
      $this->success = $success;
    }

    /**
     * @return ID
     */
    public function getAccountId()
    {
      return $this->accountId;
    }

    /**
     * @param ID $accountId
     * @return LeadConvertResult
     */
    public function setAccountId($accountId)
    {
      $this->accountId = $accountId;
      return $this;
    }

    /**
     * @return ID
     */
    public function getContactId()
    {
      return $this->contactId;
    }

    /**
     * @param ID $contactId
     * @return LeadConvertResult
     */
    public function setContactId($contactId)
    {
      $this->contactId = $contactId;
      return $this;
    }

    /**
     * @return Error[]
     */
    public function getErrors()
    {
      return $this->errors;
    }

    /**
     * @param Error[] $errors
     * @return LeadConvertResult
     */
    public function setErrors(array $errors = null)
    {
      $this->errors = $errors;
      return $this;
    }

    /**
     * @return ID
     */
    public function getLeadId()
    {
      return $this->leadId;
    }

    /**
     * @param ID $leadId
     * @return LeadConvertResult
     */
    public function setLeadId($leadId)
    {
      $this->leadId = $leadId;
      return $this;
    }

    /**
     * @return ID
     */
    public function getOpportunityId()
    {
      return $this->opportunityId;
    }

    /**
     * @param ID $opportunityId
     * @return LeadConvertResult
     */
    public function setOpportunityId($opportunityId)
    {
      $this->opportunityId = $opportunityId;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getSuccess()
    {
      return $this->success;
    }

    /**
     * @param boolean $success
     * @return LeadConvertResult
     */
    public function setSuccess($success)
    {
      $this->success = $success;
      return $this;
    }

}
