<?php

class DescribeAppMenuResult
{

    /**
     * @var DescribeAppMenuItem[] $appMenuItems
     */
    protected $appMenuItems = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return DescribeAppMenuItem[]
     */
    public function getAppMenuItems()
    {
      return $this->appMenuItems;
    }

    /**
     * @param DescribeAppMenuItem[] $appMenuItems
     * @return DescribeAppMenuResult
     */
    public function setAppMenuItems(array $appMenuItems = null)
    {
      $this->appMenuItems = $appMenuItems;
      return $this;
    }

}
