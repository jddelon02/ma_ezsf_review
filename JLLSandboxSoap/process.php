<?php

class process
{

    /**
     * @var ProcessRequest $actions
     */
    protected $actions = null;

    /**
     * @param ProcessRequest $actions
     */
    public function __construct($actions)
    {
      $this->actions = $actions;
    }

    /**
     * @return ProcessRequest
     */
    public function getActions()
    {
      return $this->actions;
    }

    /**
     * @param ProcessRequest $actions
     * @return process
     */
    public function setActions($actions)
    {
      $this->actions = $actions;
      return $this;
    }

}
