<?php

class describeSoqlListViewsResponse
{

    /**
     * @var DescribeSoqlListViewResult $result
     */
    protected $result = null;

    /**
     * @param DescribeSoqlListViewResult $result
     */
    public function __construct($result)
    {
      $this->result = $result;
    }

    /**
     * @return DescribeSoqlListViewResult
     */
    public function getResult()
    {
      return $this->result;
    }

    /**
     * @param DescribeSoqlListViewResult $result
     * @return describeSoqlListViewsResponse
     */
    public function setResult($result)
    {
      $this->result = $result;
      return $this;
    }

}
