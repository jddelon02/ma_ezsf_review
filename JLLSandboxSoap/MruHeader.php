<?php

class MruHeader
{

    /**
     * @var boolean $updateMru
     */
    protected $updateMru = null;

    /**
     * @param boolean $updateMru
     */
    public function __construct($updateMru)
    {
      $this->updateMru = $updateMru;
    }

    /**
     * @return boolean
     */
    public function getUpdateMru()
    {
      return $this->updateMru;
    }

    /**
     * @param boolean $updateMru
     * @return MruHeader
     */
    public function setUpdateMru($updateMru)
    {
      $this->updateMru = $updateMru;
      return $this;
    }

}
