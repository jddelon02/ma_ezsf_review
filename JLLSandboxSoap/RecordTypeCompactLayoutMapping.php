<?php

class RecordTypeCompactLayoutMapping
{

    /**
     * @var boolean $available
     */
    protected $available = null;

    /**
     * @var ID $compactLayoutId
     */
    protected $compactLayoutId = null;

    /**
     * @var string $compactLayoutName
     */
    protected $compactLayoutName = null;

    /**
     * @var ID $recordTypeId
     */
    protected $recordTypeId = null;

    /**
     * @var string $recordTypeName
     */
    protected $recordTypeName = null;

    /**
     * @param boolean $available
     * @param string $compactLayoutName
     * @param ID $recordTypeId
     * @param string $recordTypeName
     */
    public function __construct($available, $compactLayoutName, $recordTypeId, $recordTypeName)
    {
      $this->available = $available;
      $this->compactLayoutName = $compactLayoutName;
      $this->recordTypeId = $recordTypeId;
      $this->recordTypeName = $recordTypeName;
    }

    /**
     * @return boolean
     */
    public function getAvailable()
    {
      return $this->available;
    }

    /**
     * @param boolean $available
     * @return RecordTypeCompactLayoutMapping
     */
    public function setAvailable($available)
    {
      $this->available = $available;
      return $this;
    }

    /**
     * @return ID
     */
    public function getCompactLayoutId()
    {
      return $this->compactLayoutId;
    }

    /**
     * @param ID $compactLayoutId
     * @return RecordTypeCompactLayoutMapping
     */
    public function setCompactLayoutId($compactLayoutId)
    {
      $this->compactLayoutId = $compactLayoutId;
      return $this;
    }

    /**
     * @return string
     */
    public function getCompactLayoutName()
    {
      return $this->compactLayoutName;
    }

    /**
     * @param string $compactLayoutName
     * @return RecordTypeCompactLayoutMapping
     */
    public function setCompactLayoutName($compactLayoutName)
    {
      $this->compactLayoutName = $compactLayoutName;
      return $this;
    }

    /**
     * @return ID
     */
    public function getRecordTypeId()
    {
      return $this->recordTypeId;
    }

    /**
     * @param ID $recordTypeId
     * @return RecordTypeCompactLayoutMapping
     */
    public function setRecordTypeId($recordTypeId)
    {
      $this->recordTypeId = $recordTypeId;
      return $this;
    }

    /**
     * @return string
     */
    public function getRecordTypeName()
    {
      return $this->recordTypeName;
    }

    /**
     * @param string $recordTypeName
     * @return RecordTypeCompactLayoutMapping
     */
    public function setRecordTypeName($recordTypeName)
    {
      $this->recordTypeName = $recordTypeName;
      return $this;
    }

}
