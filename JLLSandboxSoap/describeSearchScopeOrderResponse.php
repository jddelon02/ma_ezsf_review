<?php

class describeSearchScopeOrderResponse
{

    /**
     * @var DescribeSearchScopeOrderResult $result
     */
    protected $result = null;

    /**
     * @param DescribeSearchScopeOrderResult $result
     */
    public function __construct($result)
    {
      $this->result = $result;
    }

    /**
     * @return DescribeSearchScopeOrderResult
     */
    public function getResult()
    {
      return $this->result;
    }

    /**
     * @param DescribeSearchScopeOrderResult $result
     * @return describeSearchScopeOrderResponse
     */
    public function setResult($result)
    {
      $this->result = $result;
      return $this;
    }

}
