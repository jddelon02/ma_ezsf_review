<?php

class resetPasswordResponse
{

    /**
     * @var ResetPasswordResult $result
     */
    protected $result = null;

    /**
     * @param ResetPasswordResult $result
     */
    public function __construct($result)
    {
      $this->result = $result;
    }

    /**
     * @return ResetPasswordResult
     */
    public function getResult()
    {
      return $this->result;
    }

    /**
     * @param ResetPasswordResult $result
     * @return resetPasswordResponse
     */
    public function setResult($result)
    {
      $this->result = $result;
      return $this;
    }

}
