<?php

class describeSoqlListViews
{

    /**
     * @var DescribeSoqlListViewsRequest $request
     */
    protected $request = null;

    /**
     * @param DescribeSoqlListViewsRequest $request
     */
    public function __construct($request)
    {
      $this->request = $request;
    }

    /**
     * @return DescribeSoqlListViewsRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param DescribeSoqlListViewsRequest $request
     * @return describeSoqlListViews
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
