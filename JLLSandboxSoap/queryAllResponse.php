<?php

class queryAllResponse
{

    /**
     * @var QueryResult $result
     */
    protected $result = null;

    /**
     * @param QueryResult $result
     */
    public function __construct($result)
    {
      $this->result = $result;
    }

    /**
     * @return QueryResult
     */
    public function getResult()
    {
      return $this->result;
    }

    /**
     * @param QueryResult $result
     * @return queryAllResponse
     */
    public function setResult($result)
    {
      $this->result = $result;
      return $this;
    }

}
