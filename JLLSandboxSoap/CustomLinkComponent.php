<?php

class CustomLinkComponent extends DescribeLayoutComponent
{

    /**
     * @var DescribeLayoutButton $customLink
     */
    protected $customLink = null;

    /**
     * @param int $displayLines
     * @param int $tabOrder
     * @param layoutComponentType $type
     * @param DescribeLayoutButton $customLink
     */
    public function __construct($displayLines, $tabOrder, $type, $customLink)
    {
      parent::__construct($displayLines, $tabOrder, $type);
      $this->customLink = $customLink;
    }

    /**
     * @return DescribeLayoutButton
     */
    public function getCustomLink()
    {
      return $this->customLink;
    }

    /**
     * @param DescribeLayoutButton $customLink
     * @return CustomLinkComponent
     */
    public function setCustomLink($customLink)
    {
      $this->customLink = $customLink;
      return $this;
    }

}
