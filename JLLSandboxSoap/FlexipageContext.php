<?php

class FlexipageContext
{

    /**
     * @var FlexipageContextTypeEnum $type
     */
    protected $type = null;

    /**
     * @var string $value
     */
    protected $value = null;

    /**
     * @param FlexipageContextTypeEnum $type
     * @param string $value
     */
    public function __construct($type, $value)
    {
      $this->type = $type;
      $this->value = $value;
    }

    /**
     * @return FlexipageContextTypeEnum
     */
    public function getType()
    {
      return $this->type;
    }

    /**
     * @param FlexipageContextTypeEnum $type
     * @return FlexipageContext
     */
    public function setType($type)
    {
      $this->type = $type;
      return $this;
    }

    /**
     * @return string
     */
    public function getValue()
    {
      return $this->value;
    }

    /**
     * @param string $value
     * @return FlexipageContext
     */
    public function setValue($value)
    {
      $this->value = $value;
      return $this;
    }

}
