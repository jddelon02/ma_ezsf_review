<?php

class DescribeCompactLayoutsResult
{

    /**
     * @var DescribeCompactLayout[] $compactLayouts
     */
    protected $compactLayouts = null;

    /**
     * @var ID $defaultCompactLayoutId
     */
    protected $defaultCompactLayoutId = null;

    /**
     * @var RecordTypeCompactLayoutMapping[] $recordTypeCompactLayoutMappings
     */
    protected $recordTypeCompactLayoutMappings = null;

    /**
     * @param DescribeCompactLayout[] $compactLayouts
     * @param ID $defaultCompactLayoutId
     * @param RecordTypeCompactLayoutMapping[] $recordTypeCompactLayoutMappings
     */
    public function __construct(array $compactLayouts, $defaultCompactLayoutId, array $recordTypeCompactLayoutMappings)
    {
      $this->compactLayouts = $compactLayouts;
      $this->defaultCompactLayoutId = $defaultCompactLayoutId;
      $this->recordTypeCompactLayoutMappings = $recordTypeCompactLayoutMappings;
    }

    /**
     * @return DescribeCompactLayout[]
     */
    public function getCompactLayouts()
    {
      return $this->compactLayouts;
    }

    /**
     * @param DescribeCompactLayout[] $compactLayouts
     * @return DescribeCompactLayoutsResult
     */
    public function setCompactLayouts(array $compactLayouts)
    {
      $this->compactLayouts = $compactLayouts;
      return $this;
    }

    /**
     * @return ID
     */
    public function getDefaultCompactLayoutId()
    {
      return $this->defaultCompactLayoutId;
    }

    /**
     * @param ID $defaultCompactLayoutId
     * @return DescribeCompactLayoutsResult
     */
    public function setDefaultCompactLayoutId($defaultCompactLayoutId)
    {
      $this->defaultCompactLayoutId = $defaultCompactLayoutId;
      return $this;
    }

    /**
     * @return RecordTypeCompactLayoutMapping[]
     */
    public function getRecordTypeCompactLayoutMappings()
    {
      return $this->recordTypeCompactLayoutMappings;
    }

    /**
     * @param RecordTypeCompactLayoutMapping[] $recordTypeCompactLayoutMappings
     * @return DescribeCompactLayoutsResult
     */
    public function setRecordTypeCompactLayoutMappings(array $recordTypeCompactLayoutMappings)
    {
      $this->recordTypeCompactLayoutMappings = $recordTypeCompactLayoutMappings;
      return $this;
    }

}
