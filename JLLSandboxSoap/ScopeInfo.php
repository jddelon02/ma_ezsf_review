<?php

class ScopeInfo
{

    /**
     * @var string $label
     */
    protected $label = null;

    /**
     * @var string $name
     */
    protected $name = null;

    /**
     * @param string $label
     * @param string $name
     */
    public function __construct($label, $name)
    {
      $this->label = $label;
      $this->name = $name;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
      return $this->label;
    }

    /**
     * @param string $label
     * @return ScopeInfo
     */
    public function setLabel($label)
    {
      $this->label = $label;
      return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
      return $this->name;
    }

    /**
     * @param string $name
     * @return ScopeInfo
     */
    public function setName($name)
    {
      $this->name = $name;
      return $this;
    }

}
