<?php

class PicklistForRecordType
{

    /**
     * @var string $picklistName
     */
    protected $picklistName = null;

    /**
     * @var PicklistEntry[] $picklistValues
     */
    protected $picklistValues = null;

    /**
     * @param string $picklistName
     */
    public function __construct($picklistName)
    {
      $this->picklistName = $picklistName;
    }

    /**
     * @return string
     */
    public function getPicklistName()
    {
      return $this->picklistName;
    }

    /**
     * @param string $picklistName
     * @return PicklistForRecordType
     */
    public function setPicklistName($picklistName)
    {
      $this->picklistName = $picklistName;
      return $this;
    }

    /**
     * @return PicklistEntry[]
     */
    public function getPicklistValues()
    {
      return $this->picklistValues;
    }

    /**
     * @param PicklistEntry[] $picklistValues
     * @return PicklistForRecordType
     */
    public function setPicklistValues(array $picklistValues = null)
    {
      $this->picklistValues = $picklistValues;
      return $this;
    }

}
