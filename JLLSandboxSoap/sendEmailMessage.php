<?php

class sendEmailMessage
{

    /**
     * @var ID $ids
     */
    protected $ids = null;

    /**
     * @param ID $ids
     */
    public function __construct($ids)
    {
      $this->ids = $ids;
    }

    /**
     * @return ID
     */
    public function getIds()
    {
      return $this->ids;
    }

    /**
     * @param ID $ids
     * @return sendEmailMessage
     */
    public function setIds($ids)
    {
      $this->ids = $ids;
      return $this;
    }

}
