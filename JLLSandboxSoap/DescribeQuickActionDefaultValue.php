<?php

class DescribeQuickActionDefaultValue
{

    /**
     * @var string $defaultValue
     */
    protected $defaultValue = null;

    /**
     * @var string $field
     */
    protected $field = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getDefaultValue()
    {
      return $this->defaultValue;
    }

    /**
     * @param string $defaultValue
     * @return DescribeQuickActionDefaultValue
     */
    public function setDefaultValue($defaultValue)
    {
      $this->defaultValue = $defaultValue;
      return $this;
    }

    /**
     * @return string
     */
    public function getField()
    {
      return $this->field;
    }

    /**
     * @param string $field
     * @return DescribeQuickActionDefaultValue
     */
    public function setField($field)
    {
      $this->field = $field;
      return $this;
    }

}
