<?php

class SearchLayoutFieldsDisplayed
{

    /**
     * @var boolean $applicable
     */
    protected $applicable = null;

    /**
     * @var SearchLayoutField[] $fields
     */
    protected $fields = null;

    /**
     * @param boolean $applicable
     */
    public function __construct($applicable)
    {
      $this->applicable = $applicable;
    }

    /**
     * @return boolean
     */
    public function getApplicable()
    {
      return $this->applicable;
    }

    /**
     * @param boolean $applicable
     * @return SearchLayoutFieldsDisplayed
     */
    public function setApplicable($applicable)
    {
      $this->applicable = $applicable;
      return $this;
    }

    /**
     * @return SearchLayoutField[]
     */
    public function getFields()
    {
      return $this->fields;
    }

    /**
     * @param SearchLayoutField[] $fields
     * @return SearchLayoutFieldsDisplayed
     */
    public function setFields(array $fields = null)
    {
      $this->fields = $fields;
      return $this;
    }

}
