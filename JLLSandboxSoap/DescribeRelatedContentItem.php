<?php

class DescribeRelatedContentItem
{

    /**
     * @var DescribeLayoutItem $describeLayoutItem
     */
    protected $describeLayoutItem = null;

    /**
     * @param DescribeLayoutItem $describeLayoutItem
     */
    public function __construct($describeLayoutItem)
    {
      $this->describeLayoutItem = $describeLayoutItem;
    }

    /**
     * @return DescribeLayoutItem
     */
    public function getDescribeLayoutItem()
    {
      return $this->describeLayoutItem;
    }

    /**
     * @param DescribeLayoutItem $describeLayoutItem
     * @return DescribeRelatedContentItem
     */
    public function setDescribeLayoutItem($describeLayoutItem)
    {
      $this->describeLayoutItem = $describeLayoutItem;
      return $this;
    }

}
