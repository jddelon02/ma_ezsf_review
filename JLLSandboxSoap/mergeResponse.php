<?php

class mergeResponse
{

    /**
     * @var MergeResult $result
     */
    protected $result = null;

    /**
     * @param MergeResult $result
     */
    public function __construct($result)
    {
      $this->result = $result;
    }

    /**
     * @return MergeResult
     */
    public function getResult()
    {
      return $this->result;
    }

    /**
     * @param MergeResult $result
     * @return mergeResponse
     */
    public function setResult($result)
    {
      $this->result = $result;
      return $this;
    }

}
