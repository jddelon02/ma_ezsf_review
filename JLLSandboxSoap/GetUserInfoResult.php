<?php

class GetUserInfoResult
{

    /**
     * @var boolean $accessibilityMode
     */
    protected $accessibilityMode = null;

    /**
     * @var string $currencySymbol
     */
    protected $currencySymbol = null;

    /**
     * @var int $orgAttachmentFileSizeLimit
     */
    protected $orgAttachmentFileSizeLimit = null;

    /**
     * @var string $orgDefaultCurrencyIsoCode
     */
    protected $orgDefaultCurrencyIsoCode = null;

    /**
     * @var boolean $orgDisallowHtmlAttachments
     */
    protected $orgDisallowHtmlAttachments = null;

    /**
     * @var boolean $orgHasPersonAccounts
     */
    protected $orgHasPersonAccounts = null;

    /**
     * @var ID $organizationId
     */
    protected $organizationId = null;

    /**
     * @var boolean $organizationMultiCurrency
     */
    protected $organizationMultiCurrency = null;

    /**
     * @var string $organizationName
     */
    protected $organizationName = null;

    /**
     * @var ID $profileId
     */
    protected $profileId = null;

    /**
     * @var ID $roleId
     */
    protected $roleId = null;

    /**
     * @var int $sessionSecondsValid
     */
    protected $sessionSecondsValid = null;

    /**
     * @var string $userDefaultCurrencyIsoCode
     */
    protected $userDefaultCurrencyIsoCode = null;

    /**
     * @var string $userEmail
     */
    protected $userEmail = null;

    /**
     * @var string $userFullName
     */
    protected $userFullName = null;

    /**
     * @var ID $userId
     */
    protected $userId = null;

    /**
     * @var string $userLanguage
     */
    protected $userLanguage = null;

    /**
     * @var string $userLocale
     */
    protected $userLocale = null;

    /**
     * @var string $userName
     */
    protected $userName = null;

    /**
     * @var string $userTimeZone
     */
    protected $userTimeZone = null;

    /**
     * @var string $userType
     */
    protected $userType = null;

    /**
     * @var string $userUiSkin
     */
    protected $userUiSkin = null;

    /**
     * @param boolean $accessibilityMode
     * @param int $orgAttachmentFileSizeLimit
     * @param boolean $orgDisallowHtmlAttachments
     * @param boolean $orgHasPersonAccounts
     * @param ID $organizationId
     * @param boolean $organizationMultiCurrency
     * @param string $organizationName
     * @param ID $profileId
     * @param int $sessionSecondsValid
     * @param string $userEmail
     * @param string $userFullName
     * @param ID $userId
     * @param string $userLanguage
     * @param string $userLocale
     * @param string $userName
     * @param string $userTimeZone
     * @param string $userType
     * @param string $userUiSkin
     */
    public function __construct($accessibilityMode, $orgAttachmentFileSizeLimit, $orgDisallowHtmlAttachments, $orgHasPersonAccounts, $organizationId, $organizationMultiCurrency, $organizationName, $profileId, $sessionSecondsValid, $userEmail, $userFullName, $userId, $userLanguage, $userLocale, $userName, $userTimeZone, $userType, $userUiSkin)
    {
      $this->accessibilityMode = $accessibilityMode;
      $this->orgAttachmentFileSizeLimit = $orgAttachmentFileSizeLimit;
      $this->orgDisallowHtmlAttachments = $orgDisallowHtmlAttachments;
      $this->orgHasPersonAccounts = $orgHasPersonAccounts;
      $this->organizationId = $organizationId;
      $this->organizationMultiCurrency = $organizationMultiCurrency;
      $this->organizationName = $organizationName;
      $this->profileId = $profileId;
      $this->sessionSecondsValid = $sessionSecondsValid;
      $this->userEmail = $userEmail;
      $this->userFullName = $userFullName;
      $this->userId = $userId;
      $this->userLanguage = $userLanguage;
      $this->userLocale = $userLocale;
      $this->userName = $userName;
      $this->userTimeZone = $userTimeZone;
      $this->userType = $userType;
      $this->userUiSkin = $userUiSkin;
    }

    /**
     * @return boolean
     */
    public function getAccessibilityMode()
    {
      return $this->accessibilityMode;
    }

    /**
     * @param boolean $accessibilityMode
     * @return GetUserInfoResult
     */
    public function setAccessibilityMode($accessibilityMode)
    {
      $this->accessibilityMode = $accessibilityMode;
      return $this;
    }

    /**
     * @return string
     */
    public function getCurrencySymbol()
    {
      return $this->currencySymbol;
    }

    /**
     * @param string $currencySymbol
     * @return GetUserInfoResult
     */
    public function setCurrencySymbol($currencySymbol)
    {
      $this->currencySymbol = $currencySymbol;
      return $this;
    }

    /**
     * @return int
     */
    public function getOrgAttachmentFileSizeLimit()
    {
      return $this->orgAttachmentFileSizeLimit;
    }

    /**
     * @param int $orgAttachmentFileSizeLimit
     * @return GetUserInfoResult
     */
    public function setOrgAttachmentFileSizeLimit($orgAttachmentFileSizeLimit)
    {
      $this->orgAttachmentFileSizeLimit = $orgAttachmentFileSizeLimit;
      return $this;
    }

    /**
     * @return string
     */
    public function getOrgDefaultCurrencyIsoCode()
    {
      return $this->orgDefaultCurrencyIsoCode;
    }

    /**
     * @param string $orgDefaultCurrencyIsoCode
     * @return GetUserInfoResult
     */
    public function setOrgDefaultCurrencyIsoCode($orgDefaultCurrencyIsoCode)
    {
      $this->orgDefaultCurrencyIsoCode = $orgDefaultCurrencyIsoCode;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getOrgDisallowHtmlAttachments()
    {
      return $this->orgDisallowHtmlAttachments;
    }

    /**
     * @param boolean $orgDisallowHtmlAttachments
     * @return GetUserInfoResult
     */
    public function setOrgDisallowHtmlAttachments($orgDisallowHtmlAttachments)
    {
      $this->orgDisallowHtmlAttachments = $orgDisallowHtmlAttachments;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getOrgHasPersonAccounts()
    {
      return $this->orgHasPersonAccounts;
    }

    /**
     * @param boolean $orgHasPersonAccounts
     * @return GetUserInfoResult
     */
    public function setOrgHasPersonAccounts($orgHasPersonAccounts)
    {
      $this->orgHasPersonAccounts = $orgHasPersonAccounts;
      return $this;
    }

    /**
     * @return ID
     */
    public function getOrganizationId()
    {
      return $this->organizationId;
    }

    /**
     * @param ID $organizationId
     * @return GetUserInfoResult
     */
    public function setOrganizationId($organizationId)
    {
      $this->organizationId = $organizationId;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getOrganizationMultiCurrency()
    {
      return $this->organizationMultiCurrency;
    }

    /**
     * @param boolean $organizationMultiCurrency
     * @return GetUserInfoResult
     */
    public function setOrganizationMultiCurrency($organizationMultiCurrency)
    {
      $this->organizationMultiCurrency = $organizationMultiCurrency;
      return $this;
    }

    /**
     * @return string
     */
    public function getOrganizationName()
    {
      return $this->organizationName;
    }

    /**
     * @param string $organizationName
     * @return GetUserInfoResult
     */
    public function setOrganizationName($organizationName)
    {
      $this->organizationName = $organizationName;
      return $this;
    }

    /**
     * @return ID
     */
    public function getProfileId()
    {
      return $this->profileId;
    }

    /**
     * @param ID $profileId
     * @return GetUserInfoResult
     */
    public function setProfileId($profileId)
    {
      $this->profileId = $profileId;
      return $this;
    }

    /**
     * @return ID
     */
    public function getRoleId()
    {
      return $this->roleId;
    }

    /**
     * @param ID $roleId
     * @return GetUserInfoResult
     */
    public function setRoleId($roleId)
    {
      $this->roleId = $roleId;
      return $this;
    }

    /**
     * @return int
     */
    public function getSessionSecondsValid()
    {
      return $this->sessionSecondsValid;
    }

    /**
     * @param int $sessionSecondsValid
     * @return GetUserInfoResult
     */
    public function setSessionSecondsValid($sessionSecondsValid)
    {
      $this->sessionSecondsValid = $sessionSecondsValid;
      return $this;
    }

    /**
     * @return string
     */
    public function getUserDefaultCurrencyIsoCode()
    {
      return $this->userDefaultCurrencyIsoCode;
    }

    /**
     * @param string $userDefaultCurrencyIsoCode
     * @return GetUserInfoResult
     */
    public function setUserDefaultCurrencyIsoCode($userDefaultCurrencyIsoCode)
    {
      $this->userDefaultCurrencyIsoCode = $userDefaultCurrencyIsoCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getUserEmail()
    {
      return $this->userEmail;
    }

    /**
     * @param string $userEmail
     * @return GetUserInfoResult
     */
    public function setUserEmail($userEmail)
    {
      $this->userEmail = $userEmail;
      return $this;
    }

    /**
     * @return string
     */
    public function getUserFullName()
    {
      return $this->userFullName;
    }

    /**
     * @param string $userFullName
     * @return GetUserInfoResult
     */
    public function setUserFullName($userFullName)
    {
      $this->userFullName = $userFullName;
      return $this;
    }

    /**
     * @return ID
     */
    public function getUserId()
    {
      return $this->userId;
    }

    /**
     * @param ID $userId
     * @return GetUserInfoResult
     */
    public function setUserId($userId)
    {
      $this->userId = $userId;
      return $this;
    }

    /**
     * @return string
     */
    public function getUserLanguage()
    {
      return $this->userLanguage;
    }

    /**
     * @param string $userLanguage
     * @return GetUserInfoResult
     */
    public function setUserLanguage($userLanguage)
    {
      $this->userLanguage = $userLanguage;
      return $this;
    }

    /**
     * @return string
     */
    public function getUserLocale()
    {
      return $this->userLocale;
    }

    /**
     * @param string $userLocale
     * @return GetUserInfoResult
     */
    public function setUserLocale($userLocale)
    {
      $this->userLocale = $userLocale;
      return $this;
    }

    /**
     * @return string
     */
    public function getUserName()
    {
      return $this->userName;
    }

    /**
     * @param string $userName
     * @return GetUserInfoResult
     */
    public function setUserName($userName)
    {
      $this->userName = $userName;
      return $this;
    }

    /**
     * @return string
     */
    public function getUserTimeZone()
    {
      return $this->userTimeZone;
    }

    /**
     * @param string $userTimeZone
     * @return GetUserInfoResult
     */
    public function setUserTimeZone($userTimeZone)
    {
      $this->userTimeZone = $userTimeZone;
      return $this;
    }

    /**
     * @return string
     */
    public function getUserType()
    {
      return $this->userType;
    }

    /**
     * @param string $userType
     * @return GetUserInfoResult
     */
    public function setUserType($userType)
    {
      $this->userType = $userType;
      return $this;
    }

    /**
     * @return string
     */
    public function getUserUiSkin()
    {
      return $this->userUiSkin;
    }

    /**
     * @param string $userUiSkin
     * @return GetUserInfoResult
     */
    public function setUserUiSkin($userUiSkin)
    {
      $this->userUiSkin = $userUiSkin;
      return $this;
    }

}
