<?php

class LimitInfoHeader
{

    /**
     * @var LimitInfo $limitInfo
     */
    protected $limitInfo = null;

    /**
     * @param LimitInfo $limitInfo
     */
    public function __construct($limitInfo)
    {
      $this->limitInfo = $limitInfo;
    }

    /**
     * @return LimitInfo
     */
    public function getLimitInfo()
    {
      return $this->limitInfo;
    }

    /**
     * @param LimitInfo $limitInfo
     * @return LimitInfoHeader
     */
    public function setLimitInfo($limitInfo)
    {
      $this->limitInfo = $limitInfo;
      return $this;
    }

}
