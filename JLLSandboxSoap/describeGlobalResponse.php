<?php

class describeGlobalResponse
{

    /**
     * @var DescribeGlobalResult $result
     */
    protected $result = null;

    /**
     * @param DescribeGlobalResult $result
     */
    public function __construct($result)
    {
      $this->result = $result;
    }

    /**
     * @return DescribeGlobalResult
     */
    public function getResult()
    {
      return $this->result;
    }

    /**
     * @param DescribeGlobalResult $result
     * @return describeGlobalResponse
     */
    public function setResult($result)
    {
      $this->result = $result;
      return $this;
    }

}
