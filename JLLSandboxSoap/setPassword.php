<?php

class setPassword
{

    /**
     * @var ID $userId
     */
    protected $userId = null;

    /**
     * @var string $password
     */
    protected $password = null;

    /**
     * @param ID $userId
     * @param string $password
     */
    public function __construct($userId, $password)
    {
      $this->userId = $userId;
      $this->password = $password;
    }

    /**
     * @return ID
     */
    public function getUserId()
    {
      return $this->userId;
    }

    /**
     * @param ID $userId
     * @return setPassword
     */
    public function setUserId($userId)
    {
      $this->userId = $userId;
      return $this;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
      return $this->password;
    }

    /**
     * @param string $password
     * @return setPassword
     */
    public function setPassword($password)
    {
      $this->password = $password;
      return $this;
    }

}
