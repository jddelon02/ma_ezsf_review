<?php

class MergeResult
{

    /**
     * @var Error[] $errors
     */
    protected $errors = null;

    /**
     * @var ID $id
     */
    protected $id = null;

    /**
     * @var ID[] $mergedRecordIds
     */
    protected $mergedRecordIds = null;

    /**
     * @var boolean $success
     */
    protected $success = null;

    /**
     * @var ID[] $updatedRelatedIds
     */
    protected $updatedRelatedIds = null;

    /**
     * @param boolean $success
     */
    public function __construct($success)
    {
      $this->success = $success;
    }

    /**
     * @return Error[]
     */
    public function getErrors()
    {
      return $this->errors;
    }

    /**
     * @param Error[] $errors
     * @return MergeResult
     */
    public function setErrors(array $errors = null)
    {
      $this->errors = $errors;
      return $this;
    }

    /**
     * @return ID
     */
    public function getId()
    {
      return $this->id;
    }

    /**
     * @param ID $id
     * @return MergeResult
     */
    public function setId($id)
    {
      $this->id = $id;
      return $this;
    }

    /**
     * @return ID[]
     */
    public function getMergedRecordIds()
    {
      return $this->mergedRecordIds;
    }

    /**
     * @param ID[] $mergedRecordIds
     * @return MergeResult
     */
    public function setMergedRecordIds(array $mergedRecordIds = null)
    {
      $this->mergedRecordIds = $mergedRecordIds;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getSuccess()
    {
      return $this->success;
    }

    /**
     * @param boolean $success
     * @return MergeResult
     */
    public function setSuccess($success)
    {
      $this->success = $success;
      return $this;
    }

    /**
     * @return ID[]
     */
    public function getUpdatedRelatedIds()
    {
      return $this->updatedRelatedIds;
    }

    /**
     * @param ID[] $updatedRelatedIds
     * @return MergeResult
     */
    public function setUpdatedRelatedIds(array $updatedRelatedIds = null)
    {
      $this->updatedRelatedIds = $updatedRelatedIds;
      return $this;
    }

}
