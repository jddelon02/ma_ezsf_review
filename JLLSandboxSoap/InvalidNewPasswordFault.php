<?php

class InvalidNewPasswordFault extends ApiFault
{

    /**
     * @param ExceptionCode $exceptionCode
     * @param string $exceptionMessage
     */
    public function __construct($exceptionCode, $exceptionMessage)
    {
      parent::__construct($exceptionCode, $exceptionMessage);
    }

}
