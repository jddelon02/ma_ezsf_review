<?php

class Error
{

    /**
     * @var string[] $fields
     */
    protected $fields = null;

    /**
     * @var string $message
     */
    protected $message = null;

    /**
     * @var StatusCode $statusCode
     */
    protected $statusCode = null;

    /**
     * @param string $message
     * @param StatusCode $statusCode
     */
    public function __construct($message, $statusCode)
    {
      $this->message = $message;
      $this->statusCode = $statusCode;
    }

    /**
     * @return string[]
     */
    public function getFields()
    {
      return $this->fields;
    }

    /**
     * @param string[] $fields
     * @return Error
     */
    public function setFields(array $fields = null)
    {
      $this->fields = $fields;
      return $this;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
      return $this->message;
    }

    /**
     * @param string $message
     * @return Error
     */
    public function setMessage($message)
    {
      $this->message = $message;
      return $this;
    }

    /**
     * @return StatusCode
     */
    public function getStatusCode()
    {
      return $this->statusCode;
    }

    /**
     * @param StatusCode $statusCode
     * @return Error
     */
    public function setStatusCode($statusCode)
    {
      $this->statusCode = $statusCode;
      return $this;
    }

}
