<?php

class RecordTypeInfo
{

    /**
     * @var boolean $available
     */
    protected $available = null;

    /**
     * @var boolean $defaultRecordTypeMapping
     */
    protected $defaultRecordTypeMapping = null;

    /**
     * @var boolean $master
     */
    protected $master = null;

    /**
     * @var string $name
     */
    protected $name = null;

    /**
     * @var ID $recordTypeId
     */
    protected $recordTypeId = null;

    /**
     * @param boolean $available
     * @param boolean $defaultRecordTypeMapping
     * @param boolean $master
     * @param string $name
     */
    public function __construct($available, $defaultRecordTypeMapping, $master, $name)
    {
      $this->available = $available;
      $this->defaultRecordTypeMapping = $defaultRecordTypeMapping;
      $this->master = $master;
      $this->name = $name;
    }

    /**
     * @return boolean
     */
    public function getAvailable()
    {
      return $this->available;
    }

    /**
     * @param boolean $available
     * @return RecordTypeInfo
     */
    public function setAvailable($available)
    {
      $this->available = $available;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getDefaultRecordTypeMapping()
    {
      return $this->defaultRecordTypeMapping;
    }

    /**
     * @param boolean $defaultRecordTypeMapping
     * @return RecordTypeInfo
     */
    public function setDefaultRecordTypeMapping($defaultRecordTypeMapping)
    {
      $this->defaultRecordTypeMapping = $defaultRecordTypeMapping;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getMaster()
    {
      return $this->master;
    }

    /**
     * @param boolean $master
     * @return RecordTypeInfo
     */
    public function setMaster($master)
    {
      $this->master = $master;
      return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
      return $this->name;
    }

    /**
     * @param string $name
     * @return RecordTypeInfo
     */
    public function setName($name)
    {
      $this->name = $name;
      return $this;
    }

    /**
     * @return ID
     */
    public function getRecordTypeId()
    {
      return $this->recordTypeId;
    }

    /**
     * @param ID $recordTypeId
     * @return RecordTypeInfo
     */
    public function setRecordTypeId($recordTypeId)
    {
      $this->recordTypeId = $recordTypeId;
      return $this;
    }

}
