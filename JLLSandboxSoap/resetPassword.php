<?php

class resetPassword
{

    /**
     * @var ID $userId
     */
    protected $userId = null;

    /**
     * @param ID $userId
     */
    public function __construct($userId)
    {
      $this->userId = $userId;
    }

    /**
     * @return ID
     */
    public function getUserId()
    {
      return $this->userId;
    }

    /**
     * @param ID $userId
     * @return resetPassword
     */
    public function setUserId($userId)
    {
      $this->userId = $userId;
      return $this;
    }

}
