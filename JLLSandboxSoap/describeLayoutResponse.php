<?php

class describeLayoutResponse
{

    /**
     * @var DescribeLayoutResult $result
     */
    protected $result = null;

    /**
     * @param DescribeLayoutResult $result
     */
    public function __construct($result)
    {
      $this->result = $result;
    }

    /**
     * @return DescribeLayoutResult
     */
    public function getResult()
    {
      return $this->result;
    }

    /**
     * @param DescribeLayoutResult $result
     * @return describeLayoutResponse
     */
    public function setResult($result)
    {
      $this->result = $result;
      return $this;
    }

}
