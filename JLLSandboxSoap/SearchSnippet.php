<?php

class SearchSnippet
{

    /**
     * @var string $text
     */
    protected $text = null;

    /**
     * @var NameValuePair[] $wholeFields
     */
    protected $wholeFields = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getText()
    {
      return $this->text;
    }

    /**
     * @param string $text
     * @return SearchSnippet
     */
    public function setText($text)
    {
      $this->text = $text;
      return $this;
    }

    /**
     * @return NameValuePair[]
     */
    public function getWholeFields()
    {
      return $this->wholeFields;
    }

    /**
     * @param NameValuePair[] $wholeFields
     * @return SearchSnippet
     */
    public function setWholeFields(array $wholeFields = null)
    {
      $this->wholeFields = $wholeFields;
      return $this;
    }

}
