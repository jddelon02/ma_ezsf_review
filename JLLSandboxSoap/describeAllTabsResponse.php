<?php

class describeAllTabsResponse
{

    /**
     * @var DescribeTab $result
     */
    protected $result = null;

    /**
     * @param DescribeTab $result
     */
    public function __construct($result)
    {
      $this->result = $result;
    }

    /**
     * @return DescribeTab
     */
    public function getResult()
    {
      return $this->result;
    }

    /**
     * @param DescribeTab $result
     * @return describeAllTabsResponse
     */
    public function setResult($result)
    {
      $this->result = $result;
      return $this;
    }

}
