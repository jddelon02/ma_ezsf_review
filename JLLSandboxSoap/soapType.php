<?php

class soapType
{
    const __default = 'tnsID';
    const tnsID = 'tns:ID';
    const xsdbase64Binary = 'xsd:base64Binary';
    const xsdboolean = 'xsd:boolean';
    const xsddouble = 'xsd:double';
    const xsdint = 'xsd:int';
    const xsdstring = 'xsd:string';
    const xsddate = 'xsd:date';
    const xsddateTime = 'xsd:dateTime';
    const xsdtime = 'xsd:time';
    const tnslocation = 'tns:location';
    const tnsaddress = 'tns:address';
    const xsdanyType = 'xsd:anyType';
    const urnRelationshipReferenceTo = 'urn:RelationshipReferenceTo';
    const urnSearchLayoutFieldsDisplayed = 'urn:SearchLayoutFieldsDisplayed';
    const urnSearchLayoutField = 'urn:SearchLayoutField';
    const urnSearchLayoutButtonsDisplayed = 'urn:SearchLayoutButtonsDisplayed';
    const urnSearchLayoutButton = 'urn:SearchLayoutButton';
    const urnRecordTypesSupported = 'urn:RecordTypesSupported';


}
