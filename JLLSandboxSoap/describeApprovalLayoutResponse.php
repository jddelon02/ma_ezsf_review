<?php

class describeApprovalLayoutResponse
{

    /**
     * @var DescribeApprovalLayoutResult $result
     */
    protected $result = null;

    /**
     * @param DescribeApprovalLayoutResult $result
     */
    public function __construct($result)
    {
      $this->result = $result;
    }

    /**
     * @return DescribeApprovalLayoutResult
     */
    public function getResult()
    {
      return $this->result;
    }

    /**
     * @param DescribeApprovalLayoutResult $result
     * @return describeApprovalLayoutResponse
     */
    public function setResult($result)
    {
      $this->result = $result;
      return $this;
    }

}
