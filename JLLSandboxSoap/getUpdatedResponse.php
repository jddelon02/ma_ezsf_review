<?php

class getUpdatedResponse
{

    /**
     * @var GetUpdatedResult $result
     */
    protected $result = null;

    /**
     * @param GetUpdatedResult $result
     */
    public function __construct($result)
    {
      $this->result = $result;
    }

    /**
     * @return GetUpdatedResult
     */
    public function getResult()
    {
      return $this->result;
    }

    /**
     * @param GetUpdatedResult $result
     * @return getUpdatedResponse
     */
    public function setResult($result)
    {
      $this->result = $result;
      return $this;
    }

}
