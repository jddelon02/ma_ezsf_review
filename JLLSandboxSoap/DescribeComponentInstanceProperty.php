<?php

class DescribeComponentInstanceProperty
{

    /**
     * @var string $name
     */
    protected $name = null;

    /**
     * @var DescribeFlexiPageRegion $region
     */
    protected $region = null;

    /**
     * @var string $value
     */
    protected $value = null;

    /**
     * @param string $name
     */
    public function __construct($name)
    {
      $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
      return $this->name;
    }

    /**
     * @param string $name
     * @return DescribeComponentInstanceProperty
     */
    public function setName($name)
    {
      $this->name = $name;
      return $this;
    }

    /**
     * @return DescribeFlexiPageRegion
     */
    public function getRegion()
    {
      return $this->region;
    }

    /**
     * @param DescribeFlexiPageRegion $region
     * @return DescribeComponentInstanceProperty
     */
    public function setRegion($region)
    {
      $this->region = $region;
      return $this;
    }

    /**
     * @return string
     */
    public function getValue()
    {
      return $this->value;
    }

    /**
     * @param string $value
     * @return DescribeComponentInstanceProperty
     */
    public function setValue($value)
    {
      $this->value = $value;
      return $this;
    }

}
