<?php

class emptyRecycleBinResponse
{

    /**
     * @var EmptyRecycleBinResult $result
     */
    protected $result = null;

    /**
     * @param EmptyRecycleBinResult $result
     */
    public function __construct($result)
    {
      $this->result = $result;
    }

    /**
     * @return EmptyRecycleBinResult
     */
    public function getResult()
    {
      return $this->result;
    }

    /**
     * @param EmptyRecycleBinResult $result
     * @return emptyRecycleBinResponse
     */
    public function setResult($result)
    {
      $this->result = $result;
      return $this;
    }

}
