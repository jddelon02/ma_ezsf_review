<?php

class describePrimaryCompactLayouts
{

    /**
     * @var string $sObjectTypes
     */
    protected $sObjectTypes = null;

    /**
     * @param string $sObjectTypes
     */
    public function __construct($sObjectTypes)
    {
      $this->sObjectTypes = $sObjectTypes;
    }

    /**
     * @return string
     */
    public function getSObjectTypes()
    {
      return $this->sObjectTypes;
    }

    /**
     * @param string $sObjectTypes
     * @return describePrimaryCompactLayouts
     */
    public function setSObjectTypes($sObjectTypes)
    {
      $this->sObjectTypes = $sObjectTypes;
      return $this;
    }

}
