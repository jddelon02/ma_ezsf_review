<?php

class DebuggingHeader
{

    /**
     * @var DebugLevel $debugLevel
     */
    protected $debugLevel = null;

    /**
     * @param DebugLevel $debugLevel
     */
    public function __construct($debugLevel)
    {
      $this->debugLevel = $debugLevel;
    }

    /**
     * @return DebugLevel
     */
    public function getDebugLevel()
    {
      return $this->debugLevel;
    }

    /**
     * @param DebugLevel $debugLevel
     * @return DebuggingHeader
     */
    public function setDebugLevel($debugLevel)
    {
      $this->debugLevel = $debugLevel;
      return $this;
    }

}
