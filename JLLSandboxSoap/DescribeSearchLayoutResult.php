<?php

class DescribeSearchLayoutResult
{

    /**
     * @var string $errorMsg
     */
    protected $errorMsg = null;

    /**
     * @var string $label
     */
    protected $label = null;

    /**
     * @var int $limitRows
     */
    protected $limitRows = null;

    /**
     * @var string $objectType
     */
    protected $objectType = null;

    /**
     * @var DescribeColumn[] $searchColumns
     */
    protected $searchColumns = null;

    /**
     * @param string $objectType
     */
    public function __construct($objectType)
    {
      $this->objectType = $objectType;
    }

    /**
     * @return string
     */
    public function getErrorMsg()
    {
      return $this->errorMsg;
    }

    /**
     * @param string $errorMsg
     * @return DescribeSearchLayoutResult
     */
    public function setErrorMsg($errorMsg)
    {
      $this->errorMsg = $errorMsg;
      return $this;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
      return $this->label;
    }

    /**
     * @param string $label
     * @return DescribeSearchLayoutResult
     */
    public function setLabel($label)
    {
      $this->label = $label;
      return $this;
    }

    /**
     * @return int
     */
    public function getLimitRows()
    {
      return $this->limitRows;
    }

    /**
     * @param int $limitRows
     * @return DescribeSearchLayoutResult
     */
    public function setLimitRows($limitRows)
    {
      $this->limitRows = $limitRows;
      return $this;
    }

    /**
     * @return string
     */
    public function getObjectType()
    {
      return $this->objectType;
    }

    /**
     * @param string $objectType
     * @return DescribeSearchLayoutResult
     */
    public function setObjectType($objectType)
    {
      $this->objectType = $objectType;
      return $this;
    }

    /**
     * @return DescribeColumn[]
     */
    public function getSearchColumns()
    {
      return $this->searchColumns;
    }

    /**
     * @param DescribeColumn[] $searchColumns
     * @return DescribeSearchLayoutResult
     */
    public function setSearchColumns(array $searchColumns = null)
    {
      $this->searchColumns = $searchColumns;
      return $this;
    }

}
