<?php

class DuplicateResult
{

    /**
     * @var boolean $allowSave
     */
    protected $allowSave = null;

    /**
     * @var string $duplicateRule
     */
    protected $duplicateRule = null;

    /**
     * @var string $duplicateRuleEntityType
     */
    protected $duplicateRuleEntityType = null;

    /**
     * @var string $errorMessage
     */
    protected $errorMessage = null;

    /**
     * @var MatchResult[] $matchResults
     */
    protected $matchResults = null;

    /**
     * @param boolean $allowSave
     * @param string $duplicateRule
     * @param string $duplicateRuleEntityType
     * @param string $errorMessage
     */
    public function __construct($allowSave, $duplicateRule, $duplicateRuleEntityType, $errorMessage)
    {
      $this->allowSave = $allowSave;
      $this->duplicateRule = $duplicateRule;
      $this->duplicateRuleEntityType = $duplicateRuleEntityType;
      $this->errorMessage = $errorMessage;
    }

    /**
     * @return boolean
     */
    public function getAllowSave()
    {
      return $this->allowSave;
    }

    /**
     * @param boolean $allowSave
     * @return DuplicateResult
     */
    public function setAllowSave($allowSave)
    {
      $this->allowSave = $allowSave;
      return $this;
    }

    /**
     * @return string
     */
    public function getDuplicateRule()
    {
      return $this->duplicateRule;
    }

    /**
     * @param string $duplicateRule
     * @return DuplicateResult
     */
    public function setDuplicateRule($duplicateRule)
    {
      $this->duplicateRule = $duplicateRule;
      return $this;
    }

    /**
     * @return string
     */
    public function getDuplicateRuleEntityType()
    {
      return $this->duplicateRuleEntityType;
    }

    /**
     * @param string $duplicateRuleEntityType
     * @return DuplicateResult
     */
    public function setDuplicateRuleEntityType($duplicateRuleEntityType)
    {
      $this->duplicateRuleEntityType = $duplicateRuleEntityType;
      return $this;
    }

    /**
     * @return string
     */
    public function getErrorMessage()
    {
      return $this->errorMessage;
    }

    /**
     * @param string $errorMessage
     * @return DuplicateResult
     */
    public function setErrorMessage($errorMessage)
    {
      $this->errorMessage = $errorMessage;
      return $this;
    }

    /**
     * @return MatchResult[]
     */
    public function getMatchResults()
    {
      return $this->matchResults;
    }

    /**
     * @param MatchResult[] $matchResults
     * @return DuplicateResult
     */
    public function setMatchResults(array $matchResults = null)
    {
      $this->matchResults = $matchResults;
      return $this;
    }

}
