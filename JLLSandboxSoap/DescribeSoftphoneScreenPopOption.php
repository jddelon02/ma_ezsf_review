<?php

class DescribeSoftphoneScreenPopOption
{

    /**
     * @var string $matchType
     */
    protected $matchType = null;

    /**
     * @var string $screenPopData
     */
    protected $screenPopData = null;

    /**
     * @var string $screenPopType
     */
    protected $screenPopType = null;

    /**
     * @param string $matchType
     * @param string $screenPopData
     * @param string $screenPopType
     */
    public function __construct($matchType, $screenPopData, $screenPopType)
    {
      $this->matchType = $matchType;
      $this->screenPopData = $screenPopData;
      $this->screenPopType = $screenPopType;
    }

    /**
     * @return string
     */
    public function getMatchType()
    {
      return $this->matchType;
    }

    /**
     * @param string $matchType
     * @return DescribeSoftphoneScreenPopOption
     */
    public function setMatchType($matchType)
    {
      $this->matchType = $matchType;
      return $this;
    }

    /**
     * @return string
     */
    public function getScreenPopData()
    {
      return $this->screenPopData;
    }

    /**
     * @param string $screenPopData
     * @return DescribeSoftphoneScreenPopOption
     */
    public function setScreenPopData($screenPopData)
    {
      $this->screenPopData = $screenPopData;
      return $this;
    }

    /**
     * @return string
     */
    public function getScreenPopType()
    {
      return $this->screenPopType;
    }

    /**
     * @param string $screenPopType
     * @return DescribeSoftphoneScreenPopOption
     */
    public function setScreenPopType($screenPopType)
    {
      $this->screenPopType = $screenPopType;
      return $this;
    }

}
