<?php

class DeletedRecord
{

    /**
     * @var \DateTime $deletedDate
     */
    protected $deletedDate = null;

    /**
     * @var ID $id
     */
    protected $id = null;

    /**
     * @param \DateTime $deletedDate
     * @param ID $id
     */
    public function __construct(\DateTime $deletedDate, $id)
    {
      $this->deletedDate = $deletedDate->format(\DateTime::ATOM);
      $this->id = $id;
    }

    /**
     * @return \DateTime
     */
    public function getDeletedDate()
    {
      if ($this->deletedDate == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->deletedDate);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $deletedDate
     * @return DeletedRecord
     */
    public function setDeletedDate(\DateTime $deletedDate)
    {
      $this->deletedDate = $deletedDate->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return ID
     */
    public function getId()
    {
      return $this->id;
    }

    /**
     * @param ID $id
     * @return DeletedRecord
     */
    public function setId($id)
    {
      $this->id = $id;
      return $this;
    }

}
