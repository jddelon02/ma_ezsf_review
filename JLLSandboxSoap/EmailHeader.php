<?php

class EmailHeader
{

    /**
     * @var boolean $triggerAutoResponseEmail
     */
    protected $triggerAutoResponseEmail = null;

    /**
     * @var boolean $triggerOtherEmail
     */
    protected $triggerOtherEmail = null;

    /**
     * @var boolean $triggerUserEmail
     */
    protected $triggerUserEmail = null;

    /**
     * @param boolean $triggerAutoResponseEmail
     * @param boolean $triggerOtherEmail
     * @param boolean $triggerUserEmail
     */
    public function __construct($triggerAutoResponseEmail, $triggerOtherEmail, $triggerUserEmail)
    {
      $this->triggerAutoResponseEmail = $triggerAutoResponseEmail;
      $this->triggerOtherEmail = $triggerOtherEmail;
      $this->triggerUserEmail = $triggerUserEmail;
    }

    /**
     * @return boolean
     */
    public function getTriggerAutoResponseEmail()
    {
      return $this->triggerAutoResponseEmail;
    }

    /**
     * @param boolean $triggerAutoResponseEmail
     * @return EmailHeader
     */
    public function setTriggerAutoResponseEmail($triggerAutoResponseEmail)
    {
      $this->triggerAutoResponseEmail = $triggerAutoResponseEmail;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getTriggerOtherEmail()
    {
      return $this->triggerOtherEmail;
    }

    /**
     * @param boolean $triggerOtherEmail
     * @return EmailHeader
     */
    public function setTriggerOtherEmail($triggerOtherEmail)
    {
      $this->triggerOtherEmail = $triggerOtherEmail;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getTriggerUserEmail()
    {
      return $this->triggerUserEmail;
    }

    /**
     * @param boolean $triggerUserEmail
     * @return EmailHeader
     */
    public function setTriggerUserEmail($triggerUserEmail)
    {
      $this->triggerUserEmail = $triggerUserEmail;
      return $this;
    }

}
