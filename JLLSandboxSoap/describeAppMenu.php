<?php

class describeAppMenu
{

    /**
     * @var AppMenuType $appMenuType
     */
    protected $appMenuType = null;

    /**
     * @var ID $networkId
     */
    protected $networkId = null;

    /**
     * @param AppMenuType $appMenuType
     * @param ID $networkId
     */
    public function __construct($appMenuType, $networkId)
    {
      $this->appMenuType = $appMenuType;
      $this->networkId = $networkId;
    }

    /**
     * @return AppMenuType
     */
    public function getAppMenuType()
    {
      return $this->appMenuType;
    }

    /**
     * @param AppMenuType $appMenuType
     * @return describeAppMenu
     */
    public function setAppMenuType($appMenuType)
    {
      $this->appMenuType = $appMenuType;
      return $this;
    }

    /**
     * @return ID
     */
    public function getNetworkId()
    {
      return $this->networkId;
    }

    /**
     * @param ID $networkId
     * @return describeAppMenu
     */
    public function setNetworkId($networkId)
    {
      $this->networkId = $networkId;
      return $this;
    }

}
