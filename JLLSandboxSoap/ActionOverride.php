<?php

class ActionOverride
{

    /**
     * @var string $formFactor
     */
    protected $formFactor = null;

    /**
     * @var boolean $isAvailableInTouch
     */
    protected $isAvailableInTouch = null;

    /**
     * @var string $name
     */
    protected $name = null;

    /**
     * @var ID $pageId
     */
    protected $pageId = null;

    /**
     * @var string $url
     */
    protected $url = null;

    /**
     * @param string $formFactor
     * @param boolean $isAvailableInTouch
     * @param string $name
     * @param ID $pageId
     * @param string $url
     */
    public function __construct($formFactor, $isAvailableInTouch, $name, $pageId, $url)
    {
      $this->formFactor = $formFactor;
      $this->isAvailableInTouch = $isAvailableInTouch;
      $this->name = $name;
      $this->pageId = $pageId;
      $this->url = $url;
    }

    /**
     * @return string
     */
    public function getFormFactor()
    {
      return $this->formFactor;
    }

    /**
     * @param string $formFactor
     * @return ActionOverride
     */
    public function setFormFactor($formFactor)
    {
      $this->formFactor = $formFactor;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIsAvailableInTouch()
    {
      return $this->isAvailableInTouch;
    }

    /**
     * @param boolean $isAvailableInTouch
     * @return ActionOverride
     */
    public function setIsAvailableInTouch($isAvailableInTouch)
    {
      $this->isAvailableInTouch = $isAvailableInTouch;
      return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
      return $this->name;
    }

    /**
     * @param string $name
     * @return ActionOverride
     */
    public function setName($name)
    {
      $this->name = $name;
      return $this;
    }

    /**
     * @return ID
     */
    public function getPageId()
    {
      return $this->pageId;
    }

    /**
     * @param ID $pageId
     * @return ActionOverride
     */
    public function setPageId($pageId)
    {
      $this->pageId = $pageId;
      return $this;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
      return $this->url;
    }

    /**
     * @param string $url
     * @return ActionOverride
     */
    public function setUrl($url)
    {
      $this->url = $url;
      return $this;
    }

}
