<?php

class convertLead
{

    /**
     * @var LeadConvert $leadConverts
     */
    protected $leadConverts = null;

    /**
     * @param LeadConvert $leadConverts
     */
    public function __construct($leadConverts)
    {
      $this->leadConverts = $leadConverts;
    }

    /**
     * @return LeadConvert
     */
    public function getLeadConverts()
    {
      return $this->leadConverts;
    }

    /**
     * @param LeadConvert $leadConverts
     * @return convertLead
     */
    public function setLeadConverts($leadConverts)
    {
      $this->leadConverts = $leadConverts;
      return $this;
    }

}
