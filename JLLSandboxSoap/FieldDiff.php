<?php

class FieldDiff
{

    /**
     * @var differenceType $difference
     */
    protected $difference = null;

    /**
     * @var string $name
     */
    protected $name = null;

    /**
     * @param string $name
     */
    public function __construct($name)
    {
      $this->name = $name;
    }

    /**
     * @return differenceType
     */
    public function getDifference()
    {
      return $this->difference;
    }

    /**
     * @param differenceType $difference
     * @return FieldDiff
     */
    public function setDifference($difference)
    {
      $this->difference = $difference;
      return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
      return $this->name;
    }

    /**
     * @param string $name
     * @return FieldDiff
     */
    public function setName($name)
    {
      $this->name = $name;
      return $this;
    }

}
