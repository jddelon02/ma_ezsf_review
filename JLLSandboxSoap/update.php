<?php

class update
{

    /**
     * @var sObject $sObjects
     */
    protected $sObjects = null;

    /**
     * @param sObject $sObjects
     */
    public function __construct($sObjects)
    {
      $this->sObjects = $sObjects;
    }

    /**
     * @return sObject
     */
    public function getSObjects()
    {
      return $this->sObjects;
    }

    /**
     * @param sObject $sObjects
     * @return update
     */
    public function setSObjects($sObjects)
    {
      $this->sObjects = $sObjects;
      return $this;
    }

}
