<?php

class executeListView
{

    /**
     * @var ExecuteListViewRequest $request
     */
    protected $request = null;

    /**
     * @param ExecuteListViewRequest $request
     */
    public function __construct($request)
    {
      $this->request = $request;
    }

    /**
     * @return ExecuteListViewRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param ExecuteListViewRequest $request
     * @return executeListView
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
