<?php

class DescribeComponentInstance
{

    /**
     * @var DescribeComponentInstanceProperty[] $properties
     */
    protected $properties = null;

    /**
     * @var string $typeName
     */
    protected $typeName = null;

    /**
     * @var string $typeNamespace
     */
    protected $typeNamespace = null;

    /**
     * @param string $typeName
     * @param string $typeNamespace
     */
    public function __construct($typeName, $typeNamespace)
    {
      $this->typeName = $typeName;
      $this->typeNamespace = $typeNamespace;
    }

    /**
     * @return DescribeComponentInstanceProperty[]
     */
    public function getProperties()
    {
      return $this->properties;
    }

    /**
     * @param DescribeComponentInstanceProperty[] $properties
     * @return DescribeComponentInstance
     */
    public function setProperties(array $properties = null)
    {
      $this->properties = $properties;
      return $this;
    }

    /**
     * @return string
     */
    public function getTypeName()
    {
      return $this->typeName;
    }

    /**
     * @param string $typeName
     * @return DescribeComponentInstance
     */
    public function setTypeName($typeName)
    {
      $this->typeName = $typeName;
      return $this;
    }

    /**
     * @return string
     */
    public function getTypeNamespace()
    {
      return $this->typeNamespace;
    }

    /**
     * @param string $typeNamespace
     * @return DescribeComponentInstance
     */
    public function setTypeNamespace($typeNamespace)
    {
      $this->typeNamespace = $typeNamespace;
      return $this;
    }

}
