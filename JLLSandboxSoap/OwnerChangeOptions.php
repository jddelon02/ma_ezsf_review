<?php

class OwnerChangeOptions
{

    /**
     * @var OwnerChangeOption $options
     */
    protected $options = null;

    /**
     * @param OwnerChangeOption $options
     */
    public function __construct($options)
    {
      $this->options = $options;
    }

    /**
     * @return OwnerChangeOption
     */
    public function getOptions()
    {
      return $this->options;
    }

    /**
     * @param OwnerChangeOption $options
     * @return OwnerChangeOptions
     */
    public function setOptions($options)
    {
      $this->options = $options;
      return $this;
    }

}
