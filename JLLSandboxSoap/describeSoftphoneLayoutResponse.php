<?php

class describeSoftphoneLayoutResponse
{

    /**
     * @var DescribeSoftphoneLayoutResult $result
     */
    protected $result = null;

    /**
     * @param DescribeSoftphoneLayoutResult $result
     */
    public function __construct($result)
    {
      $this->result = $result;
    }

    /**
     * @return DescribeSoftphoneLayoutResult
     */
    public function getResult()
    {
      return $this->result;
    }

    /**
     * @param DescribeSoftphoneLayoutResult $result
     * @return describeSoftphoneLayoutResponse
     */
    public function setResult($result)
    {
      $this->result = $result;
      return $this;
    }

}
