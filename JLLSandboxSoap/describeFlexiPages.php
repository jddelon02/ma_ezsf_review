<?php

class describeFlexiPages
{

    /**
     * @var string $flexiPages
     */
    protected $flexiPages = null;

    /**
     * @var FlexipageContext $contexts
     */
    protected $contexts = null;

    /**
     * @param string $flexiPages
     * @param FlexipageContext $contexts
     */
    public function __construct($flexiPages, $contexts)
    {
      $this->flexiPages = $flexiPages;
      $this->contexts = $contexts;
    }

    /**
     * @return string
     */
    public function getFlexiPages()
    {
      return $this->flexiPages;
    }

    /**
     * @param string $flexiPages
     * @return describeFlexiPages
     */
    public function setFlexiPages($flexiPages)
    {
      $this->flexiPages = $flexiPages;
      return $this;
    }

    /**
     * @return FlexipageContext
     */
    public function getContexts()
    {
      return $this->contexts;
    }

    /**
     * @param FlexipageContext $contexts
     * @return describeFlexiPages
     */
    public function setContexts($contexts)
    {
      $this->contexts = $contexts;
      return $this;
    }

}
