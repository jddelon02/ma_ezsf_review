<?php

class describeSObjectResponse
{

    /**
     * @var DescribeSObjectResult $result
     */
    protected $result = null;

    /**
     * @param DescribeSObjectResult $result
     */
    public function __construct($result)
    {
      $this->result = $result;
    }

    /**
     * @return DescribeSObjectResult
     */
    public function getResult()
    {
      return $this->result;
    }

    /**
     * @param DescribeSObjectResult $result
     * @return describeSObjectResponse
     */
    public function setResult($result)
    {
      $this->result = $result;
      return $this;
    }

}
