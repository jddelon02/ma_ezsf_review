<?php

class RelationshipReferenceTo
{

    /**
     * @var string[] $referenceTo
     */
    protected $referenceTo = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string[]
     */
    public function getReferenceTo()
    {
      return $this->referenceTo;
    }

    /**
     * @param string[] $referenceTo
     * @return RelationshipReferenceTo
     */
    public function setReferenceTo(array $referenceTo = null)
    {
      $this->referenceTo = $referenceTo;
      return $this;
    }

}
