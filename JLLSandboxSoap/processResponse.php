<?php

class processResponse
{

    /**
     * @var ProcessResult $result
     */
    protected $result = null;

    /**
     * @param ProcessResult $result
     */
    public function __construct($result)
    {
      $this->result = $result;
    }

    /**
     * @return ProcessResult
     */
    public function getResult()
    {
      return $this->result;
    }

    /**
     * @param ProcessResult $result
     * @return processResponse
     */
    public function setResult($result)
    {
      $this->result = $result;
      return $this;
    }

}
