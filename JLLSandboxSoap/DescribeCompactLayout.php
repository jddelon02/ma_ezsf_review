<?php

class DescribeCompactLayout
{

    /**
     * @var DescribeLayoutButton[] $actions
     */
    protected $actions = null;

    /**
     * @var DescribeLayoutItem[] $fieldItems
     */
    protected $fieldItems = null;

    /**
     * @var ID $id
     */
    protected $id = null;

    /**
     * @var DescribeLayoutItem[] $imageItems
     */
    protected $imageItems = null;

    /**
     * @var string $label
     */
    protected $label = null;

    /**
     * @var string $name
     */
    protected $name = null;

    /**
     * @var string $objectType
     */
    protected $objectType = null;

    /**
     * @param ID $id
     * @param string $label
     * @param string $name
     * @param string $objectType
     */
    public function __construct($id, $label, $name, $objectType)
    {
      $this->id = $id;
      $this->label = $label;
      $this->name = $name;
      $this->objectType = $objectType;
    }

    /**
     * @return DescribeLayoutButton[]
     */
    public function getActions()
    {
      return $this->actions;
    }

    /**
     * @param DescribeLayoutButton[] $actions
     * @return DescribeCompactLayout
     */
    public function setActions(array $actions = null)
    {
      $this->actions = $actions;
      return $this;
    }

    /**
     * @return DescribeLayoutItem[]
     */
    public function getFieldItems()
    {
      return $this->fieldItems;
    }

    /**
     * @param DescribeLayoutItem[] $fieldItems
     * @return DescribeCompactLayout
     */
    public function setFieldItems(array $fieldItems = null)
    {
      $this->fieldItems = $fieldItems;
      return $this;
    }

    /**
     * @return ID
     */
    public function getId()
    {
      return $this->id;
    }

    /**
     * @param ID $id
     * @return DescribeCompactLayout
     */
    public function setId($id)
    {
      $this->id = $id;
      return $this;
    }

    /**
     * @return DescribeLayoutItem[]
     */
    public function getImageItems()
    {
      return $this->imageItems;
    }

    /**
     * @param DescribeLayoutItem[] $imageItems
     * @return DescribeCompactLayout
     */
    public function setImageItems(array $imageItems = null)
    {
      $this->imageItems = $imageItems;
      return $this;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
      return $this->label;
    }

    /**
     * @param string $label
     * @return DescribeCompactLayout
     */
    public function setLabel($label)
    {
      $this->label = $label;
      return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
      return $this->name;
    }

    /**
     * @param string $name
     * @return DescribeCompactLayout
     */
    public function setName($name)
    {
      $this->name = $name;
      return $this;
    }

    /**
     * @return string
     */
    public function getObjectType()
    {
      return $this->objectType;
    }

    /**
     * @param string $objectType
     * @return DescribeCompactLayout
     */
    public function setObjectType($objectType)
    {
      $this->objectType = $objectType;
      return $this;
    }

}
