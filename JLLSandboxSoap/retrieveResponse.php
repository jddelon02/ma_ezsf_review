<?php

class retrieveResponse
{

    /**
     * @var sObject $result
     */
    protected $result = null;

    /**
     * @param sObject $result
     */
    public function __construct($result)
    {
      $this->result = $result;
    }

    /**
     * @return sObject
     */
    public function getResult()
    {
      return $this->result;
    }

    /**
     * @param sObject $result
     * @return retrieveResponse
     */
    public function setResult($result)
    {
      $this->result = $result;
      return $this;
    }

}
