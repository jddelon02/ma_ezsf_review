<?php

class describeAvailableQuickActionsResponse
{

    /**
     * @var DescribeAvailableQuickActionResult $result
     */
    protected $result = null;

    /**
     * @param DescribeAvailableQuickActionResult $result
     */
    public function __construct($result)
    {
      $this->result = $result;
    }

    /**
     * @return DescribeAvailableQuickActionResult
     */
    public function getResult()
    {
      return $this->result;
    }

    /**
     * @param DescribeAvailableQuickActionResult $result
     * @return describeAvailableQuickActionsResponse
     */
    public function setResult($result)
    {
      $this->result = $result;
      return $this;
    }

}
