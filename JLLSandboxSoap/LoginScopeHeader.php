<?php

class LoginScopeHeader
{

    /**
     * @var ID $organizationId
     */
    protected $organizationId = null;

    /**
     * @var ID $portalId
     */
    protected $portalId = null;

    /**
     * @param ID $organizationId
     * @param ID $portalId
     */
    public function __construct($organizationId, $portalId)
    {
      $this->organizationId = $organizationId;
      $this->portalId = $portalId;
    }

    /**
     * @return ID
     */
    public function getOrganizationId()
    {
      return $this->organizationId;
    }

    /**
     * @param ID $organizationId
     * @return LoginScopeHeader
     */
    public function setOrganizationId($organizationId)
    {
      $this->organizationId = $organizationId;
      return $this;
    }

    /**
     * @return ID
     */
    public function getPortalId()
    {
      return $this->portalId;
    }

    /**
     * @param ID $portalId
     * @return LoginScopeHeader
     */
    public function setPortalId($portalId)
    {
      $this->portalId = $portalId;
      return $this;
    }

}
