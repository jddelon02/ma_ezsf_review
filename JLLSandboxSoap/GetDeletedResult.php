<?php

class GetDeletedResult
{

    /**
     * @var DeletedRecord[] $deletedRecords
     */
    protected $deletedRecords = null;

    /**
     * @var \DateTime $earliestDateAvailable
     */
    protected $earliestDateAvailable = null;

    /**
     * @var \DateTime $latestDateCovered
     */
    protected $latestDateCovered = null;

    /**
     * @param \DateTime $earliestDateAvailable
     * @param \DateTime $latestDateCovered
     */
    public function __construct(\DateTime $earliestDateAvailable, \DateTime $latestDateCovered)
    {
      $this->earliestDateAvailable = $earliestDateAvailable->format(\DateTime::ATOM);
      $this->latestDateCovered = $latestDateCovered->format(\DateTime::ATOM);
    }

    /**
     * @return DeletedRecord[]
     */
    public function getDeletedRecords()
    {
      return $this->deletedRecords;
    }

    /**
     * @param DeletedRecord[] $deletedRecords
     * @return GetDeletedResult
     */
    public function setDeletedRecords(array $deletedRecords = null)
    {
      $this->deletedRecords = $deletedRecords;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEarliestDateAvailable()
    {
      if ($this->earliestDateAvailable == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->earliestDateAvailable);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $earliestDateAvailable
     * @return GetDeletedResult
     */
    public function setEarliestDateAvailable(\DateTime $earliestDateAvailable)
    {
      $this->earliestDateAvailable = $earliestDateAvailable->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getLatestDateCovered()
    {
      if ($this->latestDateCovered == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->latestDateCovered);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $latestDateCovered
     * @return GetDeletedResult
     */
    public function setLatestDateCovered(\DateTime $latestDateCovered)
    {
      $this->latestDateCovered = $latestDateCovered->format(\DateTime::ATOM);
      return $this;
    }

}
