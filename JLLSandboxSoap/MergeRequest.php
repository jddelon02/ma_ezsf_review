<?php

class MergeRequest
{

    /**
     * @var sObject $masterRecord
     */
    protected $masterRecord = null;

    /**
     * @var ID[] $recordToMergeIds
     */
    protected $recordToMergeIds = null;

    /**
     * @param sObject $masterRecord
     * @param ID[] $recordToMergeIds
     */
    public function __construct($masterRecord, array $recordToMergeIds)
    {
      $this->masterRecord = $masterRecord;
      $this->recordToMergeIds = $recordToMergeIds;
    }

    /**
     * @return sObject
     */
    public function getMasterRecord()
    {
      return $this->masterRecord;
    }

    /**
     * @param sObject $masterRecord
     * @return MergeRequest
     */
    public function setMasterRecord($masterRecord)
    {
      $this->masterRecord = $masterRecord;
      return $this;
    }

    /**
     * @return ID[]
     */
    public function getRecordToMergeIds()
    {
      return $this->recordToMergeIds;
    }

    /**
     * @param ID[] $recordToMergeIds
     * @return MergeRequest
     */
    public function setRecordToMergeIds(array $recordToMergeIds)
    {
      $this->recordToMergeIds = $recordToMergeIds;
      return $this;
    }

}
