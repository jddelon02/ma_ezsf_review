<?php

class ApiFault
{

    /**
     * @var ExceptionCode $exceptionCode
     */
    protected $exceptionCode = null;

    /**
     * @var string $exceptionMessage
     */
    protected $exceptionMessage = null;

    /**
     * @param ExceptionCode $exceptionCode
     * @param string $exceptionMessage
     */
    public function __construct($exceptionCode, $exceptionMessage)
    {
      $this->exceptionCode = $exceptionCode;
      $this->exceptionMessage = $exceptionMessage;
    }

    /**
     * @return ExceptionCode
     */
    public function getExceptionCode()
    {
      return $this->exceptionCode;
    }

    /**
     * @param ExceptionCode $exceptionCode
     * @return ApiFault
     */
    public function setExceptionCode($exceptionCode)
    {
      $this->exceptionCode = $exceptionCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getExceptionMessage()
    {
      return $this->exceptionMessage;
    }

    /**
     * @param string $exceptionMessage
     * @return ApiFault
     */
    public function setExceptionMessage($exceptionMessage)
    {
      $this->exceptionMessage = $exceptionMessage;
      return $this;
    }

}
