<?php

class DescribeSoftphoneLayoutItem
{

    /**
     * @var string $itemApiName
     */
    protected $itemApiName = null;

    /**
     * @param string $itemApiName
     */
    public function __construct($itemApiName)
    {
      $this->itemApiName = $itemApiName;
    }

    /**
     * @return string
     */
    public function getItemApiName()
    {
      return $this->itemApiName;
    }

    /**
     * @param string $itemApiName
     * @return DescribeSoftphoneLayoutItem
     */
    public function setItemApiName($itemApiName)
    {
      $this->itemApiName = $itemApiName;
      return $this;
    }

}
