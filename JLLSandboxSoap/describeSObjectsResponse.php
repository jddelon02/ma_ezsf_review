<?php

class describeSObjectsResponse
{

    /**
     * @var DescribeSObjectResult $result
     */
    protected $result = null;

    /**
     * @param DescribeSObjectResult $result
     */
    public function __construct($result)
    {
      $this->result = $result;
    }

    /**
     * @return DescribeSObjectResult
     */
    public function getResult()
    {
      return $this->result;
    }

    /**
     * @param DescribeSObjectResult $result
     * @return describeSObjectsResponse
     */
    public function setResult($result)
    {
      $this->result = $result;
      return $this;
    }

}
