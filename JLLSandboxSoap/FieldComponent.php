<?php

class FieldComponent extends DescribeLayoutComponent
{

    /**
     * @var Field $field
     */
    protected $field = null;

    /**
     * @param int $displayLines
     * @param int $tabOrder
     * @param layoutComponentType $type
     * @param Field $field
     */
    public function __construct($displayLines, $tabOrder, $type, $field)
    {
      parent::__construct($displayLines, $tabOrder, $type);
      $this->field = $field;
    }

    /**
     * @return Field
     */
    public function getField()
    {
      return $this->field;
    }

    /**
     * @param Field $field
     * @return FieldComponent
     */
    public function setField($field)
    {
      $this->field = $field;
      return $this;
    }

}
