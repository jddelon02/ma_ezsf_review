<?php

class DescribeColor
{

    /**
     * @var string $color
     */
    protected $color = null;

    /**
     * @var string $context
     */
    protected $context = null;

    /**
     * @var string $theme
     */
    protected $theme = null;

    /**
     * @param string $color
     * @param string $context
     * @param string $theme
     */
    public function __construct($color, $context, $theme)
    {
      $this->color = $color;
      $this->context = $context;
      $this->theme = $theme;
    }

    /**
     * @return string
     */
    public function getColor()
    {
      return $this->color;
    }

    /**
     * @param string $color
     * @return DescribeColor
     */
    public function setColor($color)
    {
      $this->color = $color;
      return $this;
    }

    /**
     * @return string
     */
    public function getContext()
    {
      return $this->context;
    }

    /**
     * @param string $context
     * @return DescribeColor
     */
    public function setContext($context)
    {
      $this->context = $context;
      return $this;
    }

    /**
     * @return string
     */
    public function getTheme()
    {
      return $this->theme;
    }

    /**
     * @param string $theme
     * @return DescribeColor
     */
    public function setTheme($theme)
    {
      $this->theme = $theme;
      return $this;
    }

}
