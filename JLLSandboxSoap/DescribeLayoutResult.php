<?php

class DescribeLayoutResult
{

    /**
     * @var DescribeLayout[] $layouts
     */
    protected $layouts = null;

    /**
     * @var RecordTypeMapping[] $recordTypeMappings
     */
    protected $recordTypeMappings = null;

    /**
     * @var boolean $recordTypeSelectorRequired
     */
    protected $recordTypeSelectorRequired = null;

    /**
     * @param DescribeLayout[] $layouts
     * @param boolean $recordTypeSelectorRequired
     */
    public function __construct(array $layouts, $recordTypeSelectorRequired)
    {
      $this->layouts = $layouts;
      $this->recordTypeSelectorRequired = $recordTypeSelectorRequired;
    }

    /**
     * @return DescribeLayout[]
     */
    public function getLayouts()
    {
      return $this->layouts;
    }

    /**
     * @param DescribeLayout[] $layouts
     * @return DescribeLayoutResult
     */
    public function setLayouts(array $layouts)
    {
      $this->layouts = $layouts;
      return $this;
    }

    /**
     * @return RecordTypeMapping[]
     */
    public function getRecordTypeMappings()
    {
      return $this->recordTypeMappings;
    }

    /**
     * @param RecordTypeMapping[] $recordTypeMappings
     * @return DescribeLayoutResult
     */
    public function setRecordTypeMappings(array $recordTypeMappings = null)
    {
      $this->recordTypeMappings = $recordTypeMappings;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getRecordTypeSelectorRequired()
    {
      return $this->recordTypeSelectorRequired;
    }

    /**
     * @param boolean $recordTypeSelectorRequired
     * @return DescribeLayoutResult
     */
    public function setRecordTypeSelectorRequired($recordTypeSelectorRequired)
    {
      $this->recordTypeSelectorRequired = $recordTypeSelectorRequired;
      return $this;
    }

}
