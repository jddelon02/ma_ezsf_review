<?php

class describeTheme
{

    /**
     * @var string $sobjectType
     */
    protected $sobjectType = null;

    /**
     * @param string $sobjectType
     */
    public function __construct($sobjectType)
    {
      $this->sobjectType = $sobjectType;
    }

    /**
     * @return string
     */
    public function getSobjectType()
    {
      return $this->sobjectType;
    }

    /**
     * @param string $sobjectType
     * @return describeTheme
     */
    public function setSobjectType($sobjectType)
    {
      $this->sobjectType = $sobjectType;
      return $this;
    }

}
