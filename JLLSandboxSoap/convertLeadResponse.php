<?php

class convertLeadResponse
{

    /**
     * @var LeadConvertResult $result
     */
    protected $result = null;

    /**
     * @param LeadConvertResult $result
     */
    public function __construct($result)
    {
      $this->result = $result;
    }

    /**
     * @return LeadConvertResult
     */
    public function getResult()
    {
      return $this->result;
    }

    /**
     * @param LeadConvertResult $result
     * @return convertLeadResponse
     */
    public function setResult($result)
    {
      $this->result = $result;
      return $this;
    }

}
