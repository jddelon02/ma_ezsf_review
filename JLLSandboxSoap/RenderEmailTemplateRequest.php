<?php

class RenderEmailTemplateRequest
{

    /**
     * @var string $templateBodies
     */
    protected $templateBodies = null;

    /**
     * @var ID $whatId
     */
    protected $whatId = null;

    /**
     * @var ID $whoId
     */
    protected $whoId = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getTemplateBodies()
    {
      return $this->templateBodies;
    }

    /**
     * @param string $templateBodies
     * @return RenderEmailTemplateRequest
     */
    public function setTemplateBodies($templateBodies)
    {
      $this->templateBodies = $templateBodies;
      return $this;
    }

    /**
     * @return ID
     */
    public function getWhatId()
    {
      return $this->whatId;
    }

    /**
     * @param ID $whatId
     * @return RenderEmailTemplateRequest
     */
    public function setWhatId($whatId)
    {
      $this->whatId = $whatId;
      return $this;
    }

    /**
     * @return ID
     */
    public function getWhoId()
    {
      return $this->whoId;
    }

    /**
     * @param ID $whoId
     * @return RenderEmailTemplateRequest
     */
    public function setWhoId($whoId)
    {
      $this->whoId = $whoId;
      return $this;
    }

}
