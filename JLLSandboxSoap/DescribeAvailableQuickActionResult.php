<?php

class DescribeAvailableQuickActionResult
{

    /**
     * @var string $actionEnumOrId
     */
    protected $actionEnumOrId = null;

    /**
     * @var string $label
     */
    protected $label = null;

    /**
     * @var string $name
     */
    protected $name = null;

    /**
     * @var string $type
     */
    protected $type = null;

    /**
     * @param string $actionEnumOrId
     * @param string $label
     * @param string $name
     * @param string $type
     */
    public function __construct($actionEnumOrId, $label, $name, $type)
    {
      $this->actionEnumOrId = $actionEnumOrId;
      $this->label = $label;
      $this->name = $name;
      $this->type = $type;
    }

    /**
     * @return string
     */
    public function getActionEnumOrId()
    {
      return $this->actionEnumOrId;
    }

    /**
     * @param string $actionEnumOrId
     * @return DescribeAvailableQuickActionResult
     */
    public function setActionEnumOrId($actionEnumOrId)
    {
      $this->actionEnumOrId = $actionEnumOrId;
      return $this;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
      return $this->label;
    }

    /**
     * @param string $label
     * @return DescribeAvailableQuickActionResult
     */
    public function setLabel($label)
    {
      $this->label = $label;
      return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
      return $this->name;
    }

    /**
     * @param string $name
     * @return DescribeAvailableQuickActionResult
     */
    public function setName($name)
    {
      $this->name = $name;
      return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
      return $this->type;
    }

    /**
     * @param string $type
     * @return DescribeAvailableQuickActionResult
     */
    public function setType($type)
    {
      $this->type = $type;
      return $this;
    }

}
