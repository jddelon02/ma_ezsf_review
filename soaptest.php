<?php
/**
 * @file
 * Session_start() has to go right at the top, before any output!
 */

// Not sure this change was detected.
session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></meta>
<title>REST/OAuth Example</title>
</head>
<body>
	<tt>
  <?php
  // SALESFORCE CONFIG.
  define('DRUPAL_ROOT', '../../../../../');

  require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
  drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

  define("SF_USERNAME", "info@messageagency.com.jll");
  define("SF_PASSWORD", "M3ssageAgency!");
  define("SF_SECURITY_TOKEN", "vLwUyLMBA3nBR7ESTq9uOWsrr");
  // cs51.salesforce.com.
  define('SF_LOCATION', 'https://test.salesforce.com');

  require_once 'JLLSandboxSoap/autoload.php';
  require_once 'JLL_DatabaseAccess/autoload.php';

  try {
    $service = new \SforceService();
    $login = new \login(SF_USERNAME, SF_PASSWORD . SF_SECURITY_TOKEN);
    $session = $service->login($login);
    $thissession = $session->getResult();
    $SessionId = array("sessionId" => $thissession->getSessionId());
    $UserId = $thissession->getUserId();
    $service2 = new \JLL_DatabaseAccess_WSService();
    $service2->__setSoapHeaders(new SoapHeader('http://soap.sforce.com/schemas/class/JLL_DatabaseAccess_WS', "SessionHeader", $SessionId, FALSE));
    $thisContact = new \getDatabasesForContact('0034B00000554gz');
    $sessionContact = $service2->getDatabasesForContact($thisContact);
  }
  catch (SoapFault $fault) {
    trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);
  }
  var_dump($sessionContact);



?>
</tt>
</body>
</html>
