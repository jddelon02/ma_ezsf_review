# README #

This was a Drupal Module that was created for a client project. This is a Custom Module, it will not work out of the box.

### What is this repository for? ###

This Module defines the functionality needed to connect Salesforce Data to
Drupal to Ezproxy.

What this module does is leverage the integration between D7 and SF. If setup to do so, 
SF database objects are synced with D7 "database_detail" entity/node type. 
Then this file uses a WSDL to get database access details for a particular user, and stores that in the D7 cache. 
It then matches that list against either an internal database node request, or against a url provided by ezproxy. 
If the user has access (as determined by SF), then the link gets "ezproxified" so that it is in the form of a ticket. 
This would grant the user access to that database by sending the ezproxy-friendly URL information back.

This was not the final version of this module, but it is the version where I had done about 80 percent of the work.  Their was a lot
of iteration on this project, and much of the functionality pertaining to SF was pulled out and isolated into it's own module, but this version
gives a more "full" picture of what we were trying to do. 