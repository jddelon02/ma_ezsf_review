<?php


 function autoload_8d69aad592e5e25907c0420115036acb($class)
{
    $classes = array(
        'JLL_DatabaseAccess_WSService' => __DIR__ .'/JLL_DatabaseAccess_WSService.php',
        'AllowFieldTruncationHeader' => __DIR__ .'/AllowFieldTruncationHeader.php',
        'CallOptions' => __DIR__ .'/CallOptions.php',
        'DebuggingHeader' => __DIR__ .'/DebuggingHeader.php',
        'LogInfo' => __DIR__ .'/LogInfo.php',
        'LogCategory' => __DIR__ .'/LogCategory.php',
        'LogCategoryLevel' => __DIR__ .'/LogCategoryLevel.php',
        'LogType' => __DIR__ .'/LogType.php',
        'DebuggingInfo' => __DIR__ .'/DebuggingInfo.php',
        'SessionHeader' => __DIR__ .'/SessionHeader.php',
        'address' => __DIR__ .'/address.php',
        'location' => __DIR__ .'/location.php',
        'getDatabasesForContact' => __DIR__ .'/getDatabasesForContact.php',
        'getDatabasesForContactResponse' => __DIR__ .'/getDatabasesForContactResponse.php'
    );
    if (!empty($classes[$class])) {
        include $classes[$class];
    };
}

spl_autoload_register('autoload_8d69aad592e5e25907c0420115036acb');

// Do nothing. The rest is just leftovers from the code generation.
{
}
