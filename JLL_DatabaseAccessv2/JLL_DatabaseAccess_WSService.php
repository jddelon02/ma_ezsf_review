<?php

class JLL_DatabaseAccess_WSService extends \SoapClient {
  
  /**
   *
   * @var array $classmap The defined classes
   */
  private static $classmap = array(
    'AllowFieldTruncationHeader' => '\\AllowFieldTruncationHeader', 
    'CallOptions' => '\\CallOptions', 
    'DebuggingHeader' => '\\DebuggingHeader', 
    'LogInfo' => '\\LogInfo', 
    'DebuggingInfo' => '\\DebuggingInfo', 
    'SessionHeader' => '\\SessionHeader', 
    'address' => '\\address', 
    'location' => '\\location', 
    'getDatabasesForContact' => '\\getDatabasesForContact', 
    'getDatabasesForContactResponse' => '\\getDatabasesForContactResponse'
  );

  /**
   *
   * @param array $options
   *          A array of config values
   * @param string $wsdl
   *          The wsdl file to use
   */
  public function __construct(array $options = array(), $wsdl = null) {
    foreach (self::$classmap as $key => $value) {
      if (!isset($options['classmap'][$key])) {
        $options['classmap'][$key] = $value;
      }
    }
    $options = array_merge(array(
      'features' => 1
    ), $options);
    if (!$wsdl) {
      $wsdl = DRUPAL_ROOT . '/' . variable_get('salesforce_jll_filename_wsdl');
    }
    parent::__construct($wsdl, $options);
  }

  /**
   *
   * @param getDatabasesForContact $parameters          
   * @return getDatabasesForContactResponse
   */
  public function getDatabasesForContact(getDatabasesForContact $parameters) {
    return $this->__soapCall('getDatabasesForContact', array(
      $parameters
    ));
  }

}
