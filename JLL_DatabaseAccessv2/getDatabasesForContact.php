<?php

class getDatabasesForContact
{

    /**
     * @var string $contactId
     */
    protected $contactId = null;

    /**
     * @param string $contactId
     */
    public function __construct($contactId)
    {
      $this->contactId = $contactId;
    }

    /**
     * @return string
     */
    public function getContactId()
    {
      return $this->contactId;
    }

    /**
     * @param string $contactId
     * @return getDatabasesForContact
     */
    public function setContactId($contactId)
    {
      $this->contactId = $contactId;
      return $this;
    }

}
