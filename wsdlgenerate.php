<?php
require_once __DIR__ . '/vendor' . '/autoload.php';
$generator = new \Wsdl2PhpGenerator\Generator();
$generator->generate(
    new \Wsdl2PhpGenerator\Config(array(
        'inputFile' => 'JLL_DatabaseAccess_WS.wsdl',
        'outputDir' => 'JLL_DatabaseAccess'
    ))
);
