<?php

/**
 * usage "drush scr soaptest.drush.php [uid]"
 */

$args = drush_get_arguments();

// Don't care about the first 2:
array_shift($args);
array_shift($args);

// Default sfid:
$sfid = FALSE;
if (is_numeric($args[0])) {
  $sf = salesforce_mapping_object_load_by_drupal('user', $args[0], TRUE);
  print_r($sf);
  if (!empty($sf) && !empty($sf->salesforce_id)) {
    $sfid = $sf->salesforce_id;
  }
}
if (empty($sfid)) {
  return;
}

require_once 'JLLSandboxSoap/autoload.php';
require_once 'JLL_DatabaseAccess/autoload.php';

$service = new \SforceService();
$login = new \login(variable_get('salesforce_api_username', ''), variable_get('salesforce_api_password', '') . variable_get('salesforce_api_token', ''));
$session = $service->login($login);
$thissession = $session->getResult();
$SessionId = array("sessionId" => $thissession->getSessionId());
$UserId = $thissession->getUserId();
$service2 = new \JLL_DatabaseAccess_WSService();
$service2->__setSoapHeaders(new SoapHeader('http://soap.sforce.com/schemas/class/JLL_DatabaseAccess_WS', "SessionHeader", $SessionId, FALSE));
$thisContact = new \getDatabasesForContact($sfid);
$sessionContact = $service2->getDatabasesForContact($thisContact);

var_dump($sessionContact);
