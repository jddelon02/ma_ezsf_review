<?php

/**
 * This file builds the views programatically
 *
 * @file
 * ma_ezsf.views File.
 */

/**
 * Implements hook_views_default_views().
 *
 * @return array $views
 */
function ma_ezsf_views_default_views() {
  $views[] = _ma_ezsf_memberdb_approved_list();
  $views[] = _ma_ezsf_memberdb_notapproved_list();
  return $views;
}

/**
 * Implements hook_views_data_alter().
 *
 * @param array $data          
 */
function ma_ezsf_views_data_alter(&$data) {
  // dpm($data);
  if (isset($data['ma_entity']) && !isset($data['ma_entity']['memberlistDB'])) {
    $data['ma_entity']['memberlistDB'] = array(
      'real field' => 'ma_entity_id', 
      'title' => t('JLL DB Membership'), 
      'help' => t('Filter DBs based on access level of current logged-in user.'), 
      'filter' => array(
        'handler' => 'ma_ezsf_handler_filter_memberlistDB'
      )
    );
  }
}

/* CUSTOM FUNCTIONS TO BUILD DIFFERENT VIEWS */

/**
 * Build the Approved Member List View.
 *
 * @return array $view
 */
function _ma_ezsf_memberdb_approved_list() {
  $view = new view();
  $view->name = 'my_member_databases';
  $view->description = 'View of Member DBs filtered by the user who is viewing them';
  $view->tag = 'default';
  $view->base_table = 'ma_entity';
  $view->human_name = 'My Member Databases';
  $view->core = 7;
  $view->api_version = '3.0';
  /* Edit this to true to make a default view disabled initially */
  
  $view->disabled = FALSE;
  
  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'My Member Databases';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'entity';
  $handler->display->display_options['row_options']['view_mode'] = 'token';
  /* Field: Message Agency Entity: Message agency entity ID */
  $handler->display->display_options['fields']['ma_entity_id']['id'] = 'ma_entity_id';
  $handler->display->display_options['fields']['ma_entity_id']['table'] = 'ma_entity';
  $handler->display->display_options['fields']['ma_entity_id']['field'] = 'ma_entity_id';
  /* Field: Message Agency Entity: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'ma_entity';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  /* Field: Message Agency Entity: URL */
  $handler->display->display_options['fields']['url']['id'] = 'url';
  $handler->display->display_options['fields']['url']['table'] = 'views_entity_ma_entity';
  $handler->display->display_options['fields']['url']['field'] = 'url';
  $handler->display->display_options['fields']['url']['link_to_entity'] = 0;
  /* Field: Message Agency Entity: DB Url */
  $handler->display->display_options['fields']['field_dbinfo']['id'] = 'field_dbinfo';
  $handler->display->display_options['fields']['field_dbinfo']['table'] = 'field_data_field_dbinfo';
  $handler->display->display_options['fields']['field_dbinfo']['field'] = 'field_dbinfo';
  /* Field: Message Agency Entity: Link */
  $handler->display->display_options['fields']['link_ma_entity']['id'] = 'link_ma_entity';
  $handler->display->display_options['fields']['link_ma_entity']['table'] = 'ma_entity';
  $handler->display->display_options['fields']['link_ma_entity']['field'] = 'link_ma_entity';
  /* Filter criterion: Message Agency Entity: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'ma_entity';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'Jenkins DB' => 'jenkins_db'
  );
  /* Filter criterion: Message Agency Entity: In Member DB List */
  $handler->display->display_options['filters']['memberlistDB']['id'] = 'memberlistDB';
  $handler->display->display_options['filters']['memberlistDB']['table'] = 'ma_entity';
  $handler->display->display_options['filters']['memberlistDB']['field'] = 'memberlistDB';
  $handler->display->display_options['filters']['memberlistDB']['operator'] = '==';
  $handler->display->display_options['filters']['memberlistDB']['value'] = 'ALLOWEDDBS';
  
  /* Display: Page */
  
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'my-member-databases';
  
  /* Display: Block */
  
  $handler = $view->new_display('block', 'Block', 'block');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'My Member Databases';
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['fields']['field_dbinfo']['click_sort_column'] = 'url';
  
  // $views[] = $view;
  return $view;
}

/**
 * Build the NOT Approved Member List View.
 *
 * @return array $view
 */
function _ma_ezsf_memberdb_notapproved_list() {
  $view = new view();
  $view->name = 'not_my_member_databases';
  $view->description = 'View of NOT Member DBs filtered by the user who is viewing them';
  $view->tag = 'default';
  $view->base_table = 'ma_entity';
  $view->human_name = 'Not My Member Databases';
  $view->core = 7;
  $view->api_version = '3.0';
  /* Edit this to true to make a default view disabled initially */
  
  $view->disabled = FALSE;
  
  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Not My Member Databases';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'entity';
  $handler->display->display_options['row_options']['view_mode'] = 'token';
  /* Field: Message Agency Entity: Message agency entity ID */
  $handler->display->display_options['fields']['ma_entity_id']['id'] = 'ma_entity_id';
  $handler->display->display_options['fields']['ma_entity_id']['table'] = 'ma_entity';
  $handler->display->display_options['fields']['ma_entity_id']['field'] = 'ma_entity_id';
  /* Field: Message Agency Entity: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'ma_entity';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  /* Field: Message Agency Entity: URL */
  $handler->display->display_options['fields']['url']['id'] = 'url';
  $handler->display->display_options['fields']['url']['table'] = 'views_entity_ma_entity';
  $handler->display->display_options['fields']['url']['field'] = 'url';
  $handler->display->display_options['fields']['url']['link_to_entity'] = 0;
  /* Field: Message Agency Entity: DB Url */
  $handler->display->display_options['fields']['field_dbinfo']['id'] = 'field_dbinfo';
  $handler->display->display_options['fields']['field_dbinfo']['table'] = 'field_data_field_dbinfo';
  $handler->display->display_options['fields']['field_dbinfo']['field'] = 'field_dbinfo';
  /* Field: Message Agency Entity: Link */
  $handler->display->display_options['fields']['link_ma_entity']['id'] = 'link_ma_entity';
  $handler->display->display_options['fields']['link_ma_entity']['table'] = 'ma_entity';
  $handler->display->display_options['fields']['link_ma_entity']['field'] = 'link_ma_entity';
  /* Filter criterion: Message Agency Entity: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'ma_entity';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'Jenkins DB' => 'jenkins_db'
  );
  /* Filter criterion: Message Agency Entity: In Member DB List */
  $handler->display->display_options['filters']['notmemberlistDB']['id'] = 'memberlistDB';
  $handler->display->display_options['filters']['notmemberlistDB']['table'] = 'ma_entity';
  $handler->display->display_options['filters']['notmemberlistDB']['field'] = 'memberlistDB';
  $handler->display->display_options['filters']['notmemberlistDB']['operator'] = '!=';
  $handler->display->display_options['filters']['notmemberlistDB']['value'] = 'ALLOWEDDBS';
  
  /* Display: Page */
  
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'not-my-member-databases';
  
  /* Display: Block */
  
  $handler = $view->new_display('block', 'Block', 'block');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Not My Member Databases';
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['fields']['field_dbinfo']['click_sort_column'] = 'url';
  
  // $views[] = $view;
  return $view;
}
