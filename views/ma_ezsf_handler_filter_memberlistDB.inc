<?php
/**
 * This file contains a class that extends drupal views_handler_filter_equality
 * class.
 *
 * @file
 * ma_ezsf_handler_filter_memberlistDB.inc File.
 *
 * @author Jeremy DeLong
 * @author Aaron Bauman
 */

/**
 * Class to extend views to allow for viewing the authorized DB cache.
 */
class ma_ezsf_handler_filter_memberlistDB extends views_handler_filter_equality {

  /**
   * Query db to get the "in memeber db" cache and apply to view
   *
   * @return void
   */
  function query() {
    global $user;
    $this->ensure_my_table();
    
    $operator = 'IN';
    $field = "$this->table_alias.$this->real_field";
    
    if ($this->operator == "!=") {
      // dpm('not my member dbs!');
      $operator = "NOT IN";
    }
    /**
     * @TODO fix me: uid should come from views arg.
     */
    $account = user_load($user->uid);
    if (empty($account->jll_db_access)) {
      // Make sure we don't choke views with an SQL error.
      $account->jll_db_access = array(
        0
      );
    }
    $this->query->add_where($this->options['group'], $field, array_keys($account->jll_db_access), $operator);
  }

}

