<?php

class address extends location
{

    /**
     * @var string $city
     */
    protected $city = null;

    /**
     * @var string $country
     */
    protected $country = null;

    /**
     * @var string $countryCode
     */
    protected $countryCode = null;

    /**
     * @var string $geocodeAccuracy
     */
    protected $geocodeAccuracy = null;

    /**
     * @var string $postalCode
     */
    protected $postalCode = null;

    /**
     * @var string $state
     */
    protected $state = null;

    /**
     * @var string $stateCode
     */
    protected $stateCode = null;

    /**
     * @var string $street
     */
    protected $street = null;

    /**
     * @param float $latitude
     * @param float $longitude
     * @param string $city
     * @param string $country
     * @param string $countryCode
     * @param string $geocodeAccuracy
     * @param string $postalCode
     * @param string $state
     * @param string $stateCode
     * @param string $street
     */
    public function __construct($latitude, $longitude, $city, $country, $countryCode, $geocodeAccuracy, $postalCode, $state, $stateCode, $street)
    {
      parent::__construct($latitude, $longitude);
      $this->city = $city;
      $this->country = $country;
      $this->countryCode = $countryCode;
      $this->geocodeAccuracy = $geocodeAccuracy;
      $this->postalCode = $postalCode;
      $this->state = $state;
      $this->stateCode = $stateCode;
      $this->street = $street;
    }

    /**
     * @return string
     */
    public function getCity()
    {
      return $this->city;
    }

    /**
     * @param string $city
     * @return address
     */
    public function setCity($city)
    {
      $this->city = $city;
      return $this;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
      return $this->country;
    }

    /**
     * @param string $country
     * @return address
     */
    public function setCountry($country)
    {
      $this->country = $country;
      return $this;
    }

    /**
     * @return string
     */
    public function getCountryCode()
    {
      return $this->countryCode;
    }

    /**
     * @param string $countryCode
     * @return address
     */
    public function setCountryCode($countryCode)
    {
      $this->countryCode = $countryCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getGeocodeAccuracy()
    {
      return $this->geocodeAccuracy;
    }

    /**
     * @param string $geocodeAccuracy
     * @return address
     */
    public function setGeocodeAccuracy($geocodeAccuracy)
    {
      $this->geocodeAccuracy = $geocodeAccuracy;
      return $this;
    }

    /**
     * @return string
     */
    public function getPostalCode()
    {
      return $this->postalCode;
    }

    /**
     * @param string $postalCode
     * @return address
     */
    public function setPostalCode($postalCode)
    {
      $this->postalCode = $postalCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getState()
    {
      return $this->state;
    }

    /**
     * @param string $state
     * @return address
     */
    public function setState($state)
    {
      $this->state = $state;
      return $this;
    }

    /**
     * @return string
     */
    public function getStateCode()
    {
      return $this->stateCode;
    }

    /**
     * @param string $stateCode
     * @return address
     */
    public function setStateCode($stateCode)
    {
      $this->stateCode = $stateCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
      return $this->street;
    }

    /**
     * @param string $street
     * @return address
     */
    public function setStreet($street)
    {
      $this->street = $street;
      return $this;
    }

}
