<?php

class LogCategoryLevel
{
    const __default = 'None';
    const None = 'None';
    const Internal = 'Internal';
    const Finest = 'Finest';
    const Finer = 'Finer';
    const Fine = 'Fine';
    const Debug = 'Debug';
    const Info = 'Info';
    const Warn = 'Warn';
    const Error = 'Error';


}
