<?php

class getDatabasesForContactResponse
{

    /**
     * @var string $result
     */
    protected $result = null;

    /**
     * @param string $result
     */
    public function __construct($result)
    {
      $this->result = $result;
    }

    /**
     * @return string
     */
    public function getResult()
    {
      return $this->result;
    }

    /**
     * @param string $result
     * @return getDatabasesForContactResponse
     */
    public function setResult($result)
    {
      $this->result = $result;
      return $this;
    }

}
