<?php

/**
 * This file provides the hooks to install database tables
 *
 * @author Jeremy DeLong
 *        
 *         @file
 *         ma_ezsf.install file.
 */

/**
 * Implements hook_schema().
 *
 * @return array $schema
 */
function ma_ezsf_schema() {
  $schema = array();
  
  $schema['jenkins_dbs'] = array(
    'description' => 'The table for Jenkins Databases from SF', 
    'fields' => array(
      'dbid' => array(
        'description' => 'Primary key for Jenkins Databases', 
        'type' => 'serial', 
        'unsigned' => TRUE, 
        'not null' => TRUE
      ), 
      'dbname' => array(
        'description' => 'Database name.', 
        'type' => 'varchar', 
        'length' => 255, 
        'not null' => FALSE
      ), 
      'dbdescription' => array(
        'description' => 'Database description.', 
        'type' => 'text', 
        'size' => 'big', 
        'not null' => FALSE, 
        'default' => NULL
      ), 
      'dburl' => array(
        'description' => 'Database url.', 
        'type' => 'varchar', 
        'length' => 255, 
        'not null' => FALSE
      )
    ), 
    'primary key' => array(
      'dbid'
    )
  );
  
  return $schema;
}

/**
 * Implements hook_install().
 *
 * @return void
 */
function ma_ezsf_install() {
  /*
   * CREATE TABLE IF NOT EXISTS `jddjll45`.`ezsf1` (
   * `Userid` INT NOT NULL,
   * `db1` VARCHAR(45) BINARY NULL,
   * `db2` VARCHAR(45) BINARY NULL,
   * `db3` VARCHAR(45) BINARY NULL,
   * `db4` VARCHAR(45) BINARY NULL,
   * `db5` VARCHAR(45) BINARY NULL,
   * PRIMARY KEY (`Userid`))
   * ENGINE = InnoDB
   */
}

/**
 * Implements hook_uninstall().
 *
 * @return void
 */
function ma_ezsf_uninstall() {
  // variable_del('some variable');
}


